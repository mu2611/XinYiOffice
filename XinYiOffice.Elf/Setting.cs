﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace XinYiOffice.Elf
{
    public partial class Setting : Form
    {
        public Setting()
        {
            InitializeComponent();

            
            BindData();
        }

        public void BindData()
        {
            string server_url = string.Empty;
            string dname = string.Empty;
            string username = string.Empty;
            string pwd = string.Empty;
            string start = string.Empty;
            string homepage = string.Empty;

            SettingService.GetInitData(ref server_url, ref dname, ref username, ref pwd, ref start, ref homepage);

            textBox1.Text = server_url;
            textBox2.Text = dname;
            textBox3.Text = username;
            textBox4.Text = pwd;

            if (start == "1")
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }

            if (homepage == "1")
            {
                checkBox2.Checked = true;
            }
            else
            {
                checkBox2.Checked = false;
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            string server_url = textBox1.Text;
            string dname = textBox2.Text;
            string username = textBox3.Text;
            string pwd = textBox4.Text;


            string start = "0";
            if (checkBox1.Checked)
            {
                start = "1";
            }
            string homepage = "0";
            if (checkBox2.Checked)
            {
                homepage = "1";
            }

            bool t = SettingService.SetInitData(server_url, dname, username, pwd, start, homepage);

            if (t)
            {
                SettingService.SetSystemStart(checkBox1, this);//设置开机启动
                Exitedy();

                
                this.Dispose();     //释放由 Component 占用的资源。
                this.Close();       //关闭窗体。


                //保存成功!
                //Form mm = Main.ActiveForm;
                //if (mm == null || mm.IsDisposed)
                //{
                //    mm = new Main();
                //    mm.Show();
                //}
                //else
                //{
                //    mm.BringToFront();
                //}

                //MessageBox.Show("保存成功!\n程序将重新启动",);
                MessageBox.Show("保存成功!", "提示", MessageBoxButtons.OK);

                

                Application.Exit();
                Application.Restart();
                System.Environment.Exit(System.Environment.ExitCode);  //终止此进程并为基础操作系统提供指定的退出代码。
            }
        }




        private void button4_Click(object sender, EventArgs e)
        {
            Exitedy();
        }

        private void button4_MouseClick(object sender, MouseEventArgs e)
        {
            Exitedy();
        }

        protected void Exitedy()
        {
            this.Visible = false;
         
            
            //Setting.ActiveForm.Visible = false;
            //Setting.ActiveForm.Close();
            //Setting.ActiveForm.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string url = textBox1.Text;
            bool t = SettingService.GetvUrl(url);

            if (t)
            {
                MessageBox.Show("恭喜!服务器正常运行");
            }
            else
            {
                MessageBox.Show("服务器有些问题,请确认网址格式和地址是否正确! \n例如:http://s2.xinyioffice.com ");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
