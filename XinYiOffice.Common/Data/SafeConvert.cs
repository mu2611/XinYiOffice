﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common
{
    /// <summary>
    /// 安全数据类型转换类,提供一些基本的类型之间的转换,多数为string转换为int或DateTime转换为String
    /// </summary>
    public static class SafeConvert
    {
        public static readonly DateTime NullDateTime = DateTime.Parse("1900-1-1 0:00:00");
        public static readonly Decimal DefaultDecimalValue = Decimal.Parse("0.0");
        public static readonly int DefaultIntValue = 0;
        public static readonly bool DefaultBoolValue = false;
        public static readonly string DefaultStringValue = string.Empty;


        /// <summary>
        /// 将对象转换为时间类型,若转换失败返回默认时间
        /// </summary>
        /// <param name="obj">要转换为时间的值</param>
        /// <returns></returns>
        public static DateTime ToDateTime(object obj)
        {
            return ToDateTime(obj, NullDateTime);
        }

        /// <summary>
        /// 将对象转换为时间类型,若转换失败返回指定的时间
        /// </summary>
        /// <param name="obj">要转换为时间的值</param>
        /// <param name="defaultDateTime">若转换失败要返回的时间</param>
        /// <returns></returns>
        public static DateTime ToDateTime(object obj, DateTime defaultDateTime)
        {
            if (obj != null)
            {
                DateTime dt = defaultDateTime;
                try
                {
                    dt = DateTime.Parse(obj.ToString());
                }
                catch
                { 
                }
                return dt;
            }

            return defaultDateTime;
        }

        /// <summary>
        /// 将对象转换为Int类型,若转换失败返回默认Int值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <returns></returns>
        public static int ToInt(object obj)
        {
            return ToInt(obj, DefaultIntValue);
        }

        /// <summary>
        /// 将对象转换为Int类型,若转换失败返回默认指定的Int值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <param name="defaultValue">设置转换失败要返回的int值</param>
        /// <returns></returns>
        public static int ToInt(object obj, int defaultValue)
        {
            if (obj != null&&!string.IsNullOrEmpty(obj.ToString()))
            {
                int i = defaultValue;
                int.TryParse(obj.ToString(), out i);
                return i;
            }

            return defaultValue;
        }

        /// <summary>
        /// 将对象转换为Bool类型,若转换失败返回默认bool值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <returns></returns>
        public static bool ToBoolen(object obj)
        {
            return ToBoolen(obj, DefaultBoolValue);
        }

        /// <summary>
        /// 将对象转换为Bool类型,若转换失败返回指定的bool值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <param name="defaultValue">设置转换失败要返回的bool值</param>
        /// <returns></returns>
        public static bool ToBoolen(object obj, bool defaultValue)
        {
            if (obj == null)
            {
                return defaultValue;
            }
            else
            {
                if (!string.IsNullOrEmpty(obj.ToString()))
                {
                    string str = obj.ToString();
                    if (str == "1")
                    {
                        return true;
                    }
                    else if (str == "0")
                    {
                        return false;
                    }
                    else if (str.ToLower().ToString() == "y" || str.ToLower().ToString() == "yes")
                    {
                        return true;
                    }
                    else if (str.ToLower().ToString() == "n" || str.ToLower().ToString() == "no")
                    {
                        return false;
                    }
                    else if (str.ToLower().ToString() == "true")
                    {
                        return true;
                    }
                    else if (str.ToLower().ToString() == "false")
                    {
                        return false;
                    }
                    else
                    {
                        //不符合任意
                        return defaultValue;
                    }
                }
            }
            return defaultValue;

        }

        /// <summary>
        /// 将对象转换为String类型,若转换失败返回默认的String值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <returns></returns>
        public static string ToString(object obj)
        {
            return ToString(obj, DefaultStringValue);
        }

        /// <summary>
        /// 将对象转换为String类型,若转换失败返回指定的String值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <param name="defaultValue">设置转换失败要返回的String值</param>
        /// <returns></returns>
        public static string ToString(object obj, string defaultValue)
        {
            if (obj != null)
            {
                return obj.ToString();
            }
            else
            {
                return defaultValue;
            }

        }

        /// <summary>
        /// 将对象转换为Decimal类型，若失败将返回默认的Decimal值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <returns></returns>
        public static decimal ToDecimal(object obj)
        {
            return ToDecimal(obj, DefaultDecimalValue);
        }

        /// <summary>
        /// 将对象转换为Decimal类型，若失败将返回指定的Decimal值
        /// </summary>
        /// <param name="obj">要转换的值</param>
        /// <param name="defaultValue">设置转换失败要返回的Decimal值</param>
        /// <returns></returns>
        public static decimal ToDecimal(object obj, decimal defaultValue)
        {
            if (obj != null && obj.ToString() != "")
            {
                decimal dec = defaultValue;
                decimal.TryParse(obj.ToString(), out dec);
                return dec;
            }

            return defaultValue;
        }

    }


}
