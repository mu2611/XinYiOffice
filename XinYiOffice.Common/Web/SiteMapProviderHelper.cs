﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Permissions;


namespace XinYiOffice.Common.Web
{
    /// <summary>
    /// 针对站点地图的操作类
    /// </summary>

    public class SiteMapProviderHelper : StaticSiteMapProvider
    {
        //根节点
        private SiteMapNode _rootNode = null;
        //初始化状态
        private bool initialized = false;
        //默认构造函数
        public SiteMapProviderHelper() { }

        #region 重载方法
        /// <summary>
        /// 返回当前网站地图的根节点
        /// </summary>
        public override SiteMapNode RootNode
        {
            get
            {
                SiteMapNode temp = null;
                temp = BuildSiteMap();
                return temp;
            }
        }

        /// <summary>
        /// 返回当前提供程序目前管理的所有节点的根节点
        /// </summary>
        /// <returns>根节点</returns>
        protected override SiteMapNode GetRootNodeCore()
        {
            return RootNode;
        }
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="name"></param>
        /// <param name="attributes"></param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection attributes)
        {
            if (IsInitialized)
            {
                return;
            }
            base.Initialize(name, attributes);
            initialized = true;
        }
        /// <summary>
        ///  清空所有导航节点
        /// </summary>
        protected override void Clear()
        {
            lock (this)
            {
                _rootNode = null;
                base.Clear();
            }
        }
        /// <summary>
        /// 创建sitemap树
        /// </summary>
        /// <returns>SiteMap的根节点</returns>
        public override SiteMapNode BuildSiteMap()
        {
            lock (this)
            {
                if (!IsInitialized)
                {
                    throw new Exception("BuildSiteMap called incorrectly");
                }
                if (null == _rootNode)
                {
                    Clear();
                    SiteMapProviderMetaData metaData = new SiteMapProviderMetaData();
                    AddRoot(metaData);
                }
                return _rootNode;
            }
        }
        #endregion

        #region 自定义属性
        /// <summary>
        /// 是否已经初始化
        /// </summary>
        public virtual bool IsInitialized
        {
            get
            {
                return initialized;
            }
        }
        #endregion

        public void AddChild(SiteMapNode CurrentNode, SiteMapProviderMetaData MetaData)
        {
            DataView child = MetaData.GetChild(CurrentNode);
            foreach (DataRowView row in child)
            {
                SiteMapNode temp = new SiteMapNode(this, row[0].ToString(), row[2].ToString(), row[1].ToString());
                AddNode(temp, CurrentNode);
                AddChild(temp, MetaData);
            }
        }

        public void AddRoot(SiteMapProviderMetaData MetaData)
        {
            DataView root = MetaData.GetRoot();
            foreach (DataRowView row in root)
            {
                _rootNode = new SiteMapNode(this, row[0].ToString(), row[2].ToString(), row[1].ToString());
                AddNode(_rootNode, null);
                AddChild(_rootNode, MetaData);
            }
        }



    }


    public class SiteMapProviderMetaData
    {
        
        private DataTable _source;//保存SiteMap元数据的表
        public SiteMapProviderMetaData(DataTable dt)
        {
            _source = dt;
            
        }

        public SiteMapProviderMetaData()
        {

            _source = (DataTable)DataCache.GetCache("") as DataTable;

        }
        /// <summary>
        /// 获得SiteMap的数据源，并给_Source赋值
        /// </summary>
        /// <returns>元数据</returns>
        private DataTable GetSiteMapMetaData()
        {
            return this._source;
        }

        #region GetChild方法
        /// <summary>
        /// 获得当前节点的子节点
        /// </summary>
        /// <param name="Source">数据源</param>
        /// <param name="CurrentId">当前节点</param>
        /// <returns>子节点的元数据</returns>
        public DataView GetChild(SiteMapNode CurrentNode)
        {
            return this.GetChild(this._source, CurrentNode);
        }

        /// <summary>
        /// 获得当前节点的子节点
        /// </summary>
        /// <param name="Source">数据源</param>
        /// <param name="CurrentId">当前节点</param>
        /// <returns>子节点的元数据</returns>
        public DataView GetChild(DataTable Source, SiteMapNode CurrentNode)
        {
            DataView dvw = new DataView(Source);
            dvw.RowFilter = "MenuTreeId=" + CurrentNode.Key.ToString();
            return dvw;
        }
        #endregion

        #region GetRoot方法
        /// <summary>
        /// 获得根节点元数据
        /// </summary>
        /// <param name="Source">数据源</param>
        /// <returns>根节点的元数据</returns>
        public DataView GetRoot(DataTable Source)
        {
            DataView dvw = new DataView(Source);
            dvw.RowFilter = "MenuTreeId is null";
            return dvw;
        }
        /// <summary>
        /// 获得根节点元数据
        /// </summary>
        /// <returns>根节点的元数据</returns>
        public DataView GetRoot()
        {
            return this.GetRoot(this._source);
        }
        #endregion
    }



}
