﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace XinYiOffice.Common.Web
{
    public class PageValidate
    {
        private static Regex RegNumber = new Regex("^[0-9]+$");
        private static Regex RegTelPhone = new Regex(@"^[1]+[0-9]+\d{9}");
        private static Regex RegCarNumber = new Regex(@"^[\u4E00-\u9FA5]([0-9A-Z]{6})$|([0-9A-Z]{5}[\u4E00-\u9FA5]{1})$|^[a-zA-Z]{2}[0-9a-zA-Z]{7}$");// /^[\u4E00-\u9FA5]([0-9A-Z]{6})|([0-9A-Z]{5}[\u4E00-\u9FA5]{1})$/
        private static Regex RegNumberSign = new Regex("^[+-]?[0-9]+$");

        private static Regex RegDecimal = new Regex("^[0-9]+[.]?[0-9]+$");

        private static Regex RegDecimalSign = new Regex("^[+-]?[0-9]+[.]?[0-9]+$"); //等价于^[+-]?\d+[.]?\d+$

        private static Regex RegEmail = new Regex("^[\\w-]+@[\\w-]+\\.(com|net|org|edu|mil|tv|biz|info)$");//w 英文字母或数字的字符串，和 [a-zA-Z0-9] 语法一样

        private static Regex RegCHZN = new Regex("[\u4e00-\u9fa5]");

        public static bool IsDateTime(string str)
        {
            bool t = false;

            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    DateTime dt = DateTime.Parse(str);
                    t = true;
                }
                catch
                {
                }

            }

            return t;
        }

        #region 数字字符串检查

        /// <summary> 
        /// 检查Request查询字符串的键值，是否是数字，最大长度限制 
        /// </summary> 
        /// <param name=req>Request</param> 
        /// <param name=inputKey>Request的键值</param> 
        /// <param name=maxLen>最大长度</param> 
        /// <returns>返回Request查询字符串</returns> 
        public static string FetchInputDigit(HttpRequest req, string inputKey, int maxLen)
        {

            string retVal = string.Empty;

            if (inputKey != null && inputKey != string.Empty)
            {

                retVal = req.QueryString[inputKey];

                if (null == retVal)

                    retVal = req.Form[inputKey];

                if (null != retVal)
                {

                    retVal = SqlText(retVal, maxLen);

                    if (!IsNumber(retVal))

                        retVal = string.Empty;

                }

            }
            if (retVal == null)

                retVal = string.Empty;

            return retVal;
        }


        /// <summary>
        /// 判断手机号
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsTelPhone(string inputData)
        {
            Match m = RegTelPhone.Match(inputData);
            return m.Success;
        }

        /// <summary>
        /// 判断车牌号码
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsCarNumber(string inputData)
        {
            Match m = RegCarNumber.Match(inputData);
            return m.Success;
        }

        /// <summary> 
        /// 是否数字字符串 
        /// </summary> 
        /// <param name=inputData>输入字符串</param> 
        /// <returns></returns> 
        public static bool IsNumber(string inputData)
        {
            Match m = RegNumber.Match(inputData);
            return m.Success;
        }

        /**/
        /// <summary> 
        /// 是否数字字符串可带正负号 
        /// </summary> 
        /// <param name=inputData>输入字符串</param> 
        /// <returns></returns> 
        public static bool IsNumberSign(string inputData)
        {
            Match m = RegNumberSign.Match(inputData);
            return m.Success;
        }
        /// <summary> 
        /// 是否是浮点数 
        /// </summary> 
        /// <param name=inputData>输入字符串</param> 
        /// <returns></returns> 
        public static bool IsDecimal(string inputData)
        {
            Match m = RegDecimal.Match(inputData);
            return m.Success;
        }
        /// <summary> 
        /// 是否是浮点数可带正负号 
        /// </summary> 
        /// <param name=inputData>输入字符串</param> 
        /// <returns></returns> 
        public static bool IsDecimalSign(string inputData)
        {
            Match m = RegDecimalSign.Match(inputData);

            return m.Success;

        }
        #endregion

        #region 中文检测

        /// <summary>

        /// 检测是否有中文字符 
        /// </summary> 
        /// <param name=inputData></param> 
        /// <returns></returns> 
        public static bool IsHasCHZN(string inputData)
        {
            Match m = RegCHZN.Match(inputData);
            return m.Success;
        }
        #endregion

        #region 邮件地址

        /// <summary> 
        /// 是否是浮点数可带正负号 
        /// </summary> 
        /// <param name=inputData>输入字符串</param> 
        /// <returns></returns> 
        public static bool IsEmail(string inputData)
        {
            Match m = RegEmail.Match(inputData);
            return m.Success;
        }
        #endregion

        #region 其他

        /// <summary>

        /// 检查字符串最大长度，返回指定长度的串 
        /// </summary> 
        /// <param name=sqlInput>输入字符串</param> 
        /// <param name=maxLength>最大长度</param> 
        /// <returns></returns>          
        public static string SqlText(string sqlInput, int maxLength)
        {
            if (sqlInput != null && sqlInput != string.Empty)
            {
                sqlInput = sqlInput.Trim();

                if (sqlInput.Length > maxLength)//按最大长度截取字符串

                    sqlInput = sqlInput.Substring(0, maxLength);

            }
            return sqlInput;
        }

        /// <summary> 
        /// 字符串编码 
        /// </summary> 
        /// <param name=inputData></param> 
        /// <returns></returns> 
        public static string HtmlEncode(string inputData)
        {

            return HttpUtility.HtmlEncode(inputData);
        }
        /// <summary> 
        /// 设置Label显示Encode的字符串 
        /// </summary> 
        /// <param name=lbl></param> 
        /// <param name=txtInput></param> 
        public static void SetLabel(Label lbl, string txtInput)
        {
            lbl.Text = HtmlEncode(txtInput);
        }
        /// <summary> 
        /// 设置Label显示Encode的字符串 
        /// </summary> 
        /// <param name=lbl></param> 
        /// <param name=inputObj></param> 
        public static void SetLabel(Label lbl, object inputObj)
        {

            SetLabel(lbl, inputObj.ToString());

        }



        #endregion


        /// <summary>
        /// 过滤字符,合法返回true,非法为false
        /// </summary>
        public static bool Filter(string sInput)
        {
            bool output = false;

            if (sInput == null || sInput.Trim() == string.Empty)
                return output;
            string sInput1 = sInput.ToLower();

            string pattern = @"*|and|exec|insert|select|delete|update|count|master|truncate|declare|char(|mid(|chr(|'";
            if (Regex.Match(sInput1, Regex.Escape(pattern), RegexOptions.Compiled | RegexOptions.IgnoreCase).Success)
            {
                //throw new Exception("字符串中含有非法字符!");
                return output;
            }
            else
            {
                //output = output.Replace("'", "''");
                output = true;
            }

            return output;
        }

    }

}


