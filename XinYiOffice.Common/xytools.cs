using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace XinYiOffice.Common
{
	/// <summary>
	/// 新亿web处理方法及工具
	/// 2008.1.4晚上0:20
	/// 作者:高文龙  E-mail:webgee@163.com
	/// </summary>
	public class xytools:System.Web.UI.Page
	{
		public  xytools(){}

        /// <summary>
        /// 弹出对话框返回刚才页面
        /// </summary>
        /// <param name="msg"></param>
		public static void web_alert(string msg)
		{
			System.Web.HttpContext.Current.Response.Write(@"<script>alert('"+msg+"');vbscript:history.back();</script>");
		}

        /// <summary>
        /// 弹出对话框,到达指定url
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="url"></param>
        public static void web_alert(string msg, string url)
		{
			System.Web.HttpContext.Current.Response.Write(@"<script>alert('"+msg+"');this.location.href='"+url+"';</script>");
		}

        public static void web_alert_new_url(string msg)//弹出对话框重写打开刚才页面
        {
            string url = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();

            if (url.LastIndexOf("?") > 0)//如果有 ? 问号 证明有参数 地址后就加 &
            {
                System.Web.HttpContext.Current.Response.Write(@"<script>alert('" + msg + "');this.location.href='" + url + "'+'&xinyi_sj='+Math.random();;</script>");
            }
            else
            {
                System.Web.HttpContext.Current.Response.Write(@"<script>alert('" + msg + "');this.location.href='" + url + "'+'?xy_sj='+Math.random();;</script>");
            }
        }

        public static void web_alert_new_url(string msg, string url)//弹出对话框,到达指定url并使这个页面是新载入的
		{
			if(url.LastIndexOf("?")>0)//如果有 ? 问号 证明有参数 地址后就加 &
			{
				System.Web.HttpContext.Current.Response.Write(@"<script>alert('"+msg+"');this.location.href='"+url+"'+'&xinyi_sj='+Math.random();;</script>");
			}
			else
			{
				System.Web.HttpContext.Current.Response.Write(@"<script>alert('"+msg+"');this.location.href='"+url+"'+'?xy_sj='+Math.random();;</script>");
			}
		}

        public static void YesorNo_new_url(string msg, string url, string url2)//弹出确认信息,转向url指向地址,否则转向url2
		{
			if(url.LastIndexOf("?")>0)//如果有 ? 问号 证明有参数 地址后就加 &
			{
				System.Web.HttpContext.Current.Response.Write(@"<script>if(confirm('"+msg+"')){this.location.href='"+url+"'+'&xy_sj='+Math.random();}else{this.location.href='"+url2+"'+'&amibook_sj='+Math.random();}</script>");
			}
			else
			{
				System.Web.HttpContext.Current.Response.Write(@"<script>if(confirm('"+msg+"')){this.location.href='"+url+"'+'?xy_sj='+Math.random();}else{this.location.href='"+url2+"'+'?amibook_sj='+Math.random();}</script>");
			}
		}

        public static void write(string msg)
        {
            System.Web.HttpContext.Current.Response.Write(msg);
        }

        public static void WriteJs(string jsContent,object obj)
        {
            System.Web.UI.Page pg = obj as System.Web.UI.Page;

            pg.RegisterStartupScript("writejs", "<script type='text/javascript'>" + jsContent + "</script>");
            
        }

        public static void href_url(string url)
		{
			System.Web.HttpContext.Current.Response.Write(@"<script>this.location.href='"+url+"';</script>");
		}

        public static void href_url_new(string url)
		{
			if(url.LastIndexOf("?")>0)//如果有 ? 问号 证明有参数 地址后就加 &
			{
				System.Web.HttpContext.Current.Response.Write(@"<script>this.location.href='"+url+"'+'&xinyi_sj='+Math.random();</script>");
			}
			else
			{//若没有 参数 就 直接在后面 加 ?
				System.Web.HttpContext.Current.Response.Write(@"<script>this.location.href='"+url+"'+'?xinyi_sj='+Math.random();</script>");

			}
			
		}

		#region //地址参数验证
        public static bool url_exist(string[] web)
		{
			bool t=true;
			
			for(int i=0;i<web.Length;i++)
			{
				string str=web[i].ToString();

				if(System.Web.HttpContext.Current.Request.QueryString[""+str+""]==null)
				{
					t=false; //如果不存在返回 false,并跳出循环
					
					break;
				}
			}


			return t;
		}
		#endregion

        #region //地址参数验证
        public static bool url_existNoData(string[] web)
        {
            bool t = true;

            for (int i = 0; i < web.Length; i++)
            {
                string str = web[i].ToString();

                if (System.Web.HttpContext.Current.Request.QueryString["" + str + ""] == null || System.Web.HttpContext.Current.Request.QueryString["" + str + ""].ToString()=="")
                {
                    t = false; //如果不存在返回 false,并跳出循环

                    break;
                }
            }


            return t;
        }
        #endregion

		#region //地址参数读取值
        public static string url_get(string str)
		{
			string t="";
			
			if(System.Web.HttpContext.Current.Request.QueryString[""+str+""]!=null)
			{
				t=System.Web.HttpContext.Current.Request.QueryString[""+str+""].ToString();
			}

			return t;
		}
		#endregion

        #region string转换int失败返回指定值
        public static int StringToInt(string obj, int ix)
        {
            int j = 0;
            try
            {
                j = int.Parse(obj.ToString());
            }
            catch
            {
                j = ix;
            }
            return j;
        } 
        #endregion

		#region //验证表单元素是否存在,一般表单处理页调用
        public static bool form_exist(string[] web)
		{
			bool t=true;
			
			for(int i=0;i<web.Length;i++)
			{
				string str=web[i].ToString();

				if(System.Web.HttpContext.Current.Request.Params[""+str+""]==null)
				{
					t=false; //如果不存在返回 false,并跳出循环

                    //xytools.web_alert("表单元素名:"+str+"为 null ");
					
					break;
				}
			}


			return t;
		}
		#endregion

        #region //验证表单元素是否存在,并且判断是否为空值
        public static bool form_existNoData(string[] web)
        {
            bool t = true;

            for (int i = 0; i < web.Length; i++)
            {
                string str = web[i].ToString();

                if (System.Web.HttpContext.Current.Request.Params["" + str + ""] == null || System.Web.HttpContext.Current.Request.Params["" + str + ""].ToString() =="")
                {
                    t = false; //如果不存在或为空 返回 false,并跳出循环

                    //xytools.web_alert("表单元素:"+str+"空值或为''");

                    break;
                }
            }


            return t;
        }
        #endregion

		#region //获取所写表单名的值
        public static string form_get(string s)
		{
			string str="";

			if(System.Web.HttpContext.Current.Request.Params[""+s+""]==null)
			{

			}
			else
			{
				str=System.Web.HttpContext.Current.Request.Params[""+s+""].ToString();
			}

			return str;
		}
		#endregion

        #region //获取所写表单名的值,若不存在即返回data的值
        public static object form_get(string s, object data)
        {
            object str;

            if (System.Web.HttpContext.Current.Request.Params["" + s + ""] == null)
            {
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                str = System.Web.HttpContext.Current.Request.Params["" + s + ""].ToString();
            }

            return str;
        } 
        #endregion

        #region //获取所写表单名的值,若不存在即返回data的值
        public static string form_get(string s, string data)
        {
            string str;

            if (System.Web.HttpContext.Current.Request.Params["" + s + ""] == null)
            {
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                str = System.Web.HttpContext.Current.Request.Params["" + s + ""].ToString();
            }

            return str;
        }
        #endregion


        #region 清理字符串中的HTML格式
        /// <summary>
        /// 清理字符串中的HTML格式 
        /// </summary>
        /// <param name="strHtml">字符串</param>
        /// <returns>不带HTML的字符串</returns>
        public static string ClearHtml(string Htmlstring)
        {
            string[] aryReg ={   
                      @"<script[^>]*?>.*?</script>",   
    
                      @"<(\/\s*)?!?((\w+:)?\w+)(\w+(\s*=?\s*(([""'])(\\[""'tbnr]|[^\7])*?\7|\w+)|.{0})|\s)*?(\/\s*)?>",   
                      @"([\r\n])[\s]+",   
                      @"&(quot|#34);",   
                      @"&(amp|#38);",   
                      @"&(lt|#60);",   
                      @"&(gt|#62);",     
                      @"&(nbsp|#160);",     
                      @"&(iexcl|#161);",   
                      @"&(cent|#162);",   
                      @"&(pound|#163);",   
                      @"&(copy|#169);",   
                      @"&#(\d+);",   
                      @"-->",   
                      @"<!--.*\n"   
                    };

            string[] aryRep =   {   
                        "",   
                        "",   
                        "",   
                        "\"",   
                        "&",   
                        "<",   
                        ">",   
                        "   ",   
                        "\xa1",//chr(161),   
                        "\xa2",//chr(162),   
                        "\xa3",//chr(163),   
                        "\xa9",//chr(169),   
                        "",   
                        "\r\n",   
                        ""   
                      };

            string newReg = aryReg[0];
            StringBuilder strOutput = new StringBuilder(Htmlstring);
            for (int i = 0; i < aryReg.Length; i++)
            {
                Regex regex = new Regex(aryReg[i], RegexOptions.IgnoreCase);
                strOutput = new StringBuilder(regex.Replace(strOutput.ToString(), aryRep[i]));
            }
            strOutput.Replace("<", "");
            strOutput.Replace(">", "");
            strOutput.Replace("\r\n", "");
            return strOutput.ToString();
        } 
        #endregion

        #region 截取字符
        public static string CutString(string str, int length)
        {
            string newString = string.Empty;
            str = ClearHtml(str);
            if (str != "")
            {
                if (str.Length > length)
                {
                    newString = str.Substring(0, length) + "...";
                }
                else
                {
                    newString = str;
                }
            }

            //newString = ClearHtml(newString);//清除html格式,只输出文本
            return newString;
        } 
        #endregion

        #region 
        /// <summary>
        /// js输出时过滤html格式
        /// </summary>
        /// <param name="mystr"></param>
        /// <returns></returns>
        public static string JsToHtml(string mystr)
        {
            //mystr = mystr.Replace("\n\r", "<br>&nbsp;&nbsp;");//注意这里/r/n的顺序，此为错误写法还是会报错
            mystr = mystr.Replace("\r", "");//这才是正确的！
            mystr = mystr.Replace("\n", "");
            mystr = mystr.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
            mystr = mystr.Replace(" ", "&nbsp;");
            //mystr = mystr.Replace("<","&lt;");
            //mystr = mystr.Replace(">","&gt;");
            //mystr = mystr.Replace("&","&amp");

            return mystr;

        } 
        #endregion

        public static string ApplicationPath()
        {
            string m_Application;

            if (HttpContext.Current.Request.ApplicationPath.Equals("/"))
            {
                m_Application = "http://" + HttpContext.Current.Request.Url.Authority;
            }
            else
                m_Application = HttpContext.Current.Request.ApplicationPath;

            return m_Application;
        }

        public static string CurrentExecutionFilePath()
        {
            //请求完整路径
            string q_url = HttpContext.Current.Request.CurrentExecutionFilePath.ToString();
            
            if(HttpContext.Current.Request.QueryString!=null&&HttpContext.Current.Request.QueryString.ToString()!="")
            {
                q_url +="?"+HttpContext.Current.Request.QueryString.ToString();
            }

            return q_url;
        }


        #region 获取用户登录IP
        /// <summary>
        /// 获取用户登录IP
        /// </summary>
        /// <returns></returns>
        public static string GetIp
        {
            get
            {
                string result = String.Empty;
                result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (result != null && result != String.Empty)
                {
                    //可能有代理 
                    if (result.IndexOf(".") == -1) //没有“.”肯定是非IPv4格式 
                        result = null;
                    else
                    {
                        if (result.IndexOf(",") != -1)
                        {
                            //有“,”，估计多个代理。取第一个不是内网的IP。 
                            result = result.Replace(" ", "").Replace("'", "");
                            string[] temparyip = result.Split(",;".ToCharArray());
                            for (int i = 0; i < temparyip.Length; i++)
                            {
                                if (IsIPAddress(temparyip[i])
                                && temparyip[i].Substring(0, 3) != "10."
                                && temparyip[i].Substring(0, 7) != "192.168"
                                && temparyip[i].Substring(0, 7) != "172.16.")
                                {
                                    return temparyip[i]; //找到不是内网的地址 
                                }
                            }
                        }
                        else if (IsIPAddress(result)) //代理即是IP格式 
                            return result;
                        else
                            result = null; //代理中的内容 非IP，取IP 
                    }
                }
                string IpAddress = (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null && HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != String.Empty) ? HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] : HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                if (null == result || result == String.Empty)
                    result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                if (result == null || result == String.Empty)

                    result = HttpContext.Current.Request.UserHostAddress;
                return result;
            }//get

        } 
        #endregion

        #region bool IsIPAddress(str1) 判断是否是IP格式
        /**/
        /// <summary>
        /// 判断是否是IP地址格式 0.0.0.0
        /// </summary>
        /// <param name="str1">待判断的IP地址</param>
        /// <returns>true or false</returns>
        public static bool IsIPAddress(string str1)
        {
            if (str1 == null || str1 == string.Empty || str1.Length < 7 || str1.Length > 15) return false;

            string regformat = @"^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$";

            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);
            return regex.IsMatch(str1);
        }
        #endregion

        #region IP 加密如 22.68.13.*
        public static string GetIpEncrypt(string str_ip)
        {
            string ip = string.Empty ;
            if (str_ip!=null&&str_ip!=""&&IsIPAddress(str_ip))
            {
                ip=str_ip.Substring(0, str_ip.LastIndexOf(".") + 1) + "*";
            }
            return ip;

        } 
        #endregion

        #region //清除空格
        public static string Trim(string str)
        {
            string Snew = string.Empty;

            Snew = Regex.Replace(str, @"\s", "");

            return Snew;
        } 
        #endregion

        /// <summary>
        /// 判断一个或多个变量是否存在,和存在""如果存在就返回false
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool DataIsNoNull(params object[] obj)
        {
            bool t = true;
            for (int i = 0; i < obj.Length;i++ )
            {
                if (obj[i] == null||obj[i].ToString()=="")
                {
                    t = false;
                    break;
                }
            }

            return t;
        }


	}
}
