﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:BankAccount
	/// </summary>
	public partial class BankAccount
	{
		public BankAccount()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "BankAccount"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from BankAccount");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.BankAccount model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into BankAccount(");
			strSql.Append("BankInfo,BankAccountName,BankAccountNumber,BankAccountTel,CashIn,Sate,OpeningTime,InstitutionId,DepartmentId,StorageAmount,BearerAccountId,AccountType,AccountPurpose,CreateAccountId,CreateTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@BankInfo,@BankAccountName,@BankAccountNumber,@BankAccountTel,@CashIn,@Sate,@OpeningTime,@InstitutionId,@DepartmentId,@StorageAmount,@BearerAccountId,@AccountType,@AccountPurpose,@CreateAccountId,@CreateTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@BankInfo", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountName", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountTel", SqlDbType.VarChar,4000),
					new SqlParameter("@CashIn", SqlDbType.Float,8),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@OpeningTime", SqlDbType.DateTime),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@DepartmentId", SqlDbType.Int,4),
					new SqlParameter("@StorageAmount", SqlDbType.Float,8),
					new SqlParameter("@BearerAccountId", SqlDbType.Int,4),
					new SqlParameter("@AccountType", SqlDbType.Int,4),
					new SqlParameter("@AccountPurpose", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.BankInfo;
			parameters[1].Value = model.BankAccountName;
			parameters[2].Value = model.BankAccountNumber;
			parameters[3].Value = model.BankAccountTel;
			parameters[4].Value = model.CashIn;
			parameters[5].Value = model.Sate;
			parameters[6].Value = model.OpeningTime;
			parameters[7].Value = model.InstitutionId;
			parameters[8].Value = model.DepartmentId;
			parameters[9].Value = model.StorageAmount;
			parameters[10].Value = model.BearerAccountId;
			parameters[11].Value = model.AccountType;
			parameters[12].Value = model.AccountPurpose;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.BankAccount model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update BankAccount set ");
			strSql.Append("BankInfo=@BankInfo,");
			strSql.Append("BankAccountName=@BankAccountName,");
			strSql.Append("BankAccountNumber=@BankAccountNumber,");
			strSql.Append("BankAccountTel=@BankAccountTel,");
			strSql.Append("CashIn=@CashIn,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("OpeningTime=@OpeningTime,");
			strSql.Append("InstitutionId=@InstitutionId,");
			strSql.Append("DepartmentId=@DepartmentId,");
			strSql.Append("StorageAmount=@StorageAmount,");
			strSql.Append("BearerAccountId=@BearerAccountId,");
			strSql.Append("AccountType=@AccountType,");
			strSql.Append("AccountPurpose=@AccountPurpose,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@BankInfo", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountName", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@BankAccountTel", SqlDbType.VarChar,4000),
					new SqlParameter("@CashIn", SqlDbType.Float,8),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@OpeningTime", SqlDbType.DateTime),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@DepartmentId", SqlDbType.Int,4),
					new SqlParameter("@StorageAmount", SqlDbType.Float,8),
					new SqlParameter("@BearerAccountId", SqlDbType.Int,4),
					new SqlParameter("@AccountType", SqlDbType.Int,4),
					new SqlParameter("@AccountPurpose", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.BankInfo;
			parameters[1].Value = model.BankAccountName;
			parameters[2].Value = model.BankAccountNumber;
			parameters[3].Value = model.BankAccountTel;
			parameters[4].Value = model.CashIn;
			parameters[5].Value = model.Sate;
			parameters[6].Value = model.OpeningTime;
			parameters[7].Value = model.InstitutionId;
			parameters[8].Value = model.DepartmentId;
			parameters[9].Value = model.StorageAmount;
			parameters[10].Value = model.BearerAccountId;
			parameters[11].Value = model.AccountType;
			parameters[12].Value = model.AccountPurpose;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.CreateTime;
			parameters[15].Value = model.TenantId;
			parameters[16].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BankAccount ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BankAccount ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.BankAccount GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,BankInfo,BankAccountName,BankAccountNumber,BankAccountTel,CashIn,Sate,OpeningTime,InstitutionId,DepartmentId,StorageAmount,BearerAccountId,AccountType,AccountPurpose,CreateAccountId,CreateTime,TenantId from BankAccount ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.BankAccount model=new XinYiOffice.Model.BankAccount();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BankInfo"]!=null && ds.Tables[0].Rows[0]["BankInfo"].ToString()!="")
				{
					model.BankInfo=ds.Tables[0].Rows[0]["BankInfo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankAccountName"]!=null && ds.Tables[0].Rows[0]["BankAccountName"].ToString()!="")
				{
					model.BankAccountName=ds.Tables[0].Rows[0]["BankAccountName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankAccountNumber"]!=null && ds.Tables[0].Rows[0]["BankAccountNumber"].ToString()!="")
				{
					model.BankAccountNumber=ds.Tables[0].Rows[0]["BankAccountNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankAccountTel"]!=null && ds.Tables[0].Rows[0]["BankAccountTel"].ToString()!="")
				{
					model.BankAccountTel=ds.Tables[0].Rows[0]["BankAccountTel"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CashIn"]!=null && ds.Tables[0].Rows[0]["CashIn"].ToString()!="")
				{
					model.CashIn=decimal.Parse(ds.Tables[0].Rows[0]["CashIn"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OpeningTime"]!=null && ds.Tables[0].Rows[0]["OpeningTime"].ToString()!="")
				{
					model.OpeningTime=DateTime.Parse(ds.Tables[0].Rows[0]["OpeningTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InstitutionId"]!=null && ds.Tables[0].Rows[0]["InstitutionId"].ToString()!="")
				{
					model.InstitutionId=int.Parse(ds.Tables[0].Rows[0]["InstitutionId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DepartmentId"]!=null && ds.Tables[0].Rows[0]["DepartmentId"].ToString()!="")
				{
					model.DepartmentId=int.Parse(ds.Tables[0].Rows[0]["DepartmentId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StorageAmount"]!=null && ds.Tables[0].Rows[0]["StorageAmount"].ToString()!="")
				{
					model.StorageAmount=decimal.Parse(ds.Tables[0].Rows[0]["StorageAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BearerAccountId"]!=null && ds.Tables[0].Rows[0]["BearerAccountId"].ToString()!="")
				{
					model.BearerAccountId=int.Parse(ds.Tables[0].Rows[0]["BearerAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AccountType"]!=null && ds.Tables[0].Rows[0]["AccountType"].ToString()!="")
				{
					model.AccountType=int.Parse(ds.Tables[0].Rows[0]["AccountType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AccountPurpose"]!=null && ds.Tables[0].Rows[0]["AccountPurpose"].ToString()!="")
				{
					model.AccountPurpose=int.Parse(ds.Tables[0].Rows[0]["AccountPurpose"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,BankInfo,BankAccountName,BankAccountNumber,BankAccountTel,CashIn,Sate,OpeningTime,InstitutionId,DepartmentId,StorageAmount,BearerAccountId,AccountType,AccountPurpose,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM BankAccount ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,BankInfo,BankAccountName,BankAccountNumber,BankAccountTel,CashIn,Sate,OpeningTime,InstitutionId,DepartmentId,StorageAmount,BearerAccountId,AccountType,AccountPurpose,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM BankAccount ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM BankAccount ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from BankAccount T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "BankAccount";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

