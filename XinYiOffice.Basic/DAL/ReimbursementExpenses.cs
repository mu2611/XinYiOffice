﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:ReimbursementExpenses
	/// </summary>
	public partial class ReimbursementExpenses
	{
		public ReimbursementExpenses()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "ReimbursementExpenses"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ReimbursementExpenses");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.ReimbursementExpenses model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ReimbursementExpenses(");
			strSql.Append("CostTilte,CostCon,Remarks,ApplicationDate,Amount,CostType,BillNumber,ApplicantName,UserAccountId,RelatedClient,RelatedProject,AuditOpinion,State,DealingPeopleAccountId,CreateAccountId,CreateTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@CostTilte,@CostCon,@Remarks,@ApplicationDate,@Amount,@CostType,@BillNumber,@ApplicantName,@UserAccountId,@RelatedClient,@RelatedProject,@AuditOpinion,@State,@DealingPeopleAccountId,@CreateAccountId,@CreateTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CostTilte", SqlDbType.VarChar,4000),
					new SqlParameter("@CostCon", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@ApplicationDate", SqlDbType.DateTime),
					new SqlParameter("@Amount", SqlDbType.Float,8),
					new SqlParameter("@CostType", SqlDbType.Int,4),
					new SqlParameter("@BillNumber", SqlDbType.Int,4),
					new SqlParameter("@ApplicantName", SqlDbType.VarChar,4000),
					new SqlParameter("@UserAccountId", SqlDbType.Int,4),
					new SqlParameter("@RelatedClient", SqlDbType.VarChar,4000),
					new SqlParameter("@RelatedProject", SqlDbType.VarChar,4000),
					new SqlParameter("@AuditOpinion", SqlDbType.VarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@DealingPeopleAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.CostTilte;
			parameters[1].Value = model.CostCon;
			parameters[2].Value = model.Remarks;
			parameters[3].Value = model.ApplicationDate;
			parameters[4].Value = model.Amount;
			parameters[5].Value = model.CostType;
			parameters[6].Value = model.BillNumber;
			parameters[7].Value = model.ApplicantName;
			parameters[8].Value = model.UserAccountId;
			parameters[9].Value = model.RelatedClient;
			parameters[10].Value = model.RelatedProject;
			parameters[11].Value = model.AuditOpinion;
			parameters[12].Value = model.State;
			parameters[13].Value = model.DealingPeopleAccountId;
			parameters[14].Value = model.CreateAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ReimbursementExpenses model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ReimbursementExpenses set ");
			strSql.Append("CostTilte=@CostTilte,");
			strSql.Append("CostCon=@CostCon,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("ApplicationDate=@ApplicationDate,");
			strSql.Append("Amount=@Amount,");
			strSql.Append("CostType=@CostType,");
			strSql.Append("BillNumber=@BillNumber,");
			strSql.Append("ApplicantName=@ApplicantName,");
			strSql.Append("UserAccountId=@UserAccountId,");
			strSql.Append("RelatedClient=@RelatedClient,");
			strSql.Append("RelatedProject=@RelatedProject,");
			strSql.Append("AuditOpinion=@AuditOpinion,");
			strSql.Append("State=@State,");
			strSql.Append("DealingPeopleAccountId=@DealingPeopleAccountId,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@CostTilte", SqlDbType.VarChar,4000),
					new SqlParameter("@CostCon", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@ApplicationDate", SqlDbType.DateTime),
					new SqlParameter("@Amount", SqlDbType.Float,8),
					new SqlParameter("@CostType", SqlDbType.Int,4),
					new SqlParameter("@BillNumber", SqlDbType.Int,4),
					new SqlParameter("@ApplicantName", SqlDbType.VarChar,4000),
					new SqlParameter("@UserAccountId", SqlDbType.Int,4),
					new SqlParameter("@RelatedClient", SqlDbType.VarChar,4000),
					new SqlParameter("@RelatedProject", SqlDbType.VarChar,4000),
					new SqlParameter("@AuditOpinion", SqlDbType.VarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@DealingPeopleAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.CostTilte;
			parameters[1].Value = model.CostCon;
			parameters[2].Value = model.Remarks;
			parameters[3].Value = model.ApplicationDate;
			parameters[4].Value = model.Amount;
			parameters[5].Value = model.CostType;
			parameters[6].Value = model.BillNumber;
			parameters[7].Value = model.ApplicantName;
			parameters[8].Value = model.UserAccountId;
			parameters[9].Value = model.RelatedClient;
			parameters[10].Value = model.RelatedProject;
			parameters[11].Value = model.AuditOpinion;
			parameters[12].Value = model.State;
			parameters[13].Value = model.DealingPeopleAccountId;
			parameters[14].Value = model.CreateAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.TenantId;
			parameters[17].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ReimbursementExpenses ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ReimbursementExpenses ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ReimbursementExpenses GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,CostTilte,CostCon,Remarks,ApplicationDate,Amount,CostType,BillNumber,ApplicantName,UserAccountId,RelatedClient,RelatedProject,AuditOpinion,State,DealingPeopleAccountId,CreateAccountId,CreateTime,TenantId from ReimbursementExpenses ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.ReimbursementExpenses model=new XinYiOffice.Model.ReimbursementExpenses();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CostTilte"]!=null && ds.Tables[0].Rows[0]["CostTilte"].ToString()!="")
				{
					model.CostTilte=ds.Tables[0].Rows[0]["CostTilte"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CostCon"]!=null && ds.Tables[0].Rows[0]["CostCon"].ToString()!="")
				{
					model.CostCon=ds.Tables[0].Rows[0]["CostCon"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ApplicationDate"]!=null && ds.Tables[0].Rows[0]["ApplicationDate"].ToString()!="")
				{
					model.ApplicationDate=DateTime.Parse(ds.Tables[0].Rows[0]["ApplicationDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Amount"]!=null && ds.Tables[0].Rows[0]["Amount"].ToString()!="")
				{
					model.Amount=decimal.Parse(ds.Tables[0].Rows[0]["Amount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CostType"]!=null && ds.Tables[0].Rows[0]["CostType"].ToString()!="")
				{
					model.CostType=int.Parse(ds.Tables[0].Rows[0]["CostType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BillNumber"]!=null && ds.Tables[0].Rows[0]["BillNumber"].ToString()!="")
				{
					model.BillNumber=int.Parse(ds.Tables[0].Rows[0]["BillNumber"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ApplicantName"]!=null && ds.Tables[0].Rows[0]["ApplicantName"].ToString()!="")
				{
					model.ApplicantName=ds.Tables[0].Rows[0]["ApplicantName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UserAccountId"]!=null && ds.Tables[0].Rows[0]["UserAccountId"].ToString()!="")
				{
					model.UserAccountId=int.Parse(ds.Tables[0].Rows[0]["UserAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RelatedClient"]!=null && ds.Tables[0].Rows[0]["RelatedClient"].ToString()!="")
				{
					model.RelatedClient=ds.Tables[0].Rows[0]["RelatedClient"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RelatedProject"]!=null && ds.Tables[0].Rows[0]["RelatedProject"].ToString()!="")
				{
					model.RelatedProject=ds.Tables[0].Rows[0]["RelatedProject"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AuditOpinion"]!=null && ds.Tables[0].Rows[0]["AuditOpinion"].ToString()!="")
				{
					model.AuditOpinion=ds.Tables[0].Rows[0]["AuditOpinion"].ToString();
				}
				if(ds.Tables[0].Rows[0]["State"]!=null && ds.Tables[0].Rows[0]["State"].ToString()!="")
				{
					model.State=int.Parse(ds.Tables[0].Rows[0]["State"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DealingPeopleAccountId"]!=null && ds.Tables[0].Rows[0]["DealingPeopleAccountId"].ToString()!="")
				{
					model.DealingPeopleAccountId=int.Parse(ds.Tables[0].Rows[0]["DealingPeopleAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,CostTilte,CostCon,Remarks,ApplicationDate,Amount,CostType,BillNumber,ApplicantName,UserAccountId,RelatedClient,RelatedProject,AuditOpinion,State,DealingPeopleAccountId,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM ReimbursementExpenses ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,CostTilte,CostCon,Remarks,ApplicationDate,Amount,CostType,BillNumber,ApplicantName,UserAccountId,RelatedClient,RelatedProject,AuditOpinion,State,DealingPeopleAccountId,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM ReimbursementExpenses ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ReimbursementExpenses ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from ReimbursementExpenses T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ReimbursementExpenses";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

