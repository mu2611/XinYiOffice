﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:Department
	/// </summary>
	public partial class Department
	{
		public Department()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "Department"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Department");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.Department model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Department(");
			strSql.Append("DepartmentName,DepartmentCon,InstitutionId,TorchbearerAccountId,PhoneCode,PhoneCode2,Fax,ZipCode,Address,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@DepartmentName,@DepartmentCon,@InstitutionId,@TorchbearerAccountId,@PhoneCode,@PhoneCode2,@Fax,@ZipCode,@Address,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@DepartmentName", SqlDbType.VarChar,4000),
					new SqlParameter("@DepartmentCon", SqlDbType.VarChar,4000),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@TorchbearerAccountId", SqlDbType.Int,4),
					new SqlParameter("@PhoneCode", SqlDbType.VarChar,4000),
					new SqlParameter("@PhoneCode2", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.DepartmentName;
			parameters[1].Value = model.DepartmentCon;
			parameters[2].Value = model.InstitutionId;
			parameters[3].Value = model.TorchbearerAccountId;
			parameters[4].Value = model.PhoneCode;
			parameters[5].Value = model.PhoneCode2;
			parameters[6].Value = model.Fax;
			parameters[7].Value = model.ZipCode;
			parameters[8].Value = model.Address;
			parameters[9].Value = model.CreateAccountId;
			parameters[10].Value = model.RefreshAccountId;
			parameters[11].Value = model.CreateTime;
			parameters[12].Value = model.RefreshTime;
			parameters[13].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.Department model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Department set ");
			strSql.Append("DepartmentName=@DepartmentName,");
			strSql.Append("DepartmentCon=@DepartmentCon,");
			strSql.Append("InstitutionId=@InstitutionId,");
			strSql.Append("TorchbearerAccountId=@TorchbearerAccountId,");
			strSql.Append("PhoneCode=@PhoneCode,");
			strSql.Append("PhoneCode2=@PhoneCode2,");
			strSql.Append("Fax=@Fax,");
			strSql.Append("ZipCode=@ZipCode,");
			strSql.Append("Address=@Address,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@DepartmentName", SqlDbType.VarChar,4000),
					new SqlParameter("@DepartmentCon", SqlDbType.VarChar,4000),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@TorchbearerAccountId", SqlDbType.Int,4),
					new SqlParameter("@PhoneCode", SqlDbType.VarChar,4000),
					new SqlParameter("@PhoneCode2", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.DepartmentName;
			parameters[1].Value = model.DepartmentCon;
			parameters[2].Value = model.InstitutionId;
			parameters[3].Value = model.TorchbearerAccountId;
			parameters[4].Value = model.PhoneCode;
			parameters[5].Value = model.PhoneCode2;
			parameters[6].Value = model.Fax;
			parameters[7].Value = model.ZipCode;
			parameters[8].Value = model.Address;
			parameters[9].Value = model.CreateAccountId;
			parameters[10].Value = model.RefreshAccountId;
			parameters[11].Value = model.CreateTime;
			parameters[12].Value = model.RefreshTime;
			parameters[13].Value = model.TenantId;
			parameters[14].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Department ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Department ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.Department GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,DepartmentName,DepartmentCon,InstitutionId,TorchbearerAccountId,PhoneCode,PhoneCode2,Fax,ZipCode,Address,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from Department ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.Department model=new XinYiOffice.Model.Department();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DepartmentName"]!=null && ds.Tables[0].Rows[0]["DepartmentName"].ToString()!="")
				{
					model.DepartmentName=ds.Tables[0].Rows[0]["DepartmentName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["DepartmentCon"]!=null && ds.Tables[0].Rows[0]["DepartmentCon"].ToString()!="")
				{
					model.DepartmentCon=ds.Tables[0].Rows[0]["DepartmentCon"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InstitutionId"]!=null && ds.Tables[0].Rows[0]["InstitutionId"].ToString()!="")
				{
					model.InstitutionId=int.Parse(ds.Tables[0].Rows[0]["InstitutionId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TorchbearerAccountId"]!=null && ds.Tables[0].Rows[0]["TorchbearerAccountId"].ToString()!="")
				{
					model.TorchbearerAccountId=int.Parse(ds.Tables[0].Rows[0]["TorchbearerAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PhoneCode"]!=null && ds.Tables[0].Rows[0]["PhoneCode"].ToString()!="")
				{
					model.PhoneCode=ds.Tables[0].Rows[0]["PhoneCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PhoneCode2"]!=null && ds.Tables[0].Rows[0]["PhoneCode2"].ToString()!="")
				{
					model.PhoneCode2=ds.Tables[0].Rows[0]["PhoneCode2"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Fax"]!=null && ds.Tables[0].Rows[0]["Fax"].ToString()!="")
				{
					model.Fax=ds.Tables[0].Rows[0]["Fax"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ZipCode"]!=null && ds.Tables[0].Rows[0]["ZipCode"].ToString()!="")
				{
					model.ZipCode=ds.Tables[0].Rows[0]["ZipCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Address"]!=null && ds.Tables[0].Rows[0]["Address"].ToString()!="")
				{
					model.Address=ds.Tables[0].Rows[0]["Address"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,DepartmentName,DepartmentCon,InstitutionId,TorchbearerAccountId,PhoneCode,PhoneCode2,Fax,ZipCode,Address,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Department ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,DepartmentName,DepartmentCon,InstitutionId,TorchbearerAccountId,PhoneCode,PhoneCode2,Fax,ZipCode,Address,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Department ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Department ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from Department T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Department";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

