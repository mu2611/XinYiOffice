﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:MarketingPlan
	/// </summary>
	public partial class MarketingPlan
	{
		public MarketingPlan()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "MarketingPlan"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from MarketingPlan");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.MarketingPlan model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into MarketingPlan(");
			strSql.Append("ProgramName,ProgramDescription,Sate,StartTime,EndTime,TYPE,AnticipatedRevenue,BudgetCost,SchemerAccountId,ExecuteAccountId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ProgramName,@ProgramDescription,@Sate,@StartTime,@EndTime,@TYPE,@AnticipatedRevenue,@BudgetCost,@SchemerAccountId,@ExecuteAccountId,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProgramName", SqlDbType.VarChar,4000),
					new SqlParameter("@ProgramDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@TYPE", SqlDbType.Int,4),
					new SqlParameter("@AnticipatedRevenue", SqlDbType.Float,8),
					new SqlParameter("@BudgetCost", SqlDbType.Float,8),
					new SqlParameter("@SchemerAccountId", SqlDbType.Int,4),
					new SqlParameter("@ExecuteAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ProgramName;
			parameters[1].Value = model.ProgramDescription;
			parameters[2].Value = model.Sate;
			parameters[3].Value = model.StartTime;
			parameters[4].Value = model.EndTime;
			parameters[5].Value = model.TYPE;
			parameters[6].Value = model.AnticipatedRevenue;
			parameters[7].Value = model.BudgetCost;
			parameters[8].Value = model.SchemerAccountId;
			parameters[9].Value = model.ExecuteAccountId;
			parameters[10].Value = model.CreateAccountId;
			parameters[11].Value = model.RefreshAccountId;
			parameters[12].Value = model.CreateTime;
			parameters[13].Value = model.RefreshTime;
			parameters[14].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.MarketingPlan model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update MarketingPlan set ");
			strSql.Append("ProgramName=@ProgramName,");
			strSql.Append("ProgramDescription=@ProgramDescription,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("StartTime=@StartTime,");
			strSql.Append("EndTime=@EndTime,");
			strSql.Append("TYPE=@TYPE,");
			strSql.Append("AnticipatedRevenue=@AnticipatedRevenue,");
			strSql.Append("BudgetCost=@BudgetCost,");
			strSql.Append("SchemerAccountId=@SchemerAccountId,");
			strSql.Append("ExecuteAccountId=@ExecuteAccountId,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ProgramName", SqlDbType.VarChar,4000),
					new SqlParameter("@ProgramDescription", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@TYPE", SqlDbType.Int,4),
					new SqlParameter("@AnticipatedRevenue", SqlDbType.Float,8),
					new SqlParameter("@BudgetCost", SqlDbType.Float,8),
					new SqlParameter("@SchemerAccountId", SqlDbType.Int,4),
					new SqlParameter("@ExecuteAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ProgramName;
			parameters[1].Value = model.ProgramDescription;
			parameters[2].Value = model.Sate;
			parameters[3].Value = model.StartTime;
			parameters[4].Value = model.EndTime;
			parameters[5].Value = model.TYPE;
			parameters[6].Value = model.AnticipatedRevenue;
			parameters[7].Value = model.BudgetCost;
			parameters[8].Value = model.SchemerAccountId;
			parameters[9].Value = model.ExecuteAccountId;
			parameters[10].Value = model.CreateAccountId;
			parameters[11].Value = model.RefreshAccountId;
			parameters[12].Value = model.CreateTime;
			parameters[13].Value = model.RefreshTime;
			parameters[14].Value = model.TenantId;
			parameters[15].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from MarketingPlan ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from MarketingPlan ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.MarketingPlan GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ProgramName,ProgramDescription,Sate,StartTime,EndTime,TYPE,AnticipatedRevenue,BudgetCost,SchemerAccountId,ExecuteAccountId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from MarketingPlan ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.MarketingPlan model=new XinYiOffice.Model.MarketingPlan();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProgramName"]!=null && ds.Tables[0].Rows[0]["ProgramName"].ToString()!="")
				{
					model.ProgramName=ds.Tables[0].Rows[0]["ProgramName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProgramDescription"]!=null && ds.Tables[0].Rows[0]["ProgramDescription"].ToString()!="")
				{
					model.ProgramDescription=ds.Tables[0].Rows[0]["ProgramDescription"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartTime"]!=null && ds.Tables[0].Rows[0]["StartTime"].ToString()!="")
				{
					model.StartTime=DateTime.Parse(ds.Tables[0].Rows[0]["StartTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndTime"]!=null && ds.Tables[0].Rows[0]["EndTime"].ToString()!="")
				{
					model.EndTime=DateTime.Parse(ds.Tables[0].Rows[0]["EndTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TYPE"]!=null && ds.Tables[0].Rows[0]["TYPE"].ToString()!="")
				{
					model.TYPE=int.Parse(ds.Tables[0].Rows[0]["TYPE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AnticipatedRevenue"]!=null && ds.Tables[0].Rows[0]["AnticipatedRevenue"].ToString()!="")
				{
					model.AnticipatedRevenue=decimal.Parse(ds.Tables[0].Rows[0]["AnticipatedRevenue"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BudgetCost"]!=null && ds.Tables[0].Rows[0]["BudgetCost"].ToString()!="")
				{
					model.BudgetCost=decimal.Parse(ds.Tables[0].Rows[0]["BudgetCost"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SchemerAccountId"]!=null && ds.Tables[0].Rows[0]["SchemerAccountId"].ToString()!="")
				{
					model.SchemerAccountId=int.Parse(ds.Tables[0].Rows[0]["SchemerAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExecuteAccountId"]!=null && ds.Tables[0].Rows[0]["ExecuteAccountId"].ToString()!="")
				{
					model.ExecuteAccountId=int.Parse(ds.Tables[0].Rows[0]["ExecuteAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ProgramName,ProgramDescription,Sate,StartTime,EndTime,TYPE,AnticipatedRevenue,BudgetCost,SchemerAccountId,ExecuteAccountId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM MarketingPlan ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ProgramName,ProgramDescription,Sate,StartTime,EndTime,TYPE,AnticipatedRevenue,BudgetCost,SchemerAccountId,ExecuteAccountId,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM MarketingPlan ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM MarketingPlan ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from MarketingPlan T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "MarketingPlan";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

