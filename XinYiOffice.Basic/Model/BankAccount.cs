﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 银行帐号
	/// </summary>
	[Serializable]
	public partial class BankAccount
	{
		public BankAccount()
		{}
		#region Model
		private int _id;
		private string _bankinfo;
		private string _bankaccountname;
		private string _bankaccountnumber;
		private string _bankaccounttel;
		private decimal? _cashin;
		private int? _sate;
		private DateTime? _openingtime;
		private int? _institutionid;
		private int? _departmentid;
		private decimal? _storageamount;
		private int? _beareraccountid;
		private int? _accounttype;
		private int? _accountpurpose;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 开户行信息 银行名称及地址
		/// </summary>
		public string BankInfo
		{
			set{ _bankinfo=value;}
			get{return _bankinfo;}
		}
		/// <summary>
		/// 户名
		/// </summary>
		public string BankAccountName
		{
			set{ _bankaccountname=value;}
			get{return _bankaccountname;}
		}
		/// <summary>
		/// 银行帐号
		/// </summary>
		public string BankAccountNumber
		{
			set{ _bankaccountnumber=value;}
			get{return _bankaccountnumber;}
		}
		/// <summary>
		/// 联系手机号 用于接收银行或网银短信
		/// </summary>
		public string BankAccountTel
		{
			set{ _bankaccounttel=value;}
			get{return _bankaccounttel;}
		}
		/// <summary>
		/// 现有金额
		/// </summary>
		public decimal? CashIn
		{
			set{ _cashin=value;}
			get{return _cashin;}
		}
		/// <summary>
		/// 账户状态 1-正常,2-冻结
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 开户时间
		/// </summary>
		public DateTime? OpeningTime
		{
			set{ _openingtime=value;}
			get{return _openingtime;}
		}
		/// <summary>
		/// 所属机构
		/// </summary>
		public int? InstitutionId
		{
			set{ _institutionid=value;}
			get{return _institutionid;}
		}
		/// <summary>
		/// 所属部门
		/// </summary>
		public int? DepartmentId
		{
			set{ _departmentid=value;}
			get{return _departmentid;}
		}
		/// <summary>
		/// 入库时金额 新建后此字段一般都不做更改
		/// </summary>
		public decimal? StorageAmount
		{
			set{ _storageamount=value;}
			get{return _storageamount;}
		}
		/// <summary>
		/// 帐号持有人
		/// </summary>
		public int? BearerAccountId
		{
			set{ _beareraccountid=value;}
			get{return _beareraccountid;}
		}
		/// <summary>
		/// 帐号类型 1-对公,2-个人账户,3-信用卡,4-支付宝
		/// </summary>
		public int? AccountType
		{
			set{ _accounttype=value;}
			get{return _accounttype;}
		}
		/// <summary>
		/// 帐号用途 1-对外贸易,2-工资帐号,3-福利卡
		/// </summary>
		public int? AccountPurpose
		{
			set{ _accountpurpose=value;}
			get{return _accountpurpose;}
		}
		/// <summary>
		/// 帐号录入人
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

