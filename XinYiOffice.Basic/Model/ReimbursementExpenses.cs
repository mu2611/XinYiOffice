﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 费用报销 在公司经营中的一些成本，员工招待费等。
	/// </summary>
	[Serializable]
	public partial class ReimbursementExpenses
	{
		public ReimbursementExpenses()
		{}
		#region Model
		private int _id;
		private string _costtilte;
		private string _costcon;
		private string _remarks;
		private DateTime? _applicationdate;
		private decimal? _amount;
		private int? _costtype;
		private int? _billnumber;
		private string _applicantname;
		private int? _useraccountid;
		private string _relatedclient;
		private string _relatedproject;
		private string _auditopinion;
		private int? _state;
		private int? _dealingpeopleaccountid;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 费用主题
		/// </summary>
		public string CostTilte
		{
			set{ _costtilte=value;}
			get{return _costtilte;}
		}
		/// <summary>
		/// 内容描述
		/// </summary>
		public string CostCon
		{
			set{ _costcon=value;}
			get{return _costcon;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 申请日期
		/// </summary>
		public DateTime? ApplicationDate
		{
			set{ _applicationdate=value;}
			get{return _applicationdate;}
		}
		/// <summary>
		/// 费用金额
		/// </summary>
		public decimal? Amount
		{
			set{ _amount=value;}
			get{return _amount;}
		}
		/// <summary>
		/// 费用类型 1-招待费,2-交通费,3-会议费,4-物件费
		/// </summary>
		public int? CostType
		{
			set{ _costtype=value;}
			get{return _costtype;}
		}
		/// <summary>
		/// 票据张数
		/// </summary>
		public int? BillNumber
		{
			set{ _billnumber=value;}
			get{return _billnumber;}
		}
		/// <summary>
		/// 申请人姓名
		/// </summary>
		public string ApplicantName
		{
			set{ _applicantname=value;}
			get{return _applicantname;}
		}
		/// <summary>
		/// 申请人用户帐号 系统中帐号
		/// </summary>
		public int? UserAccountId
		{
			set{ _useraccountid=value;}
			get{return _useraccountid;}
		}
		/// <summary>
		/// 相关客户
		/// </summary>
		public string RelatedClient
		{
			set{ _relatedclient=value;}
			get{return _relatedclient;}
		}
		/// <summary>
		/// 相关项目
		/// </summary>
		public string RelatedProject
		{
			set{ _relatedproject=value;}
			get{return _relatedproject;}
		}
		/// <summary>
		/// 审核意见
		/// </summary>
		public string AuditOpinion
		{
			set{ _auditopinion=value;}
			get{return _auditopinion;}
		}
		/// <summary>
		/// 状态 1-待审批,2-通过,3-驳回
		/// </summary>
		public int? State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 处理人
		/// </summary>
		public int? DealingPeopleAccountId
		{
			set{ _dealingpeopleaccountid=value;}
			get{return _dealingpeopleaccountid;}
		}
		/// <summary>
		/// 创建账户
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 创建日期
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

