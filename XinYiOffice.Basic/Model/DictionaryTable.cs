﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 字典表_分组
	/// </summary>
	[Serializable]
	public partial class DictionaryTable
	{
		public DictionaryTable()
		{}
		#region Model
		private int _id;
		private string _name;
		private string _con;
		private string _tablename;
		private string _fieldname;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 字典总名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 说明
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 表名
		/// </summary>
		public string TableName
		{
			set{ _tablename=value;}
			get{return _tablename;}
		}
		/// <summary>
		/// 字段名 与TableName约束成为主键
		/// </summary>
		public string FieldName
		{
			set{ _fieldname=value;}
			get{return _fieldname;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

