﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 菜单管理 此表作为b/s系统的所有页面的菜单名称及页面名称
	/// </summary>
	[Serializable]
	public partial class MenuTree
	{
		public MenuTree()
		{}
		#region Model
		private int _id;
		private string _guid;
		private string _name;
		private int? _menutreeid;
		private string _chainedaddress;
		private int? _isshow;
		private int? _sort;
		private string _icon;
		private string _iconmax;
		private string _description;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 唯一签名 用于系统内部判断读取使用,可采取GUID或其他英文命名方式
		/// </summary>
		public string Guid
		{
			set{ _guid=value;}
			get{return _guid;}
		}
		/// <summary>
		/// 菜单名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 上级ID
		/// </summary>
		public int? MenuTreeId
		{
			set{ _menutreeid=value;}
			get{return _menutreeid;}
		}
		/// <summary>
		/// 链接地址
		/// </summary>
		public string ChainedAddress
		{
			set{ _chainedaddress=value;}
			get{return _chainedaddress;}
		}
		/// <summary>
		/// 是否显示
		/// </summary>
		public int? IsShow
		{
			set{ _isshow=value;}
			get{return _isshow;}
		}
		/// <summary>
		/// 排序
		/// </summary>
		public int? Sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 菜单小图标
		/// </summary>
		public string Icon
		{
			set{ _icon=value;}
			get{return _icon;}
		}
		/// <summary>
		/// 菜单大图标 在显示桌面快捷方式等模块使用。
		/// </summary>
		public string IconMax
		{
			set{ _iconmax=value;}
			get{return _iconmax;}
		}
		/// <summary>
		/// 菜单说明
		/// </summary>
		public string Description
		{
			set{ _description=value;}
			get{return _description;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

