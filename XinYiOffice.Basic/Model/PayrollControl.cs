﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 工资管理
	/// </summary>
	[Serializable]
	public partial class PayrollControl
	{
		public PayrollControl()
		{}
		#region Model
		private int _id;
		private DateTime? _paydate;
		private decimal? _basepay;
		private decimal? _attendancewages;
		private decimal? _communicationexpense;
		private decimal? _personalincometax;
		private decimal? _housingfund;
		private decimal? _residualamount;
		private decimal? _allwages;
		private decimal? _overtimewage;
		private decimal? _percentagewages;
		private decimal? _otherbenefits;
		private decimal? _socialsecurity;
		private decimal? _deductmoney;
		private decimal? _repaymentamount;
		private decimal? _actualpayment;
		private int? _bankaccountid;
		private int? _personnelaccount;
		private int? _sate;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 工资年月日期
		/// </summary>
		public DateTime? PayDate
		{
			set{ _paydate=value;}
			get{return _paydate;}
		}
		/// <summary>
		/// 基本工资
		/// </summary>
		public decimal? BasePay
		{
			set{ _basepay=value;}
			get{return _basepay;}
		}
		/// <summary>
		/// 考勤工资
		/// </summary>
		public decimal? AttendanceWages
		{
			set{ _attendancewages=value;}
			get{return _attendancewages;}
		}
		/// <summary>
		/// 通讯费
		/// </summary>
		public decimal? CommunicationExpense
		{
			set{ _communicationexpense=value;}
			get{return _communicationexpense;}
		}
		/// <summary>
		/// 个人所得税
		/// </summary>
		public decimal? PersonalIncomeTax
		{
			set{ _personalincometax=value;}
			get{return _personalincometax;}
		}
		/// <summary>
		/// 住房公积金
		/// </summary>
		public decimal? HousingFund
		{
			set{ _housingfund=value;}
			get{return _housingfund;}
		}
		/// <summary>
		/// 剩余还款金额
		/// </summary>
		public decimal? ResidualAmount
		{
			set{ _residualamount=value;}
			get{return _residualamount;}
		}
		/// <summary>
		/// 全部工资
		/// </summary>
		public decimal? AllWages
		{
			set{ _allwages=value;}
			get{return _allwages;}
		}
		/// <summary>
		/// 加班工资
		/// </summary>
		public decimal? OvertimeWage
		{
			set{ _overtimewage=value;}
			get{return _overtimewage;}
		}
		/// <summary>
		/// 提成工资
		/// </summary>
		public decimal? PercentageWages
		{
			set{ _percentagewages=value;}
			get{return _percentagewages;}
		}
		/// <summary>
		/// 其他补助
		/// </summary>
		public decimal? OtherBenefits
		{
			set{ _otherbenefits=value;}
			get{return _otherbenefits;}
		}
		/// <summary>
		/// 社保
		/// </summary>
		public decimal? SocialSecurity
		{
			set{ _socialsecurity=value;}
			get{return _socialsecurity;}
		}
		/// <summary>
		/// 扣钱
		/// </summary>
		public decimal? DeductMoney
		{
			set{ _deductmoney=value;}
			get{return _deductmoney;}
		}
		/// <summary>
		/// 本次抵扣还款金额
		/// </summary>
		public decimal? RepaymentAmount
		{
			set{ _repaymentamount=value;}
			get{return _repaymentamount;}
		}
		/// <summary>
		/// 实际发放工资
		/// </summary>
		public decimal? ActualPayment
		{
			set{ _actualpayment=value;}
			get{return _actualpayment;}
		}
		/// <summary>
		/// 支付账户
		/// </summary>
		public int? BankAccountId
		{
			set{ _bankaccountid=value;}
			get{return _bankaccountid;}
		}
		/// <summary>
		/// 人员帐号
		/// </summary>
		public int? PersonnelAccount
		{
			set{ _personnelaccount=value;}
			get{return _personnelaccount;}
		}
		/// <summary>
		/// 工资状态 1-已发,2-未发
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 工资单创建人
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

