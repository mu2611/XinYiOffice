﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 自定义显示表 暂未使用
	/// </summary>
	[Serializable]
	public partial class CustomDisplay
	{
		public CustomDisplay()
		{}
		#region Model
		private int _id;
		private string _keytablename;
		private string _displayname;
		private string _fieldname;
		private int? _sort;
		private int? _tenantid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属表名
		/// </summary>
		public string KeyTableName
		{
			set{ _keytablename=value;}
			get{return _keytablename;}
		}
		/// <summary>
		/// 显示名称
		/// </summary>
		public string DisplayName
		{
			set{ _displayname=value;}
			get{return _displayname;}
		}
		/// <summary>
		/// 字段名
		/// </summary>
		public string FieldName
		{
			set{ _fieldname=value;}
			get{return _fieldname;}
		}
		/// <summary>
		/// 排序
		/// </summary>
		public int? Sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		#endregion Model

	}
}

