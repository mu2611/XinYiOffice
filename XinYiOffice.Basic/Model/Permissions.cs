﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 权限表
	/// </summary>
	[Serializable]
	public partial class Permissions
	{
		public Permissions()
		{}
		#region Model
		private int _id;
		private string _name;
		private string _sign;
		private int? _permissioncategoriesid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 权限id
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 权限名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 系统内签名 比如"CONTENT_ARTICLE_ADD"即为内容模块的，文章添加功能
		/// </summary>
		public string Sign
		{
			set{ _sign=value;}
			get{return _sign;}
		}
		/// <summary>
		/// 权限所属分类
		/// </summary>
		public int? PermissionCategoriesId
		{
			set{ _permissioncategoriesid=value;}
			get{return _permissioncategoriesid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

