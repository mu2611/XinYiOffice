﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 搜索项
	/// </summary>
	[Serializable]
	public partial class SearchConfigItem
	{
		public SearchConfigItem()
		{}
		#region Model
		private int _id;
		private string _displayname;
		private string _fieldname;
		private int? _searchdatasoureid;
		private int? _sort;
		private int? _valuetype;
		private string _valuerelchar;
		private string _prefixchar;
		private string _prefixrelchar;
		private string _suffixchar;
		private string _suffixrelchar;
		private int? _searchconfigid;
		private int? _usevaluetype;
		private string _defaultvalue;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 显示字段名称 如名称
		/// </summary>
		public string DisplayName
		{
			set{ _displayname=value;}
			get{return _displayname;}
		}
		/// <summary>
		/// 数据库字段名 如Name
		/// </summary>
		public string FieldName
		{
			set{ _fieldname=value;}
			get{return _fieldname;}
		}
		/// <summary>
		/// 表单数据来源 使用数据源id
		/// </summary>
		public int? SearchDataSoureId
		{
			set{ _searchdatasoureid=value;}
			get{return _searchdatasoureid;}
		}
		/// <summary>
		/// 搜索项排序 1-100,搜索项的排序
		/// </summary>
		public int? Sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 值类型 string为='1',int为=1
		/// </summary>
		public int? ValueType
		{
			set{ _valuetype=value;}
			get{return _valuetype;}
		}
		/// <summary>
		/// 与值的关系 =,in,like,<=,>=
		/// </summary>
		public string ValueRelChar
		{
			set{ _valuerelchar=value;}
			get{return _valuerelchar;}
		}
		/// <summary>
		/// 前置字符 之前字符串
		/// </summary>
		public string PrefixChar
		{
			set{ _prefixchar=value;}
			get{return _prefixchar;}
		}
		/// <summary>
		/// 前置关系 or and ()
		/// </summary>
		public string PrefixRelChar
		{
			set{ _prefixrelchar=value;}
			get{return _prefixrelchar;}
		}
		/// <summary>
		/// 后置字符 之后字符串
		/// </summary>
		public string SuffixChar
		{
			set{ _suffixchar=value;}
			get{return _suffixchar;}
		}
		/// <summary>
		/// 后置关系 与之后项的关系
		/// </summary>
		public string SuffixRelChar
		{
			set{ _suffixrelchar=value;}
			get{return _suffixrelchar;}
		}
		/// <summary>
		/// 所属的搜索配置 所属配置
		/// </summary>
		public int? SearchConfigId
		{
			set{ _searchconfigid=value;}
			get{return _searchconfigid;}
		}
		/// <summary>
		/// 采用值的txt还是value 1=text,2=value
		/// </summary>
		public int? UseValueType
		{
			set{ _usevaluetype=value;}
			get{return _usevaluetype;}
		}
		/// <summary>
		/// 显示的默认值 搜索可以有默认值
		/// </summary>
		public string DefaultValue
		{
			set{ _defaultvalue=value;}
			get{return _defaultvalue;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

