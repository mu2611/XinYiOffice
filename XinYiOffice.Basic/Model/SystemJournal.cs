﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 系统日志
	/// </summary>
	[Serializable]
	public partial class SystemJournal
	{
		public SystemJournal()
		{}
		#region Model
		private int _id;
		private int? _type;
		private string _operationcontent;
		private int? _currentaccountid;
		private string _ipaddress;
		private string _systeminfo;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 日志类型 1-系统日志,3-错误日志,2-用户日志
		/// </summary>
		public int? TYPE
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 操作内容
		/// </summary>
		public string OperationContent
		{
			set{ _operationcontent=value;}
			get{return _operationcontent;}
		}
		/// <summary>
		/// 当前用户id
		/// </summary>
		public int? CurrentAccountId
		{
			set{ _currentaccountid=value;}
			get{return _currentaccountid;}
		}
		/// <summary>
		/// 当前IP地址
		/// </summary>
		public string IPAddress
		{
			set{ _ipaddress=value;}
			get{return _ipaddress;}
		}
		/// <summary>
		/// 系统信息
		/// </summary>
		public string SystemInfo
		{
			set{ _systeminfo=value;}
			get{return _systeminfo;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

