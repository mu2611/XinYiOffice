﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 本地租户信息表
	/// </summary>
	[Serializable]
	public partial class LocalTenants
	{
		public LocalTenants()
		{}
		#region Model
		private int _id;
		private string _tenantguid;
		private string _name;
		private string _nameen;
		private string _namecn;
		private string _email;
		private string _remarks;
		private int? _state;
		private int? _createuserid;
		private int? _upuserid;
		private DateTime? _uptime;
		private DateTime? _createtime;
		/// <summary>
		/// 租户id
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 租户全局唯一序号
		/// </summary>
		public string TenantGuid
		{
			set{ _tenantguid=value;}
			get{return _tenantguid;}
		}
		/// <summary>
		/// 域名_唯一性
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 组织名_英文
		/// </summary>
		public string NameEn
		{
			set{ _nameen=value;}
			get{return _nameen;}
		}
		/// <summary>
		/// 组织名_中文
		/// </summary>
		public string NameCn
		{
			set{ _namecn=value;}
			get{return _namecn;}
		}
		/// <summary>
		/// 电子邮箱
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 租户状态 0-未初始化,1-初始化完成,2-初始化失败,3-停用
		/// </summary>
		public int? State
		{
			set{ _state=value;}
			get{return _state;}
		}
		/// <summary>
		/// 创建者ID
		/// </summary>
		public int? CreateUserId
		{
			set{ _createuserid=value;}
			get{return _createuserid;}
		}
		/// <summary>
		/// 更新者ID
		/// </summary>
		public int? UpUserId
		{
			set{ _upuserid=value;}
			get{return _upuserid;}
		}
		/// <summary>
		/// 更新时间
		/// </summary>
		public DateTime? UpTime
		{
			set{ _uptime=value;}
			get{return _uptime;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		#endregion Model

	}
}

