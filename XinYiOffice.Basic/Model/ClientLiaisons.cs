﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户联络人
	/// </summary>
	[Serializable]
	public partial class ClientLiaisons
	{
		public ClientLiaisons()
		{}
		#region Model
		private int _id;
		private int? _clientinfoid;
		private string _personnumber;
		private string _fullname;
		private int? _power;
		private string _phone;
		private string _otherphone;
		private string _fax;
		private string _email;
		private string _qq;
		private string _birthday;
		private int? _byaccountid;
		private string _department;
		private string _functionname;
		private string _directsuperior_bk2_;
		private int? _source;
		private int? _ifcontact;
		private int? _classification;
		private string _business;
		private int? _primarycontact;
		private string _shortphone;
		private int? _sex;
		private string _countriesregions;
		private string _zip;
		private string _address;
		private int? _province;
		private int? _county;
		private string _remarks;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _directsuperior;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属客户ID
		/// </summary>
		public int? ClientInfoId
		{
			set{ _clientinfoid=value;}
			get{return _clientinfoid;}
		}
		/// <summary>
		/// 联系人编号
		/// </summary>
		public string PersonNumber
		{
			set{ _personnumber=value;}
			get{return _personnumber;}
		}
		/// <summary>
		/// 联系人姓名
		/// </summary>
		public string FullName
		{
			set{ _fullname=value;}
			get{return _fullname;}
		}
		/// <summary>
		/// 权限
		/// </summary>
		public int? Power
		{
			set{ _power=value;}
			get{return _power;}
		}
		/// <summary>
		/// 手机
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 其他电话
		/// </summary>
		public string OtherPhone
		{
			set{ _otherphone=value;}
			get{return _otherphone;}
		}
		/// <summary>
		/// 传真
		/// </summary>
		public string Fax
		{
			set{ _fax=value;}
			get{return _fax;}
		}
		/// <summary>
		/// 电子邮件
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string QQ
		{
			set{ _qq=value;}
			get{return _qq;}
		}
		/// <summary>
		/// 生日
		/// </summary>
		public string Birthday
		{
			set{ _birthday=value;}
			get{return _birthday;}
		}
		/// <summary>
		/// 所属人员
		/// </summary>
		public int? ByAccountId
		{
			set{ _byaccountid=value;}
			get{return _byaccountid;}
		}
		/// <summary>
		/// 部门
		/// </summary>
		public string Department
		{
			set{ _department=value;}
			get{return _department;}
		}
		/// <summary>
		/// 职务
		/// </summary>
		public string FunctionName
		{
			set{ _functionname=value;}
			get{return _functionname;}
		}
		/// <summary>
		/// 直属上级
		/// </summary>
		public string DirectSuperior_BK2_
		{
			set{ _directsuperior_bk2_=value;}
			get{return _directsuperior_bk2_;}
		}
		/// <summary>
		/// 来源
		/// </summary>
		public int? Source
		{
			set{ _source=value;}
			get{return _source;}
		}
		/// <summary>
		/// 是否可以联系
		/// </summary>
		public int? IfContact
		{
			set{ _ifcontact=value;}
			get{return _ifcontact;}
		}
		/// <summary>
		/// 联系人分类
		/// </summary>
		public int? ClassiFication
		{
			set{ _classification=value;}
			get{return _classification;}
		}
		/// <summary>
		/// 负责业务
		/// </summary>
		public string Business
		{
			set{ _business=value;}
			get{return _business;}
		}
		/// <summary>
		/// 是否为主要联系人
		/// </summary>
		public int? PrimaryContact
		{
			set{ _primarycontact=value;}
			get{return _primarycontact;}
		}
		/// <summary>
		/// 接受短信手机
		/// </summary>
		public string ShortPhone
		{
			set{ _shortphone=value;}
			get{return _shortphone;}
		}
		/// <summary>
		/// 性别
		/// </summary>
		public int? Sex
		{
			set{ _sex=value;}
			get{return _sex;}
		}
		/// <summary>
		/// 国家地区
		/// </summary>
		public string CountriesRegions
		{
			set{ _countriesregions=value;}
			get{return _countriesregions;}
		}
		/// <summary>
		/// 邮编
		/// </summary>
		public string Zip
		{
			set{ _zip=value;}
			get{return _zip;}
		}
		/// <summary>
		/// 地址
		/// </summary>
		public string Address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 省份
		/// </summary>
		public int? Province
		{
			set{ _province=value;}
			get{return _province;}
		}
		/// <summary>
		/// 区县
		/// </summary>
		public int? County
		{
			set{ _county=value;}
			get{return _county;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DirectSuperior
		{
			set{ _directsuperior=value;}
			get{return _directsuperior;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

