﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 讨论区分类
	/// </summary>
	[Serializable]
	public partial class DiscussClass
	{
		public DiscussClass()
		{}
		#region Model
		private int _id;
		private string _name;
		private string _con;
		private string _discussclassid;
		private string _projectinfoid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 分类名
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 简介
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 上级分类ID
		/// </summary>
		public string DiscussClassId
		{
			set{ _discussclassid=value;}
			get{return _discussclassid;}
		}
		/// <summary>
		/// 所属项目ID
		/// </summary>
		public string ProjectInfoId
		{
			set{ _projectinfoid=value;}
			get{return _projectinfoid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

