﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 费用报销 在公司经营中的一些成本
	/// </summary>
	public partial class ReimbursementExpenses
	{
		private readonly XinYiOffice.DAL.ReimbursementExpenses dal=new XinYiOffice.DAL.ReimbursementExpenses();
		public ReimbursementExpenses()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.ReimbursementExpenses model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ReimbursementExpenses model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ReimbursementExpenses GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.ReimbursementExpenses GetModelByCache(int Id)
		{
			
			string CacheKey = "ReimbursementExpensesModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.ReimbursementExpenses)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ReimbursementExpenses> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ReimbursementExpenses> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.ReimbursementExpenses> modelList = new List<XinYiOffice.Model.ReimbursementExpenses>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.ReimbursementExpenses model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.ReimbursementExpenses();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["CostTilte"]!=null && dt.Rows[n]["CostTilte"].ToString()!="")
					{
					model.CostTilte=dt.Rows[n]["CostTilte"].ToString();
					}
					if(dt.Rows[n]["CostCon"]!=null && dt.Rows[n]["CostCon"].ToString()!="")
					{
					model.CostCon=dt.Rows[n]["CostCon"].ToString();
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["ApplicationDate"]!=null && dt.Rows[n]["ApplicationDate"].ToString()!="")
					{
						model.ApplicationDate=DateTime.Parse(dt.Rows[n]["ApplicationDate"].ToString());
					}
					if(dt.Rows[n]["Amount"]!=null && dt.Rows[n]["Amount"].ToString()!="")
					{
						model.Amount=decimal.Parse(dt.Rows[n]["Amount"].ToString());
					}
					if(dt.Rows[n]["CostType"]!=null && dt.Rows[n]["CostType"].ToString()!="")
					{
						model.CostType=int.Parse(dt.Rows[n]["CostType"].ToString());
					}
					if(dt.Rows[n]["BillNumber"]!=null && dt.Rows[n]["BillNumber"].ToString()!="")
					{
						model.BillNumber=int.Parse(dt.Rows[n]["BillNumber"].ToString());
					}
					if(dt.Rows[n]["ApplicantName"]!=null && dt.Rows[n]["ApplicantName"].ToString()!="")
					{
					model.ApplicantName=dt.Rows[n]["ApplicantName"].ToString();
					}
					if(dt.Rows[n]["UserAccountId"]!=null && dt.Rows[n]["UserAccountId"].ToString()!="")
					{
						model.UserAccountId=int.Parse(dt.Rows[n]["UserAccountId"].ToString());
					}
					if(dt.Rows[n]["RelatedClient"]!=null && dt.Rows[n]["RelatedClient"].ToString()!="")
					{
					model.RelatedClient=dt.Rows[n]["RelatedClient"].ToString();
					}
					if(dt.Rows[n]["RelatedProject"]!=null && dt.Rows[n]["RelatedProject"].ToString()!="")
					{
					model.RelatedProject=dt.Rows[n]["RelatedProject"].ToString();
					}
					if(dt.Rows[n]["AuditOpinion"]!=null && dt.Rows[n]["AuditOpinion"].ToString()!="")
					{
					model.AuditOpinion=dt.Rows[n]["AuditOpinion"].ToString();
					}
					if(dt.Rows[n]["State"]!=null && dt.Rows[n]["State"].ToString()!="")
					{
						model.State=int.Parse(dt.Rows[n]["State"].ToString());
					}
					if(dt.Rows[n]["DealingPeopleAccountId"]!=null && dt.Rows[n]["DealingPeopleAccountId"].ToString()!="")
					{
						model.DealingPeopleAccountId=int.Parse(dt.Rows[n]["DealingPeopleAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

