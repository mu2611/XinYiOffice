﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 工资管理
	/// </summary>
	public partial class PayrollControl
	{
		private readonly XinYiOffice.DAL.PayrollControl dal=new XinYiOffice.DAL.PayrollControl();
		public PayrollControl()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.PayrollControl model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.PayrollControl model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.PayrollControl GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.PayrollControl GetModelByCache(int Id)
		{
			
			string CacheKey = "PayrollControlModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.PayrollControl)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.PayrollControl> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.PayrollControl> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.PayrollControl> modelList = new List<XinYiOffice.Model.PayrollControl>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.PayrollControl model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.PayrollControl();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["PayDate"]!=null && dt.Rows[n]["PayDate"].ToString()!="")
					{
						model.PayDate=DateTime.Parse(dt.Rows[n]["PayDate"].ToString());
					}
					if(dt.Rows[n]["BasePay"]!=null && dt.Rows[n]["BasePay"].ToString()!="")
					{
						model.BasePay=decimal.Parse(dt.Rows[n]["BasePay"].ToString());
					}
					if(dt.Rows[n]["AttendanceWages"]!=null && dt.Rows[n]["AttendanceWages"].ToString()!="")
					{
						model.AttendanceWages=decimal.Parse(dt.Rows[n]["AttendanceWages"].ToString());
					}
					if(dt.Rows[n]["CommunicationExpense"]!=null && dt.Rows[n]["CommunicationExpense"].ToString()!="")
					{
						model.CommunicationExpense=decimal.Parse(dt.Rows[n]["CommunicationExpense"].ToString());
					}
					if(dt.Rows[n]["PersonalIncomeTax"]!=null && dt.Rows[n]["PersonalIncomeTax"].ToString()!="")
					{
						model.PersonalIncomeTax=decimal.Parse(dt.Rows[n]["PersonalIncomeTax"].ToString());
					}
					if(dt.Rows[n]["HousingFund"]!=null && dt.Rows[n]["HousingFund"].ToString()!="")
					{
						model.HousingFund=decimal.Parse(dt.Rows[n]["HousingFund"].ToString());
					}
					if(dt.Rows[n]["ResidualAmount"]!=null && dt.Rows[n]["ResidualAmount"].ToString()!="")
					{
						model.ResidualAmount=decimal.Parse(dt.Rows[n]["ResidualAmount"].ToString());
					}
					if(dt.Rows[n]["AllWages"]!=null && dt.Rows[n]["AllWages"].ToString()!="")
					{
						model.AllWages=decimal.Parse(dt.Rows[n]["AllWages"].ToString());
					}
					if(dt.Rows[n]["OvertimeWage"]!=null && dt.Rows[n]["OvertimeWage"].ToString()!="")
					{
						model.OvertimeWage=decimal.Parse(dt.Rows[n]["OvertimeWage"].ToString());
					}
					if(dt.Rows[n]["PercentageWages"]!=null && dt.Rows[n]["PercentageWages"].ToString()!="")
					{
						model.PercentageWages=decimal.Parse(dt.Rows[n]["PercentageWages"].ToString());
					}
					if(dt.Rows[n]["OtherBenefits"]!=null && dt.Rows[n]["OtherBenefits"].ToString()!="")
					{
						model.OtherBenefits=decimal.Parse(dt.Rows[n]["OtherBenefits"].ToString());
					}
					if(dt.Rows[n]["SocialSecurity"]!=null && dt.Rows[n]["SocialSecurity"].ToString()!="")
					{
						model.SocialSecurity=decimal.Parse(dt.Rows[n]["SocialSecurity"].ToString());
					}
					if(dt.Rows[n]["DeductMoney"]!=null && dt.Rows[n]["DeductMoney"].ToString()!="")
					{
						model.DeductMoney=decimal.Parse(dt.Rows[n]["DeductMoney"].ToString());
					}
					if(dt.Rows[n]["RepaymentAmount"]!=null && dt.Rows[n]["RepaymentAmount"].ToString()!="")
					{
						model.RepaymentAmount=decimal.Parse(dt.Rows[n]["RepaymentAmount"].ToString());
					}
					if(dt.Rows[n]["ActualPayment"]!=null && dt.Rows[n]["ActualPayment"].ToString()!="")
					{
						model.ActualPayment=decimal.Parse(dt.Rows[n]["ActualPayment"].ToString());
					}
					if(dt.Rows[n]["BankAccountId"]!=null && dt.Rows[n]["BankAccountId"].ToString()!="")
					{
						model.BankAccountId=int.Parse(dt.Rows[n]["BankAccountId"].ToString());
					}
					if(dt.Rows[n]["PersonnelAccount"]!=null && dt.Rows[n]["PersonnelAccount"].ToString()!="")
					{
						model.PersonnelAccount=int.Parse(dt.Rows[n]["PersonnelAccount"].ToString());
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

