﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 本地租户信息表
	/// </summary>
	public partial class LocalTenants
	{
		private readonly XinYiOffice.DAL.LocalTenants dal=new XinYiOffice.DAL.LocalTenants();
		public LocalTenants()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.LocalTenants model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.LocalTenants model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.LocalTenants GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.LocalTenants GetModelByCache(int Id)
		{
			
			string CacheKey = "LocalTenantsModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.LocalTenants)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.LocalTenants> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.LocalTenants> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.LocalTenants> modelList = new List<XinYiOffice.Model.LocalTenants>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.LocalTenants model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.LocalTenants();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["TenantGuid"]!=null && dt.Rows[n]["TenantGuid"].ToString()!="")
					{
					model.TenantGuid=dt.Rows[n]["TenantGuid"].ToString();
					}
					if(dt.Rows[n]["Name"]!=null && dt.Rows[n]["Name"].ToString()!="")
					{
					model.Name=dt.Rows[n]["Name"].ToString();
					}
					if(dt.Rows[n]["NameEn"]!=null && dt.Rows[n]["NameEn"].ToString()!="")
					{
					model.NameEn=dt.Rows[n]["NameEn"].ToString();
					}
					if(dt.Rows[n]["NameCn"]!=null && dt.Rows[n]["NameCn"].ToString()!="")
					{
					model.NameCn=dt.Rows[n]["NameCn"].ToString();
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["State"]!=null && dt.Rows[n]["State"].ToString()!="")
					{
						model.State=int.Parse(dt.Rows[n]["State"].ToString());
					}
					if(dt.Rows[n]["CreateUserId"]!=null && dt.Rows[n]["CreateUserId"].ToString()!="")
					{
						model.CreateUserId=int.Parse(dt.Rows[n]["CreateUserId"].ToString());
					}
					if(dt.Rows[n]["UpUserId"]!=null && dt.Rows[n]["UpUserId"].ToString()!="")
					{
						model.UpUserId=int.Parse(dt.Rows[n]["UpUserId"].ToString());
					}
					if(dt.Rows[n]["UpTime"]!=null && dt.Rows[n]["UpTime"].ToString()!="")
					{
						model.UpTime=DateTime.Parse(dt.Rows[n]["UpTime"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

