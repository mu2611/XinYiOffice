﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 银行帐号
	/// </summary>
	public partial class BankAccount
	{
		private readonly XinYiOffice.DAL.BankAccount dal=new XinYiOffice.DAL.BankAccount();
		public BankAccount()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.BankAccount model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.BankAccount model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.BankAccount GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.BankAccount GetModelByCache(int Id)
		{
			
			string CacheKey = "BankAccountModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.BankAccount)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.BankAccount> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.BankAccount> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.BankAccount> modelList = new List<XinYiOffice.Model.BankAccount>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.BankAccount model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.BankAccount();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["BankInfo"]!=null && dt.Rows[n]["BankInfo"].ToString()!="")
					{
					model.BankInfo=dt.Rows[n]["BankInfo"].ToString();
					}
					if(dt.Rows[n]["BankAccountName"]!=null && dt.Rows[n]["BankAccountName"].ToString()!="")
					{
					model.BankAccountName=dt.Rows[n]["BankAccountName"].ToString();
					}
					if(dt.Rows[n]["BankAccountNumber"]!=null && dt.Rows[n]["BankAccountNumber"].ToString()!="")
					{
					model.BankAccountNumber=dt.Rows[n]["BankAccountNumber"].ToString();
					}
					if(dt.Rows[n]["BankAccountTel"]!=null && dt.Rows[n]["BankAccountTel"].ToString()!="")
					{
					model.BankAccountTel=dt.Rows[n]["BankAccountTel"].ToString();
					}
					if(dt.Rows[n]["CashIn"]!=null && dt.Rows[n]["CashIn"].ToString()!="")
					{
						model.CashIn=decimal.Parse(dt.Rows[n]["CashIn"].ToString());
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["OpeningTime"]!=null && dt.Rows[n]["OpeningTime"].ToString()!="")
					{
						model.OpeningTime=DateTime.Parse(dt.Rows[n]["OpeningTime"].ToString());
					}
					if(dt.Rows[n]["InstitutionId"]!=null && dt.Rows[n]["InstitutionId"].ToString()!="")
					{
						model.InstitutionId=int.Parse(dt.Rows[n]["InstitutionId"].ToString());
					}
					if(dt.Rows[n]["DepartmentId"]!=null && dt.Rows[n]["DepartmentId"].ToString()!="")
					{
						model.DepartmentId=int.Parse(dt.Rows[n]["DepartmentId"].ToString());
					}
					if(dt.Rows[n]["StorageAmount"]!=null && dt.Rows[n]["StorageAmount"].ToString()!="")
					{
						model.StorageAmount=decimal.Parse(dt.Rows[n]["StorageAmount"].ToString());
					}
					if(dt.Rows[n]["BearerAccountId"]!=null && dt.Rows[n]["BearerAccountId"].ToString()!="")
					{
						model.BearerAccountId=int.Parse(dt.Rows[n]["BearerAccountId"].ToString());
					}
					if(dt.Rows[n]["AccountType"]!=null && dt.Rows[n]["AccountType"].ToString()!="")
					{
						model.AccountType=int.Parse(dt.Rows[n]["AccountType"].ToString());
					}
					if(dt.Rows[n]["AccountPurpose"]!=null && dt.Rows[n]["AccountPurpose"].ToString()!="")
					{
						model.AccountPurpose=int.Parse(dt.Rows[n]["AccountPurpose"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

