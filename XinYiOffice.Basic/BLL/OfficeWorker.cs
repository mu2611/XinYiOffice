﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 职员表
	/// </summary>
	public partial class OfficeWorker
	{
		private readonly XinYiOffice.DAL.OfficeWorker dal=new XinYiOffice.DAL.OfficeWorker();
		public OfficeWorker()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.OfficeWorker model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.OfficeWorker model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.OfficeWorker GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.OfficeWorker GetModelByCache(int Id)
		{
			
			string CacheKey = "OfficeWorkerModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.OfficeWorker)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.OfficeWorker> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.OfficeWorker> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.OfficeWorker> modelList = new List<XinYiOffice.Model.OfficeWorker>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.OfficeWorker model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.OfficeWorker();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["FullName"]!=null && dt.Rows[n]["FullName"].ToString()!="")
					{
					model.FullName=dt.Rows[n]["FullName"].ToString();
					}
					if(dt.Rows[n]["UsedName"]!=null && dt.Rows[n]["UsedName"].ToString()!="")
					{
					model.UsedName=dt.Rows[n]["UsedName"].ToString();
					}
					if(dt.Rows[n]["Sex"]!=null && dt.Rows[n]["Sex"].ToString()!="")
					{
						model.Sex=int.Parse(dt.Rows[n]["Sex"].ToString());
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["Tel"]!=null && dt.Rows[n]["Tel"].ToString()!="")
					{
					model.Tel=dt.Rows[n]["Tel"].ToString();
					}
					if(dt.Rows[n]["Phone"]!=null && dt.Rows[n]["Phone"].ToString()!="")
					{
					model.Phone=dt.Rows[n]["Phone"].ToString();
					}
					if(dt.Rows[n]["IntNumber"]!=null && dt.Rows[n]["IntNumber"].ToString()!="")
					{
					model.IntNumber=dt.Rows[n]["IntNumber"].ToString();
					}
					if(dt.Rows[n]["InstitutionId"]!=null && dt.Rows[n]["InstitutionId"].ToString()!="")
					{
						model.InstitutionId=int.Parse(dt.Rows[n]["InstitutionId"].ToString());
					}
					if(dt.Rows[n]["DepartmentId"]!=null && dt.Rows[n]["DepartmentId"].ToString()!="")
					{
						model.DepartmentId=int.Parse(dt.Rows[n]["DepartmentId"].ToString());
					}
					if(dt.Rows[n]["Position"]!=null && dt.Rows[n]["Position"].ToString()!="")
					{
						model.Position=int.Parse(dt.Rows[n]["Position"].ToString());
					}
					if(dt.Rows[n]["Province"]!=null && dt.Rows[n]["Province"].ToString()!="")
					{
					model.Province=dt.Rows[n]["Province"].ToString();
					}
					if(dt.Rows[n]["City"]!=null && dt.Rows[n]["City"].ToString()!="")
					{
					model.City=dt.Rows[n]["City"].ToString();
					}
					if(dt.Rows[n]["County"]!=null && dt.Rows[n]["County"].ToString()!="")
					{
					model.County=dt.Rows[n]["County"].ToString();
					}
					if(dt.Rows[n]["Street"]!=null && dt.Rows[n]["Street"].ToString()!="")
					{
					model.Street=dt.Rows[n]["Street"].ToString();
					}
					if(dt.Rows[n]["ZipCode"]!=null && dt.Rows[n]["ZipCode"].ToString()!="")
					{
					model.ZipCode=dt.Rows[n]["ZipCode"].ToString();
					}
					if(dt.Rows[n]["BirthDate"]!=null && dt.Rows[n]["BirthDate"].ToString()!="")
					{
					model.BirthDate=dt.Rows[n]["BirthDate"].ToString();
					}
					if(dt.Rows[n]["ChinaID"]!=null && dt.Rows[n]["ChinaID"].ToString()!="")
					{
					model.ChinaID=dt.Rows[n]["ChinaID"].ToString();
					}
					if(dt.Rows[n]["Nationality"]!=null && dt.Rows[n]["Nationality"].ToString()!="")
					{
					model.Nationality=dt.Rows[n]["Nationality"].ToString();
					}
					if(dt.Rows[n]["NativePlace"]!=null && dt.Rows[n]["NativePlace"].ToString()!="")
					{
					model.NativePlace=dt.Rows[n]["NativePlace"].ToString();
					}
					if(dt.Rows[n]["Phone1"]!=null && dt.Rows[n]["Phone1"].ToString()!="")
					{
					model.Phone1=dt.Rows[n]["Phone1"].ToString();
					}
					if(dt.Rows[n]["Phone2"]!=null && dt.Rows[n]["Phone2"].ToString()!="")
					{
					model.Phone2=dt.Rows[n]["Phone2"].ToString();
					}
					if(dt.Rows[n]["PoliticalStatus"]!=null && dt.Rows[n]["PoliticalStatus"].ToString()!="")
					{
					model.PoliticalStatus=dt.Rows[n]["PoliticalStatus"].ToString();
					}
					if(dt.Rows[n]["EntryTime"]!=null && dt.Rows[n]["EntryTime"].ToString()!="")
					{
					model.EntryTime=dt.Rows[n]["EntryTime"].ToString();
					}
					if(dt.Rows[n]["EntranceMode"]!=null && dt.Rows[n]["EntranceMode"].ToString()!="")
					{
					model.EntranceMode=dt.Rows[n]["EntranceMode"].ToString();
					}
					if(dt.Rows[n]["PostGrades"]!=null && dt.Rows[n]["PostGrades"].ToString()!="")
					{
						model.PostGrades=int.Parse(dt.Rows[n]["PostGrades"].ToString());
					}
					if(dt.Rows[n]["WageLevel"]!=null && dt.Rows[n]["WageLevel"].ToString()!="")
					{
						model.WageLevel=int.Parse(dt.Rows[n]["WageLevel"].ToString());
					}
					if(dt.Rows[n]["InsuranceWelfare"]!=null && dt.Rows[n]["InsuranceWelfare"].ToString()!="")
					{
					model.InsuranceWelfare=dt.Rows[n]["InsuranceWelfare"].ToString();
					}
					if(dt.Rows[n]["GraduateSchool"]!=null && dt.Rows[n]["GraduateSchool"].ToString()!="")
					{
					model.GraduateSchool=dt.Rows[n]["GraduateSchool"].ToString();
					}
					if(dt.Rows[n]["FormalSchooling"]!=null && dt.Rows[n]["FormalSchooling"].ToString()!="")
					{
					model.FormalSchooling=dt.Rows[n]["FormalSchooling"].ToString();
					}
					if(dt.Rows[n]["Major"]!=null && dt.Rows[n]["Major"].ToString()!="")
					{
					model.Major=dt.Rows[n]["Major"].ToString();
					}
					if(dt.Rows[n]["EnglishLevel"]!=null && dt.Rows[n]["EnglishLevel"].ToString()!="")
					{
					model.EnglishLevel=dt.Rows[n]["EnglishLevel"].ToString();
					}
					if(dt.Rows[n]["PreWork"]!=null && dt.Rows[n]["PreWork"].ToString()!="")
					{
					model.PreWork=dt.Rows[n]["PreWork"].ToString();
					}
					if(dt.Rows[n]["PrePosition"]!=null && dt.Rows[n]["PrePosition"].ToString()!="")
					{
					model.PrePosition=dt.Rows[n]["PrePosition"].ToString();
					}
					if(dt.Rows[n]["PreStartTime"]!=null && dt.Rows[n]["PreStartTime"].ToString()!="")
					{
					model.PreStartTime=dt.Rows[n]["PreStartTime"].ToString();
					}
					if(dt.Rows[n]["PreEndTime"]!=null && dt.Rows[n]["PreEndTime"].ToString()!="")
					{
					model.PreEndTime=dt.Rows[n]["PreEndTime"].ToString();
					}
					if(dt.Rows[n]["PreEpartment"]!=null && dt.Rows[n]["PreEpartment"].ToString()!="")
					{
					model.PreEpartment=dt.Rows[n]["PreEpartment"].ToString();
					}
					if(dt.Rows[n]["TurnoverTime"]!=null && dt.Rows[n]["TurnoverTime"].ToString()!="")
					{
					model.TurnoverTime=dt.Rows[n]["TurnoverTime"].ToString();
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

