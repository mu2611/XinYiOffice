﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 客户_营销计划
	/// </summary>
	public partial class MarketingPlan
	{
		private readonly XinYiOffice.DAL.MarketingPlan dal=new XinYiOffice.DAL.MarketingPlan();
		public MarketingPlan()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.MarketingPlan model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.MarketingPlan model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.MarketingPlan GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.MarketingPlan GetModelByCache(int Id)
		{
			
			string CacheKey = "MarketingPlanModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.MarketingPlan)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.MarketingPlan> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.MarketingPlan> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.MarketingPlan> modelList = new List<XinYiOffice.Model.MarketingPlan>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.MarketingPlan model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.MarketingPlan();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["ProgramName"]!=null && dt.Rows[n]["ProgramName"].ToString()!="")
					{
					model.ProgramName=dt.Rows[n]["ProgramName"].ToString();
					}
					if(dt.Rows[n]["ProgramDescription"]!=null && dt.Rows[n]["ProgramDescription"].ToString()!="")
					{
					model.ProgramDescription=dt.Rows[n]["ProgramDescription"].ToString();
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["StartTime"]!=null && dt.Rows[n]["StartTime"].ToString()!="")
					{
						model.StartTime=DateTime.Parse(dt.Rows[n]["StartTime"].ToString());
					}
					if(dt.Rows[n]["EndTime"]!=null && dt.Rows[n]["EndTime"].ToString()!="")
					{
						model.EndTime=DateTime.Parse(dt.Rows[n]["EndTime"].ToString());
					}
					if(dt.Rows[n]["TYPE"]!=null && dt.Rows[n]["TYPE"].ToString()!="")
					{
						model.TYPE=int.Parse(dt.Rows[n]["TYPE"].ToString());
					}
					if(dt.Rows[n]["AnticipatedRevenue"]!=null && dt.Rows[n]["AnticipatedRevenue"].ToString()!="")
					{
						model.AnticipatedRevenue=decimal.Parse(dt.Rows[n]["AnticipatedRevenue"].ToString());
					}
					if(dt.Rows[n]["BudgetCost"]!=null && dt.Rows[n]["BudgetCost"].ToString()!="")
					{
						model.BudgetCost=decimal.Parse(dt.Rows[n]["BudgetCost"].ToString());
					}
					if(dt.Rows[n]["SchemerAccountId"]!=null && dt.Rows[n]["SchemerAccountId"].ToString()!="")
					{
						model.SchemerAccountId=int.Parse(dt.Rows[n]["SchemerAccountId"].ToString());
					}
					if(dt.Rows[n]["ExecuteAccountId"]!=null && dt.Rows[n]["ExecuteAccountId"].ToString()!="")
					{
						model.ExecuteAccountId=int.Parse(dt.Rows[n]["ExecuteAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

