﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 付款单
	/// </summary>
	public partial class AdvicePayment
	{
		private readonly XinYiOffice.DAL.AdvicePayment dal=new XinYiOffice.DAL.AdvicePayment();
		public AdvicePayment()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.AdvicePayment model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.AdvicePayment model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.AdvicePayment GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.AdvicePayment GetModelByCache(int Id)
		{
			
			string CacheKey = "AdvicePaymentModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.AdvicePayment)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.AdvicePayment> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.AdvicePayment> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.AdvicePayment> modelList = new List<XinYiOffice.Model.AdvicePayment>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.AdvicePayment model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.AdvicePayment();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["DocumentNumber"]!=null && dt.Rows[n]["DocumentNumber"].ToString()!="")
					{
					model.DocumentNumber=dt.Rows[n]["DocumentNumber"].ToString();
					}
					if(dt.Rows[n]["DocumentDescription"]!=null && dt.Rows[n]["DocumentDescription"].ToString()!="")
					{
					model.DocumentDescription=dt.Rows[n]["DocumentDescription"].ToString();
					}
					if(dt.Rows[n]["MethodPayment"]!=null && dt.Rows[n]["MethodPayment"].ToString()!="")
					{
						model.MethodPayment=int.Parse(dt.Rows[n]["MethodPayment"].ToString());
					}
					if(dt.Rows[n]["SupplierName"]!=null && dt.Rows[n]["SupplierName"].ToString()!="")
					{
					model.SupplierName=dt.Rows[n]["SupplierName"].ToString();
					}
					if(dt.Rows[n]["ClientName"]!=null && dt.Rows[n]["ClientName"].ToString()!="")
					{
					model.ClientName=dt.Rows[n]["ClientName"].ToString();
					}
					if(dt.Rows[n]["PayeeFullName"]!=null && dt.Rows[n]["PayeeFullName"].ToString()!="")
					{
					model.PayeeFullName=dt.Rows[n]["PayeeFullName"].ToString();
					}
					if(dt.Rows[n]["PayeeAccountId"]!=null && dt.Rows[n]["PayeeAccountId"].ToString()!="")
					{
						model.PayeeAccountId=int.Parse(dt.Rows[n]["PayeeAccountId"].ToString());
					}
					if(dt.Rows[n]["PaymentDate"]!=null && dt.Rows[n]["PaymentDate"].ToString()!="")
					{
						model.PaymentDate=DateTime.Parse(dt.Rows[n]["PaymentDate"].ToString());
					}
					if(dt.Rows[n]["TargetDate"]!=null && dt.Rows[n]["TargetDate"].ToString()!="")
					{
						model.TargetDate=DateTime.Parse(dt.Rows[n]["TargetDate"].ToString());
					}
					if(dt.Rows[n]["Remark"]!=null && dt.Rows[n]["Remark"].ToString()!="")
					{
					model.Remark=dt.Rows[n]["Remark"].ToString();
					}
					if(dt.Rows[n]["PaymentAmount"]!=null && dt.Rows[n]["PaymentAmount"].ToString()!="")
					{
						model.PaymentAmount=decimal.Parse(dt.Rows[n]["PaymentAmount"].ToString());
					}
					if(dt.Rows[n]["PaymentBankAccountId"]!=null && dt.Rows[n]["PaymentBankAccountId"].ToString()!="")
					{
						model.PaymentBankAccountId=int.Parse(dt.Rows[n]["PaymentBankAccountId"].ToString());
					}
					if(dt.Rows[n]["Sate"]!=null && dt.Rows[n]["Sate"].ToString()!="")
					{
						model.Sate=int.Parse(dt.Rows[n]["Sate"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

