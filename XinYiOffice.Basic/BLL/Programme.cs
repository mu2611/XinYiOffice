﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 日程表
	/// </summary>
	public partial class Programme
	{
		private readonly XinYiOffice.DAL.Programme dal=new XinYiOffice.DAL.Programme();
		public Programme()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.Programme model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.Programme model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.Programme GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.Programme GetModelByCache(int Id)
		{
			
			string CacheKey = "ProgrammeModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.Programme)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.Programme> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.Programme> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.Programme> modelList = new List<XinYiOffice.Model.Programme>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.Programme model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.Programme();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["StartTime"]!=null && dt.Rows[n]["StartTime"].ToString()!="")
					{
						model.StartTime=DateTime.Parse(dt.Rows[n]["StartTime"].ToString());
					}
					if(dt.Rows[n]["EndTime"]!=null && dt.Rows[n]["EndTime"].ToString()!="")
					{
						model.EndTime=DateTime.Parse(dt.Rows[n]["EndTime"].ToString());
					}
					if(dt.Rows[n]["ScheduleType"]!=null && dt.Rows[n]["ScheduleType"].ToString()!="")
					{
						model.ScheduleType=int.Parse(dt.Rows[n]["ScheduleType"].ToString());
					}
					if(dt.Rows[n]["ProjectInfoId"]!=null && dt.Rows[n]["ProjectInfoId"].ToString()!="")
					{
						model.ProjectInfoId=int.Parse(dt.Rows[n]["ProjectInfoId"].ToString());
					}
					if(dt.Rows[n]["InstitutionId"]!=null && dt.Rows[n]["InstitutionId"].ToString()!="")
					{
						model.InstitutionId=int.Parse(dt.Rows[n]["InstitutionId"].ToString());
					}
					if(dt.Rows[n]["Title"]!=null && dt.Rows[n]["Title"].ToString()!="")
					{
					model.Title=dt.Rows[n]["Title"].ToString();
					}
					if(dt.Rows[n]["Locale"]!=null && dt.Rows[n]["Locale"].ToString()!="")
					{
					model.Locale=dt.Rows[n]["Locale"].ToString();
					}
					if(dt.Rows[n]["Note"]!=null && dt.Rows[n]["Note"].ToString()!="")
					{
					model.Note=dt.Rows[n]["Note"].ToString();
					}
					if(dt.Rows[n]["IsRepeat"]!=null && dt.Rows[n]["IsRepeat"].ToString()!="")
					{
						model.IsRepeat=int.Parse(dt.Rows[n]["IsRepeat"].ToString());
					}
					if(dt.Rows[n]["RepeatInterval"]!=null && dt.Rows[n]["RepeatInterval"].ToString()!="")
					{
						model.RepeatInterval=int.Parse(dt.Rows[n]["RepeatInterval"].ToString());
					}
					if(dt.Rows[n]["UpToDate"]!=null && dt.Rows[n]["UpToDate"].ToString()!="")
					{
						model.UpToDate=DateTime.Parse(dt.Rows[n]["UpToDate"].ToString());
					}
					if(dt.Rows[n]["IsRemind"]!=null && dt.Rows[n]["IsRemind"].ToString()!="")
					{
						model.IsRemind=int.Parse(dt.Rows[n]["IsRemind"].ToString());
					}
					if(dt.Rows[n]["RemindDay"]!=null && dt.Rows[n]["RemindDay"].ToString()!="")
					{
						model.RemindDay=int.Parse(dt.Rows[n]["RemindDay"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

