﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 帐号资金流转表
	/// </summary>
	public partial class BankAccountDetails
	{
		private readonly XinYiOffice.DAL.BankAccountDetails dal=new XinYiOffice.DAL.BankAccountDetails();
		public BankAccountDetails()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.BankAccountDetails model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.BankAccountDetails model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.BankAccountDetails GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.BankAccountDetails GetModelByCache(int Id)
		{
			
			string CacheKey = "BankAccountDetailsModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.BankAccountDetails)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.BankAccountDetails> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.BankAccountDetails> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.BankAccountDetails> modelList = new List<XinYiOffice.Model.BankAccountDetails>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.BankAccountDetails model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.BankAccountDetails();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["BankAccountId"]!=null && dt.Rows[n]["BankAccountId"].ToString()!="")
					{
						model.BankAccountId=int.Parse(dt.Rows[n]["BankAccountId"].ToString());
					}
					if(dt.Rows[n]["IncreaseOrReduce"]!=null && dt.Rows[n]["IncreaseOrReduce"].ToString()!="")
					{
						model.IncreaseOrReduce=int.Parse(dt.Rows[n]["IncreaseOrReduce"].ToString());
					}
					if(dt.Rows[n]["SpecificAmount"]!=null && dt.Rows[n]["SpecificAmount"].ToString()!="")
					{
					model.SpecificAmount=dt.Rows[n]["SpecificAmount"].ToString();
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["OccurrenceTime"]!=null && dt.Rows[n]["OccurrenceTime"].ToString()!="")
					{
						model.OccurrenceTime=DateTime.Parse(dt.Rows[n]["OccurrenceTime"].ToString());
					}
					if(dt.Rows[n]["MakeCollectionsId"]!=null && dt.Rows[n]["MakeCollectionsId"].ToString()!="")
					{
						model.MakeCollectionsId=int.Parse(dt.Rows[n]["MakeCollectionsId"].ToString());
					}
					if(dt.Rows[n]["AdvicePaymentId"]!=null && dt.Rows[n]["AdvicePaymentId"].ToString()!="")
					{
						model.AdvicePaymentId=int.Parse(dt.Rows[n]["AdvicePaymentId"].ToString());
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

