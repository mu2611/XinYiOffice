﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.BLL
{
    public partial class BankAccount
    {

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListView(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vBankAccount ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by Id desc ");
            return DbHelperSQL.Query(strSql.ToString());
        }

    }
}
