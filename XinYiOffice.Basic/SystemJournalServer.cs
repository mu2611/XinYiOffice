﻿using System;
using System.Collections.Generic;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Basic
{
    public class SystemJournalServer
    {
        public static void SetSystemJournal(string msg,int accountid,int tenantid,string ip,string geterinfo,int type)
        {
            Model.SystemJournal sj=new Model.SystemJournal();

            try
            {

                sj.CreateAccountId = accountid;
                sj.CreateTime = DateTime.Now;
                sj.CurrentAccountId = accountid;
                sj.IPAddress = ip;
                sj.OperationContent = msg;
                sj.SystemInfo = xytools.CutString( geterinfo,3000);
                sj.TenantId = tenantid;
                sj.TYPE = type;

                new BLL.SystemJournal().Add(sj);
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }

        }

        /// <summary>
        /// 写错误日志
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="account"></param>
        /// <param name="tenantid"></param>
        public static void SetErrorLog(Exception ex,int account,int tenantid )
        {
            SetSystemJournal(ex.Message,account,tenantid,xytools.GetIp,BaseData.ClientInformation.GetAllClientInfoStrnig(),3);
        }

        /// <summary>
        /// 写系统日志
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="account"></param>
        /// <param name="tenantid"></param>
        public static void SetSysLog(string msg,int account, int tenantid)
        {
            SetSystemJournal(msg, account, tenantid, xytools.GetIp, BaseData.ClientInformation.GetAllClientInfoStrnig(),1);
        }
    }
}
