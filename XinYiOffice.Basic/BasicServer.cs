﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class BasicServer
    {
        public BasicServer()
        { 
        }

        /// <summary>
        /// 获取当前系统的所有表名
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllTableName()
        {
            return DbHelperSQL.Query(string.Format(" select [name] from sysobjects where xtype='u' or xtype='v' order by [name]")).Tables[0];
        }

        public DataTable GetAllColumsByTableName(string tablename)
        {
            return DbHelperSQL.Query(string.Format("SELECT Name FROM SysColumns WHERE id=Object_Id('{0}')", tablename)).Tables[0];
        }
    }
}
