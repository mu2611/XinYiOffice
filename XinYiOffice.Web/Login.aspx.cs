﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Basic;

namespace XinYiOffice.Web
{
    public partial class Login : System.Web.UI.Page
    {
        public string msg = string.Empty;
        public string TenantId = "0";

        string dname = string.Empty;//租户域名

        Model.LocalTenants lt = new Model.LocalTenants();

        protected void Page_Load(object sender, EventArgs e)
        {
            dname = xytools.url_get("dname");
            TenantId = xytools.url_get("tid");

            string xinyioffice_site="<a href=\"http://www.xinyioffice.com/cloud/login.aspx?f=app\" title=\"点击从云端入口登录\">云办公平台登录</a>";

            #region dname
            if (!string.IsNullOrEmpty(dname))
            {
                try
                {
                    lt = LocalTenantsServer.GetLocalTenantsByDname(dname);
                    if (!string.IsNullOrEmpty(lt.NameCn))
                    {
                        msg = string.Format("欢迎进入 [{0}] 在线办公!", lt.NameCn);
                        TenantId = Common.DESEncrypt.Encrypt(SafeConvert.ToString(lt.Id));
                    }
                    else
                    {
                        msg = "<span style='color:#FF0302;'>租户信息为空,请确认登录服务器 |"+xinyioffice_site+"</span>";
                    }
                }
                catch (Exception ex)
                {
                    EasyLog.WriteLog(ex);
                    msg = "域信息错误,请从 "+xinyioffice_site;
                }
                finally
                {
                }

            }
            else
            {
                msg = "<span style='color:#FF0302;'>域地址为空,请从"+xinyioffice_site+"</span>";
            } 
            #endregion

            #region tid
            if (!string.IsNullOrEmpty(TenantId))
            {
                try
                {
                    int tid = SafeConvert.ToInt(DESEncrypt.Decrypt(TenantId));
                    lt = LocalTenantsServer.GetLocalTenantsByTenantId(tid);
                    
                    if (!string.IsNullOrEmpty(lt.NameCn))
                    {
                        msg = string.Format("欢迎进入 [{0}] 在线办公!", lt.NameCn);
                        //TenantId = Common.DESEncrypt.Encrypt(SafeConvert.ToString(lt.Id));
                    }
                    else
                    {
                        msg = "<span style='color:#FF0302;'>租户信息为空,请确认登录服务器 |"+xinyioffice_site+"</span>";
                    }
                }
                catch (Exception ex)
                {
                    EasyLog.WriteLog(ex);
                    msg = "域信息错误,请从 "+xinyioffice_site;
                }
                finally
                {
                }

            }
            else
            {
                msg = "<span style='color:#FF0302;'>域地址为空,请从 " + xinyioffice_site + "</span>";
            } 
            #endregion
        }
    }
}