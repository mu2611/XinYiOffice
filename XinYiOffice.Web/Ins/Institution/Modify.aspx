﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Ins.Institution.Modify" Title="修改页" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
 <div class="setup_box" style="margin-bottom: 0">
 <div class="h_title"><h4>基本信息</h4></div>
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		机构名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtOrganizationName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		机构简介
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIntroduction" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		机构logo
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtLogo" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		机构URL
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtURL" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_txtRefreshTime" runat="server" />
        <asp:HiddenField ID="HiddenField_txtCreateTime" runat="server" />
        <asp:HiddenField ID="HiddenField_txtRefreshAccountId" runat="server" />
        <asp:HiddenField ID="HiddenField_txtCreateAccountId" runat="server" />
	</td></tr>
	</table>
        <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div></div>
    </form>
    </asp:Content>