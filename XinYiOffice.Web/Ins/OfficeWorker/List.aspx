﻿<%@ Page Title="职员表" MasterPageFile="~/BasicContent.Master"  Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Ins.OfficeWorker.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2"  >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <%if (ValidatePermission("CLIENTINFO_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("CLIENTINFO_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:searchapp ID="SearchApp1" runat="server" TableName="vClientInfo"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

<asp:Repeater ID="repOfficeWorker" runat="server" >
                <HeaderTemplate>
                <tr>
                <th  class="center"><input type="checkbox" id="cb_all"></th>
                <th>编号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>手机号码</th>
                <th>固定电话</th>
                <th>电子邮件</th>
                <th>所属机构</th>
                <th>所属部门</th>
                <th>所属职位</th>
                 <th>状态</th>
                  <th>入职时间</th>
                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "Id")%></td>
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "FullName")%></a></td>

                <td><%#DataBinder.Eval(Container.DataItem, "Sex")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Phone")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Tel")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Email")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "InstitutionName")%></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "DepartmentName")%></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "PositionName")%></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "SateName")%></td>
                  <td><%#DataBinder.Eval(Container.DataItem, "EntryTime")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>
</form>
<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>
