﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.OfficeWorker
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("OFFICEWORKER_MANAGE") && ValidatePermission("OFFICEWORKER_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            InitData();

            XinYiOffice.BLL.OfficeWorker bll = new XinYiOffice.BLL.OfficeWorker();
            XinYiOffice.Model.OfficeWorker model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtFullName.Text = model.FullName;
            this.txtUsedName.Text = model.UsedName;
            DropDownList_Sex.SelectedValue = model.Sex.ToString();

            this.txtEmail.Text = model.Email;
            this.txtTel.Text = model.Tel;
            this.txtPhone.Text = model.Phone;
            this.txtIntNumber.Text = model.IntNumber;
            this.DropDownList_InstitutionId.SelectedValue = model.InstitutionId.ToString();

            //部门
            this.DropDownList_DepartmentId.DataSource = new BLL.Department().GetList(string.Format("InstitutionId={0} and TenantId={1}", model.InstitutionId,CurrentTenantId));
            this.DropDownList_DepartmentId.DataTextField = "DepartmentName";
            this.DropDownList_DepartmentId.DataValueField = "Id";
            DropDownList_DepartmentId.DataBind();
            DropDownList_DepartmentId.Items.Insert(0, new ListItem("请选择所属部门", "0"));

            this.DropDownList_DepartmentId.SelectedValue = model.DepartmentId.ToString();
            HiddenField_DepartmentId.Value = model.DepartmentId.ToString();

            this.DropDownList_Position.SelectedValue = model.Position.ToString();
            this.txtProvince.Text = model.Province;
            this.txtCity.Text = model.City;
            this.txtCounty.Text = model.County;
            this.txtStreet.Text = model.Street;
            this.txtZipCode.Text = model.ZipCode;
            this.txtBirthDate.Text = model.BirthDate;
            this.txtChinaID.Text = model.ChinaID;
            this.txtNationality.Text = model.Nationality;
            this.txtNativePlace.Text = model.NativePlace;
            this.txtPhone1.Text = model.Phone1;
            this.txtPhone2.Text = model.Phone2;
            this.txtPoliticalStatus.Text = model.PoliticalStatus;
            this.txtEntryTime.Text = model.EntryTime;
            this.txtEntranceMode.Text = model.EntranceMode;
            this.DropDownList_PostGrades.SelectedValue = model.PostGrades.ToString();
            this.DropDownList_WageLevel.SelectedValue = model.WageLevel.ToString();
            this.txtInsuranceWelfare.Text = model.InsuranceWelfare;
            this.txtGraduateSchool.Text = model.GraduateSchool;
            this.txtFormalSchooling.Text = model.FormalSchooling;
            this.txtMajor.Text = model.Major;
            this.txtEnglishLevel.Text = model.EnglishLevel;
            this.txtPreWork.Text = model.PreWork;
            this.txtPrePosition.Text = model.PrePosition;
            this.txtPreStartTime.Text = model.PreStartTime;
            this.txtPreEndTime.Text = model.PreEndTime;
            this.txtPreEpartment.Text = model.PreEpartment;
            this.txtTurnoverTime.Text = model.TurnoverTime;
            this.DropDownList_Sate.SelectedValue = model.Sate.ToString();
            this.txtRemarks.Text = model.Remarks;
            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtRefreshAccountId.Value = model.RefreshAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();
            this.txtRefreshTime.Value = model.RefreshTime.ToString();

        }

        public void InitData()
        {
            DropDownList_InstitutionId.DataSource = new BLL.Institution().GetAllList();
            DropDownList_InstitutionId.DataTextField = "OrganizationName";
            DropDownList_InstitutionId.DataValueField = "Id";
            DropDownList_InstitutionId.DataBind();
            DropDownList_InstitutionId.Items.Insert(0, new ListItem("请选择所属机构", "0"));

            //DropDownList_Position 职位
            SetDropDownList("Position", ref DropDownList_Position);
            DropDownList_Position.Items.Insert(0, new ListItem("请选择所属职位", "0"));

            //状态
            SetDropDownList("Sate", ref DropDownList_Sate);

            //岗位级别
            SetDropDownList("PostGrades", ref DropDownList_PostGrades);

            //工资级别
            SetDropDownList("WageLevel", ref DropDownList_WageLevel);

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.OfficeWorkerDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                int Id = int.Parse(this.lblId.Text);
                string FullName = this.txtFullName.Text;
                string UsedName = this.txtUsedName.Text;
                int Sex = SafeConvert.ToInt(DropDownList_Sex.SelectedValue);

                string Email = this.txtEmail.Text;
                string Tel = this.txtTel.Text;
                string Phone = this.txtPhone.Text;
                string IntNumber = this.txtIntNumber.Text;
                int InstitutionId = SafeConvert.ToInt(this.DropDownList_InstitutionId.SelectedValue);
                int DepartmentId = SafeConvert.ToInt(this.HiddenField_DepartmentId.Value);
                int Position = SafeConvert.ToInt(this.DropDownList_Position.SelectedValue);

                string Province = this.txtProvince.Text;
                string City = this.txtCity.Text;
                string County = this.txtCounty.Text;
                string Street = this.txtStreet.Text;
                string ZipCode = this.txtZipCode.Text;
                string BirthDate = this.txtBirthDate.Text;
                string ChinaID = this.txtChinaID.Text;
                string Nationality = this.txtNationality.Text;
                string NativePlace = this.txtNativePlace.Text;
                string Phone1 = this.txtPhone1.Text;
                string Phone2 = this.txtPhone2.Text;
                string PoliticalStatus = this.txtPoliticalStatus.Text;
                string EntryTime = this.txtEntryTime.Text;
                string EntranceMode = this.txtEntranceMode.Text;
                int PostGrades = SafeConvert.ToInt(this.DropDownList_PostGrades.SelectedValue);
                int WageLevel = SafeConvert.ToInt(this.DropDownList_WageLevel.SelectedValue);
                string InsuranceWelfare = this.txtInsuranceWelfare.Text;
                string GraduateSchool = this.txtGraduateSchool.Text;
                string FormalSchooling = this.txtFormalSchooling.Text;
                string Major = this.txtMajor.Text;
                string EnglishLevel = this.txtEnglishLevel.Text;
                string PreWork = this.txtPreWork.Text;
                string PrePosition = this.txtPrePosition.Text;
                string PreStartTime = this.txtPreStartTime.Text;
                string PreEndTime = this.txtPreEndTime.Text;
                string PreEpartment = this.txtPreEpartment.Text;
                string TurnoverTime = this.txtTurnoverTime.Text;
                int Sate = SafeConvert.ToInt(this.DropDownList_Sate.SelectedValue);
                string Remarks = this.txtRemarks.Text;
                int CreateAccountId = SafeConvert.ToInt(this.txtCreateAccountId.Value);
                int RefreshAccountId = SafeConvert.ToInt(this.txtRefreshAccountId.Value);
                DateTime CreateTime = SafeConvert.ToDateTime(this.txtCreateTime.Value);
                DateTime RefreshTime = SafeConvert.ToDateTime(this.txtRefreshTime.Value);

                XinYiOffice.Model.OfficeWorker model = new XinYiOffice.Model.OfficeWorker();
                model.Id = Id;
                model.FullName = FullName;
                model.UsedName = UsedName;
                model.Sex = Sex;
                model.Email = Email;
                model.Tel = Tel;
                model.Phone = Phone;
                model.IntNumber = IntNumber;
                model.InstitutionId = InstitutionId;
                model.DepartmentId = DepartmentId;
                model.Position = Position;
                model.Province = Province;
                model.City = City;
                model.County = County;
                model.Street = Street;
                model.ZipCode = ZipCode;
                model.BirthDate = BirthDate;
                model.ChinaID = ChinaID;
                model.Nationality = Nationality;
                model.NativePlace = NativePlace;
                model.Phone1 = Phone1;
                model.Phone2 = Phone2;
                model.PoliticalStatus = PoliticalStatus;
                model.EntryTime = EntryTime;
                model.EntranceMode = EntranceMode;
                model.PostGrades = PostGrades;
                model.WageLevel = WageLevel;
                model.InsuranceWelfare = InsuranceWelfare;
                model.GraduateSchool = GraduateSchool;
                model.FormalSchooling = FormalSchooling;
                model.Major = Major;
                model.EnglishLevel = EnglishLevel;
                model.PreWork = PreWork;
                model.PrePosition = PrePosition;
                model.PreStartTime = PreStartTime;
                model.PreEndTime = PreEndTime;
                model.PreEpartment = PreEpartment;
                model.TurnoverTime = TurnoverTime;
                model.Sate = Sate;
                model.Remarks = Remarks;
                model.CreateAccountId = CreateAccountId;
                model.RefreshAccountId = RefreshAccountId;
                model.CreateTime = CreateTime;
                model.RefreshTime = RefreshTime;
                model.TenantId = CurrentTenantId;
                XinYiOffice.BLL.OfficeWorker bll = new XinYiOffice.BLL.OfficeWorker();
                bll.Update(model);
                xytools.web_alert("保存成功！", "list.aspx");

            }
            catch (Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务器忙,请稍候再试!");
            }
            finally
            {
            }
        }
    }
}
