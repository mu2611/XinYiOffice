﻿// JavaScript Document
$(function(){
    var oTable = $('#ctl00_ContentPlaceHolder_Calendar1');
	oTable.css('background','#f5f5f5');
	oTable.attr('cellpadding',0).css({'width':'100%','border':'none'})
	var DateTd=oTable.find('tr:gt(1) td');
	var DateA=DateTd.find('a')
	DateA.addClass('tda').css('color','#adabab');
	DateTd.addClass('datetd')
	var oTr_title = oTable.find('tr:first').find('td');
	oTr_title.addClass('td_ba_title');
	var prv = oTable.find('tr:first').find('table').find('td').eq(0);
	var next = oTable.find('tr:first').find('table').find('td').eq(2);
	prv.find('a').html('上一月').addClass('pre_next pre').css('color','#fff');next.find('a').html('下一月').addClass('pre_next next').css('color','#fff');
	oTable.find('th').each(function() {
        var html=$(this).html();
		$(this).html('星期'+html).addClass('th');
    });
	oTable.find('th').eq(0).css('color','#d86b19')
	oTable.find('th').eq(6).css('color','#d86b19')
	DateTd.hover(function(){$(this).css('border','1px solid #d96a19')},function(){$(this).css('border','1px solid #d3d3d3')})
	
	$('#mb').css('height',$('body').height())
	$('.pren_ul li').live('click',function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');	
		}else{
			$(this).addClass('active')	
		}
	})
	var roR=$('.content_btn input').eq(0);
	var roL=$('.content_btn input').eq(1);
	var roR_all=$('.content_btn input').eq(2);
	var roL_all=$('.content_btn input').eq(3);
	roR.click(function(){
		$('#left_list_p li').each(function() {
            if($(this).hasClass('active')){
				var xz_prev=$(this).remove();
				xz_prev.removeClass('active')	
				$('#right_list_p').append(xz_prev)
			}
        });	
	});
	roL.click(function(){
		$('#right_list_p li').each(function() {
            if($(this).hasClass('active')){
				var xz_prev=$(this).remove();
				xz_prev.removeClass('active')	
				$('#left_list_p').append(xz_prev)
			}
        });	
	});
	roR_all.click(function(){
		$('#left_list_p li').each(function() {
			var xz_prev=$(this).remove();
			xz_prev.removeClass('active')	
			$('#right_list_p').append(xz_prev)	
        });	
	});
	roL_all.click(function(){
		$('#right_list_p li').each(function() {
			var xz_prev=$(this).remove();
			xz_prev.removeClass('active')	
			$('#left_list_p').append(xz_prev)	
        });	
	});
	Close_A=$('.title_h4 a');
	Close_A.click(function(){
		CloseAll()
	})
	DateA.click(function(){
		$('#mb').show();$('#tcc1').show();	
	});
	

	
	$('.qd').click(function(){
		CloseAll();	
	});
	
	$('.qx').click(function(){CloseAll();});
	
	$('.tj_t a').click(function(){
		$('#tcc1').show();	$('#tcc2').hide();	
	})
	
	function CloseAll()
	{
		$('#tcc1').hide();$('#tcc2').hide();$('#mb').hide();
	}
})