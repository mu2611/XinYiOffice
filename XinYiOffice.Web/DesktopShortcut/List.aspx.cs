﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.DesktopShortcut
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.DesktopShortcut bll = new XinYiOffice.BLL.DesktopShortcut();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindData();
            }            
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }
        
        
        #region gridView
                        
        public void BindData()
        {
            

            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder();
           
            DataTable dt= DesktopShortcutServer.GetMyDesktopShortcut(CurrentAccountId,CurrentTenantId);
            repDesktopShortcut.DataSource = dt;
            repDesktopShortcut.DataBind();
        }

        #endregion





    }
}
