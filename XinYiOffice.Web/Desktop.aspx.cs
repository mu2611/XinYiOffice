﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using System.Data;

namespace XinYiOffice.Web
{
    public partial class Desktop : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        protected void InitData()
        {
            DataTable dt=DesktopServer.GetMyDesktop(CurrentAccountId,CurrentTenantId);
            
            foreach(DataRow dr in dt.Rows)
            {
                try
                {
                    string address = dr["LoadAddress"].ToString();
                    panDesktop.Controls.Add(LoadControl(address));
                }
                catch
                {
                    continue;
                }
                
            }

            //panDesktop.Controls.Add();
        }
    }
}