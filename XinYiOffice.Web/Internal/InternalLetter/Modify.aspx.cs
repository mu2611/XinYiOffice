﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Internal.InternalLetter
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.InternalLetter bll = new XinYiOffice.BLL.InternalLetter();
            XinYiOffice.Model.InternalLetter model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtInternalLetterId.Text = model.InternalLetterId.ToString();
            this.txtTitle.Text = model.Title;
            this.txtCon.Text = model.Con;
            this.txtAuthorAccountId.Text = model.AuthorAccountId.ToString();
            this.txtImpDegree.Text = model.ImpDegree.ToString();
            this.txtIsDelete.Text = model.IsDelete.ToString();
            this.txtRemarks.Text = model.Remarks;
            this.txtCreateAccountId.Text = model.CreateAccountId.ToString();
            this.txtRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.txtCreateTime.Text = model.CreateTime.ToString();
            this.txtRefreshTime.Text = model.RefreshTime.ToString();

        }
 


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            int InternalLetterId = int.Parse(this.txtInternalLetterId.Text);
            string Title = this.txtTitle.Text;
            string Con = this.txtCon.Text;
            int AuthorAccountId = int.Parse(this.txtAuthorAccountId.Text);
            int ImpDegree = int.Parse(this.txtImpDegree.Text);
            int IsDelete = int.Parse(this.txtIsDelete.Text);
            string Remarks = this.txtRemarks.Text;
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);


            XinYiOffice.Model.InternalLetter model = new XinYiOffice.Model.InternalLetter();
            model.Id = Id;
            model.InternalLetterId = InternalLetterId;
            model.Title = Title;
            model.Con = Con;
            model.AuthorAccountId = AuthorAccountId;
            model.ImpDegree = ImpDegree;
            model.IsDelete = IsDelete;
            model.Remarks = Remarks;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.InternalLetter bll = new XinYiOffice.BLL.InternalLetter();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
