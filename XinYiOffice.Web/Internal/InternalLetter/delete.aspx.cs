﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Internal.InternalLetter
{
    public partial class delete : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XinYiOffice.BLL.InternalReceiver bll_InternalReceiver = new XinYiOffice.BLL.InternalReceiver();
                XinYiOffice.BLL.InternalLetter bll_InternalLetter = new BLL.InternalLetter();
                bool isInternalLetter = false;
                string action = xytools.url_get("action");
                if (!string.IsNullOrEmpty(action) && action == "InternalLetter")
                {
                    isInternalLetter = true;
                }

                string idList = xytools.url_get("id");
                if (!string.IsNullOrEmpty(idList))
                {
                    idList = idList.TrimEnd(',');

                    bool isList = xyStringClass.StringIsExistIN(idList, ",");
                    if (isList)
                    {
                        if (isInternalLetter)
                        {
                            //bll_InternalLetter.DeleteList(idList);
                            foreach (string _id in idList.Split(','))
                            {
                                Model.InternalLetter il = new BLL.InternalLetter().GetModel(SafeConvert.ToInt(_id));
                                il.IsDelete = 1;
                                new BLL.InternalLetter().Update(il);
                            }
                            
                        }
                        else
                        {
                            bll_InternalReceiver.DeleteList(idList);
                        }
                    }
                    else
                    {
                        if (isInternalLetter)
                        {
                            //bll_InternalLetter.Delete(SafeConvert.ToInt(idList, 0));

                            Model.InternalLetter il = new BLL.InternalLetter().GetModel(SafeConvert.ToInt(idList));
                            il.IsDelete = 1;
                            new BLL.InternalLetter().Update(il);
                        }
                        else
                        {
                            bll_InternalReceiver.Delete(SafeConvert.ToInt(idList, 0));
                        }
                    }

                    Response.Redirect("list.aspx");
                }

            }

        }
    }
}