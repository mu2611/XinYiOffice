﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Internal.InternalReceiver.Add" Title="增加页" %>

<form  id="f2" name="f2" runat="server">
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right">
		信件ID
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtInternalLetterId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		收件人ID
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRecipientAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		收件人类型 个人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRecipientType" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		是否已查看
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsView" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		是否删除
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsDelete" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		是否已回复
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsReply" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		信件位置 1-收件箱,2-发件
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtLetterPosition" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		位置标识 文件夹名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPositionFoler" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCreateAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRefreshAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		查看日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtCheckDate" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtCreateTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新时间 最近一次编辑的人员I
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtRefreshTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
</table>
    </form>