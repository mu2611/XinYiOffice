﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Discuss.Show" Title="显示页" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
<link href="/css/project.css" rel="stylesheet" type="text/css" />
<style>

.btn_box a {
    float: left;
    font-size: 14px;
    font-weight: bold;
    height: 29px;
    line-height: 29px;
    margin-right: 10px;
    text-align: center;
    width: 75px;
}
.save {
    background: url("../img/save.jpg") no-repeat scroll 0 0 transparent;
    color: #FFFFFF;
}

</style>
<script type="text/javascript">
    $(function () {
        $('.table_right div').each(function () {
            $(this).css('height', $(this).parent().height())
        });
    })
</script>

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <form id="form1" runat="server" enctype="multipart/form-data">

<div class="setup_box">
       	<ul id="ck_list">
        
            <asp:Repeater ID="repDiscuss" runat="server" 
                onitemdatabound="repDiscuss_ItemDataBound">
            <ItemTemplate>
        	<li>
            	<div class="li_title li_t clear"><div class="list_t_time"><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></div><div class="list_t_bt"><%#DataBinder.Eval(Container.DataItem, "Title")%></div><div class="list_t_yy"><a href="javascript:;">引用</a></div></div>
                <table cellpadding="0" cellspacing="0" class="plan_table">
                    <tr>
                        <td class="table_left">
                            <div>
                                <a href="javascript:;" class="tx_img"><img src="<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem, "HeadPortrait").ToString())%>" /></a>
                                <span><a href="javascript:;" class="user_name"><%#DataBinder.Eval(Container.DataItem, "FullName")%></a></span>
                                <p class="time">加入时间：<%#DataBinder.Eval(Container.DataItem, "AccountCreateTime", "{0:yyyy-MM-dd}")%></p>
                                <a href="javascript:;" class="fsxx">发送消息</a>
                            </div>
                        </td>
                        <td class="table_right">
                        	<div>
                                <p class="text"><%#DataBinder.Eval(Container.DataItem, "Con")%></p>
                                <ul class="clear">

                                    <asp:Repeater ID="repResAttachment" runat="server">
                                    <ItemTemplate>
                                    <li><%#DataBinder.Eval(Container.DataItem, "Title")%><a href="<%#DataBinder.Eval(Container.DataItem, "Annex")%>">下载</a></li>
                                    </ItemTemplate>
                                    </asp:Repeater>

     
                                </ul>
                                <span>楼主</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </li>
            </ItemTemplate>
            </asp:Repeater>
            
            
            
            <asp:Repeater ID="repDiscussRe" runat="server" 
                onitemdatabound="repDiscussRe_ItemDataBound">
            <ItemTemplate>
            
            <li>
            	<div class=" li_t clear"><div class="list_t_time"><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></div><div class="list_t_bt"><%#DataBinder.Eval(Container.DataItem, "Title")%></div><div class="list_t_yy"><a href="javascript:;">引用</a><a href="javascript:;">删除</a></div></div>
                <table cellpadding="0" cellspacing="0" class="plan_table">
                    <tr>
                        <td class="table_left">
                        <div>
                            <a href="javascript:;" class="tx_img"><img src="<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem, "HeadPortrait").ToString())%>" /></a>
                                <span><a href="javascript:;" class="user_name"><%#DataBinder.Eval(Container.DataItem, "FullName")%></a></span>
                                <p class="time">加入时间：<%#DataBinder.Eval(Container.DataItem, "AccountCreateTime", "{0:yyyy-MM-dd}")%></p>
                                <a href="javascript:;" class="fsxx">发送消息</a>
                                </div>
                        </td>
                        <td class="table_right">
                        	<div>
                                <p class="text"><%#DataBinder.Eval(Container.DataItem, "Con")%></p>
                                <ul class="clear">

                                 <asp:Repeater ID="repResAttachment" runat="server" >
                                    <ItemTemplate>
                                    <li><%#DataBinder.Eval(Container.DataItem, "Title")%><a href="<%#DataBinder.Eval(Container.DataItem, "Annex")%>">下载</a></li>
                                    </ItemTemplate>
                                    </asp:Repeater>

                                </ul>
                                <span><%#FloorIndex((Container.ItemIndex + 1))%> 楼</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </li>
            </ItemTemplate>
            </asp:Repeater>
            
        </ul>
    </div>

    
<%if (ValidatePermission("DISCUSS_TOOL_KJHF")){ %>
<div class="setup_box">
 <div class="h_title"><h4>快捷回复</h4></div>
<table cellpadding="0" cellspacing="0" class="plan_table">
            <tr>
            	<td class="table_left">回复标题：
            	</td>
              <td class="table_right">
                  <asp:HiddenField ID="HiddenField_DiscussId" runat="server" />
                  <asp:TextBox ID="txt_Title" runat="server" Width="520px"></asp:TextBox>
                </td>
            </tr>
            <tr>
              <td class="table_left">内容：</td>
              <td class="table_right">
                <asp:TextBox ID="TextBox_Con" runat="server" Height="118px" TextMode="MultiLine" 
                      Width="520px"></asp:TextBox></td>
            </tr>

            <%if (ValidatePermission("DISCUSS_TOOL_UPRES")){ %>
            <tr>
              <td class="table_left">附件资源：</td>
              <td class="table_right">
              <a href="javascript:addimg()" >增加</a>
                <div id="mdiv" style="display:none;"></div></td>
            </tr>
            <%} %>

            </table>

<div class="clear btn_box">
<asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click" style="margin-left:125px; margin-top:5px;">回复</asp:LinkButton>
</div>

</div>

<script type="text/javascript" >
    $(document).ready(function () {
        bindListener();
    });
    // 用来绑定事件(使用unbind避免重复绑定)
    function bindListener() {
        $("a[name=rmlink]").unbind().click(function () {
            $(this).parent().remove();
            $("#mdiv").hide();
        })
    }
    function addimg() {
        $("#mdiv").append('<div class="iptdiv"><input type="file" name="file_res" class="wj" /><a href="javascript:void(0);" name="rmlink">[X]</a></div>');
        $("#mdiv").show();

        // 为新元素节点添加事件侦听器

        bindListener();
    } 
</script>
<%} %>


    </form>
</asp:Content>