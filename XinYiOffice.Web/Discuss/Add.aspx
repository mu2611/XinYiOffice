﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Discuss.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
<style>
.plan_table select{ width:150px;}
</style>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加新讨论</h4></div>
<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属项目：</td>
	<td height="25" width="*" align="left">
	    <asp:Label ID="Label_Project" runat="server" Text="Label"></asp:Label>
	    <asp:HiddenField ID="HiddenField_ProjectId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		讨论类别
	：</td>
	<td height="25" width="*" align="left">
	    <asp:Label ID="Label_DiscussClass" runat="server" Text="Label"></asp:Label>
	    <asp:HiddenField ID="HiddenField_ClassId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		标题
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTitle" runat="server" Width="606px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内容
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCon" runat="server" Width="609px" Height="162px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		是否置顶
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_IsTop" runat="server" AutoPostBack="True">
            <asp:ListItem Value="1">否</asp:ListItem>
            <asp:ListItem Value="2">置顶</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		是否隐藏
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_IsShow" runat="server">
            <asp:ListItem Value="1">显示</asp:ListItem>
            <asp:ListItem Value="2">隐藏</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
            <asp:ListItem Value="1">开放讨论</asp:ListItem>
            <asp:ListItem Value="2">不开放</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSort" runat="server" Width="43px">1</asp:TextBox>
	</td></tr>
</table>
</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

    </form>
</asp:Content>

