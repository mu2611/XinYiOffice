﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web
{
    public partial class LeftMent : BasicPage
    {
        public int MenuTreeId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            MenuTreeId = SafeConvert.ToInt(xytools.url_get("tid"), 0);
            if (MenuTreeId == 0)
            {
                panLeftDefault.Visible = true;
                panLeft.Visible = false;
                return;
            }
            else
            {
                panLeftDefault.Visible = false;
                panLeft.Visible = true;

                DataRow[] drCurrentMenuTree = AppDataCacheServer.MenuTreeList().Select(string.Format("Id={0} and IsShow=1", MenuTreeId));
                if (drCurrentMenuTree != null && drCurrentMenuTree.Length > 0)
                {
                    litCurrentMenuTreeName.Text = SafeConvert.ToString(drCurrentMenuTree[0]["Name"]);
                }

                DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0} and IsShow=1", MenuTreeId));
                DataTable dt = AppDataCacheServer.MenuTreeList().Clone();
                foreach (DataRow _dr in dr)
                {
                    string sign = string.Format("MENUTREE_{0}", _dr["Guid"]);

                    if (ValidatePermission(sign))
                    {
                        dt.ImportRow(_dr);
                    }
                }

                repLeftMent.DataSource = dt;
                repLeftMent.DataBind();
            }


        }

        protected void repLeftMent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rep = (Repeater)e.Item.FindControl("repLeftMentClass") as Repeater;
            DataRowView dv = (DataRowView)e.Item.DataItem;

            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0} and IsShow=1", SafeConvert.ToInt(dv["Id"])));
            DataTable dt = AppDataCacheServer.MenuTreeList().Clone();
            foreach (DataRow _dr in dr)
            {
                string sign = string.Format("MENUTREE_{0}", _dr["Guid"]);

                if (ValidatePermission(sign))
                {
                    dt.ImportRow(_dr);
                }
            }


            rep.DataSource = dt;
            rep.DataBind();
        }



    }
}