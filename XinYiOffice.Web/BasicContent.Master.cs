﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common.Web;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web
{
    public partial class BasicContent : System.Web.UI.MasterPage
    {
        public string CurrentTenantNameCn=string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            //SMP smp = new SMP();
            //SiteMapPath1.Provider = smp;

            SiteMapPath1.SiteMapProvider = "SqlSiteMapProvider";

            BasicPage bp = (BasicPage)this.Page as UI.BasicPage;
            CurrentTenantNameCn=bp.CurrentTenantNameCn;
        }

        public  void SetMarketingPlanCssIsHide()
        {
            marketingplan_css.Visible = false;
        }
    }
}