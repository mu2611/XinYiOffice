﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Finance.ReimbursementExpenses.Show" Title="显示页" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 36px;
        }
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>费用报销详情</h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td width="30%" align="right" class="table_left">
		编号
	：</td>
	<td width="*" align="left" class="style1">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用主题
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCostTilte" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		内容描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCostCon" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRemarks" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblApplicationDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		费用类型
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCostType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		票据张数
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblBillNumber" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请人姓名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblApplicantName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		申请人用户帐号 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblUserAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		相关客户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRelatedClient" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		相关项目
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRelatedProject" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		审核意见
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAuditOpinion" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblState" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		处理人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDealingPeopleAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>
            </form>




            </asp:Content>