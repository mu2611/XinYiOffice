﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Finance.BankAccountDetails
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.BankAccountDetails bll = new XinYiOffice.BLL.BankAccountDetails();
            XinYiOffice.Model.BankAccountDetails model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtBankAccountId.Text = model.BankAccountId.ToString();
            this.txtIncreaseOrReduce.Text = model.IncreaseOrReduce.ToString();
            this.txtSpecificAmount.Text = model.SpecificAmount;
            this.txtRemarks.Text = model.Remarks;
            this.txtOccurrenceTime.Text = model.OccurrenceTime.ToString();
            this.txtMakeCollectionsId.Text = model.MakeCollectionsId.ToString();
            this.txtAdvicePaymentId.Text = model.AdvicePaymentId.ToString();
            this.txtCreateAccountId.Text = model.CreateAccountId.ToString();
            this.txtCreateTime.Text = model.CreateTime.ToString();

        }



        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            int BankAccountId = int.Parse(this.txtBankAccountId.Text);
            int IncreaseOrReduce = int.Parse(this.txtIncreaseOrReduce.Text);
            string SpecificAmount = this.txtSpecificAmount.Text;
            string Remarks = this.txtRemarks.Text;
            DateTime OccurrenceTime = DateTime.Parse(this.txtOccurrenceTime.Text);
            int MakeCollectionsId = int.Parse(this.txtMakeCollectionsId.Text);
            int AdvicePaymentId = int.Parse(this.txtAdvicePaymentId.Text);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);


            XinYiOffice.Model.BankAccountDetails model = new XinYiOffice.Model.BankAccountDetails();
            model.Id = Id;
            model.BankAccountId = BankAccountId;
            model.IncreaseOrReduce = IncreaseOrReduce;
            model.SpecificAmount = SpecificAmount;
            model.Remarks = Remarks;
            model.OccurrenceTime = OccurrenceTime;
            model.MakeCollectionsId = MakeCollectionsId;
            model.AdvicePaymentId = AdvicePaymentId;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.BankAccountDetails bll = new XinYiOffice.BLL.BankAccountDetails();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
