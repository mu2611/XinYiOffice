﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Finance.BankAccount
{
    public partial class delete : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("BANKACCOUNT_MANAGE") && base.ValidatePermission("BANKACCOUNT_MANAGE_LIST_DEL")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                XinYiOffice.BLL.BankAccount bll = new XinYiOffice.BLL.BankAccount();

                string idList = xytools.url_get("id");
                if (!string.IsNullOrEmpty(idList))
                {
                    idList = idList.TrimEnd(',');

                    bool isList = xyStringClass.StringIsExistIN(idList, ",");
                    if (isList)
                    {
                        bll.DeleteList(idList);
                    }
                    else
                    {
                        bll.Delete(SafeConvert.ToInt(idList, 0));
                    }

                    Response.Redirect("list.aspx");
                }

            }

        }
    }
}