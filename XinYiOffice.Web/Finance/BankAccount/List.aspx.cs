﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.BankAccount
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.BankAccount bll = new XinYiOffice.BLL.BankAccount();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("BANKACCOUNT_MANAGE") && base.ValidatePermission("BANKACCOUNT_MANAGE_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
        }
        
        #region gridView
                        
        public void BindData()
        {
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
     
            ds = bll.GetListView(strWhere.ToString(),CurrentTenantId);
            repBankAccount.DataSource = ds;
            repBankAccount.DataBind();

        }
        #endregion






    }
}
