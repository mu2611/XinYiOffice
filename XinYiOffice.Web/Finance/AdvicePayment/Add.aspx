﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Finance.AdvicePayment.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script src="/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/js/chosen/chosen.css" />

        <script>
            $(function () {

                window.setTimeout(function () {
                    $(".chzn-select").chosen({ width: "220px" });
                }, 100);


            });
    </script>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>新建付款单</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentDescription" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款方式
	：</td>
	<td height="25" width="*" align="left">
	    <asp:DropDownList ID="DropDownList_MethodPayment" runat="server">
        </asp:DropDownList>
        现金,银行转</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		供应商
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSupplierName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		客户名称 
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtClientName" runat="server" Width="200px"></asp:TextBox>
	&nbsp;客户可能也是付款人</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPayeeFullName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人帐号 
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PayeeAccountId" runat="server" class="chzn-select"   data-placeholder="点击选择人...">
        </asp:DropDownList>
        没有则为0</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtPaymentDate" runat="server" Width="200px"   class="Wdate"  onClick="WdatePicker()"  ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		预定付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtTargetDate" runat="server" Width="200px"  class="Wdate"  onClick="WdatePicker()" ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemark" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPaymentAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款银行账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_PaymentBankAccountId" runat="server" class="chzn-select"   data-placeholder="选择银行帐号" >
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
        </asp:DropDownList>
        0-应付,1-已付
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>
</asp:Content>

