﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Finance.AdvicePayment
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("ADVICEPAYMENT_MANAGE") &&
(base.ValidatePermission("ADVICEPAYMENT_MANAGE_LIST_SATE1_VIEW") || base.ValidatePermission("ADVICEPAYMENT_MANAGE_LIST_SATE0_VIEW"))
))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.AdvicePayment bll = new XinYiOffice.BLL.AdvicePayment();
            //XinYiOffice.Model.AdvicePayment model = bll.GetModel(Id);

            try
            {

                DataSet ds = DbHelperSQL.Query(string.Format("select * from vAdvicePayment where Id={0}", Id));
                DataRow dr = ds.Tables[0].Rows[0];


                this.lblId.Text = SafeConvert.ToString(dr["Id"]);
                this.lblDocumentNumber.Text = SafeConvert.ToString(dr["DocumentNumber"]);
                this.lblDocumentDescription.Text = SafeConvert.ToString(dr["DocumentDescription"]);

                this.lblMethodPayment.Text = SafeConvert.ToString(dr["MethodPaymentName"]);

                this.lblSupplierName.Text = SafeConvert.ToString(dr["SupplierName"]);
                this.lblClientName.Text = SafeConvert.ToString(dr["ClientName"]);
                this.lblPayeeFullName.Text = SafeConvert.ToString(dr["PayeeFullName"]);
                this.lblPayeeAccountId.Text = SafeConvert.ToString(dr["PayeeAccountName"]);
                this.lblPaymentDate.Text = SafeConvert.ToString(dr["PaymentDate"]);
                this.lblTargetDate.Text = SafeConvert.ToString(dr["TargetDate"]);
                this.lblRemark.Text = SafeConvert.ToString(dr["Remark"]);
                this.lblPaymentAmount.Text = SafeConvert.ToString(dr["PaymentAmount"]);

                this.lblPaymentBankAccountId.Text = SafeConvert.ToString(dr["BankAccountName"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountName"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }


        }


    }
}
