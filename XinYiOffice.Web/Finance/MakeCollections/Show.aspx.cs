﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Finance.MakeCollections
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.ValidatePermission("MAKECOLLECTIONS_MANAGE_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.MakeCollections bll = new XinYiOffice.BLL.MakeCollections();
            //XinYiOffice.Model.MakeCollections model = bll.GetModel(Id);
            
            //this.lblId.Text = model.Id.ToString();
            //this.lblDocumentNumber.Text = model.DocumentNumber;
            //this.lblDocumentDescription.Text = model.DocumentDescription;
            //this.lblMethodPayment.Text = model.MethodPayment.ToString();
            //this.lblSupplierName.Text = model.SupplierName;
            //this.lblGatheringFullName.Text = model.GatheringFullName;
            //this.lblGatheringAccountId.Text = model.GatheringAccountId.ToString();
            //this.lblReceiptDate.Text = model.ReceiptDate.ToString();
            //this.lblTargetDate.Text = model.TargetDate.ToString();
            //this.lblRemark.Text = model.Remark;
            //this.lblClientName.Text = model.ClientName;
            //this.lblGatheringAmount.Text = model.GatheringAmount.ToString();
            //this.lblGatheringBankAccountId.Text = model.GatheringBankAccountId.ToString();
            //this.lblSate.Text = model.Sate.ToString();
            //this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            //this.lblCreateTime.Text = model.CreateTime.ToString();

            string mysql = string.Format("select top 1 * from  vMakeCollections where Id={0}",Id.ToString());

            try
            {
                DataSet ds = DbHelperSQL.Query(mysql);
                DataRow dr = ds.Tables[0].Rows[0];

                this.lblId.Text = SafeConvert.ToString(dr["Id"]);
                this.lblDocumentNumber.Text = SafeConvert.ToString(dr["DocumentNumber"]);
                this.lblDocumentDescription.Text = SafeConvert.ToString(dr["DocumentDescription"]);
                this.lblMethodPayment.Text = SafeConvert.ToString(dr["MethodPaymentName"]);
                this.lblSupplierName.Text =SafeConvert.ToString(dr["SupplierName"]);
                this.lblGatheringFullName.Text = SafeConvert.ToString(dr["GatheringFullName"]);
                this.lblGatheringAccountId.Text = SafeConvert.ToString(dr["GatheringBankAccountName"]);

                this.lblReceiptDate.Text = SafeConvert.ToString(dr["ReceiptDate"]);
                this.lblTargetDate.Text = SafeConvert.ToString(dr["TargetDate"]);
                this.lblRemark.Text = SafeConvert.ToString(dr["Remark"]);
                this.lblClientName.Text = SafeConvert.ToString(dr["ClientName"]);
                this.lblGatheringAmount.Text = SafeConvert.ToString(dr["GatheringAmount"]);
                this.lblGatheringBankAccountId.Text = SafeConvert.ToString(dr["GatheringBankAccountName"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountName"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }
            finally
            { 
            }
            

        }


    }
}
