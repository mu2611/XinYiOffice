﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Finance.MakeCollections.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script src="/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/js/chosen/chosen.css" />

        <script>
            $(function () {

                window.setTimeout(function () {
                    $(".chzn-select").chosen({ width: "220px" });
                    $(".chzn-select2").chosen({ width: "350px" });
                }, 100);


            });
    </script>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加收款单</h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDocumentDescription" runat="server" Width="600px" 
            Height="120px" TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款方式
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_MethodPayment" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		供应商
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSupplierName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtGatheringFullName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款人用户帐号
	：</td>
	<td height="25" width="*" align="left">
		
        	    <asp:DropDownList ID="DropDownList_GatheringAccountId" runat="server"  
            class="chzn-select"   data-placeholder="点击选择人..." tabindex="4">
        </asp:DropDownList>

	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtReceiptDate" runat="server" Width="200px"  
             class="Wdate"  onClick="WdatePicker()" ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		约定收款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtTargetDate" runat="server" Width="200px"  
            class="Wdate"  onClick="WdatePicker()" ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRemark" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		客户
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtClientName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtGatheringAmount" runat="server" Width="100px">0.00</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款银行账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_GatheringBankAccountId" runat="server" class="chzn-select2" Width="350"   data-placeholder="选择银行帐号">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
        </asp:DropDownList>
&nbsp;0-应收,1-已收
	</td></tr>
	</table>


</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>
</asp:Content>
