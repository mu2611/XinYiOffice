﻿<%@ Page Title="工资管理" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Finance.PayrollControl.List" %>
<%@ Register src="~/InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2"  >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            
              <%if (ValidatePermission("PAYROLLCONTROL_MANAGE_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_ADD"))
              { %>
            <a href="add.aspx" id="addLink" runat="server">添加</a>
            <%} %>
            <%if (ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                
                  <uc1:searchapp ID="SearchApp1" runat="server" TableName="vProjectInfoList"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="Discuss_table">

                <asp:Repeater ID="repPayrollControl" runat="server" >
                <HeaderTemplate>
                <tr>
                <th><input type="checkbox" id="cb_all"></th>
                <th>工资年月日期</th>
                <th>人员帐号</th>
                <th>实际发放工资</th>
                <th>工资状态</th>
                 <th>工资单创建人</th>
                 <th>支付账户</th>
                 <th>创建时间</th>
                
                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td ><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><%#DataBinder.Eval(Container.DataItem, "PayDate")%></td>
                <td><a href="show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "PersonnelAccountName")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ActualPayment")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "SateName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateAccountName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "BankAccountName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>

                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>
<%=pagehtml%>
</form>

<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>