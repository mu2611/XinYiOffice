﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Permissions
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Permissions bll = new XinYiOffice.BLL.Permissions();
            XinYiOffice.Model.Permissions model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtName.Text = model.Name;
            this.txtSign.Text = model.Sign;

            DropDownList_PermissionCategoriesId.DataSource = new BLL.PermissionCategories().GetAllList();
            DropDownList_PermissionCategoriesId.DataTextField = "Name";
            DropDownList_PermissionCategoriesId.DataValueField = "Id";
            DropDownList_PermissionCategoriesId.DataBind();

            DropDownList_PermissionCategoriesId.SelectedValue = model.PermissionCategoriesId.ToString();

            this.HiddenField_txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.HiddenField_txtRefreshAccountId.Value = model.RefreshAccountId.ToString();
            this.HiddenField_txtCreateTime.Value = model.CreateTime.ToString();
            this.HiddenField_txtRefreshTime.Value = model.RefreshTime.ToString();

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string Name = this.txtName.Text;
            string Sign = this.txtSign.Text;
            int PermissionCategoriesId = SafeConvert.ToInt(DropDownList_PermissionCategoriesId.SelectedValue);
            int CreateAccountId = SafeConvert.ToInt(HiddenField_txtCreateAccountId.Value);
            int RefreshAccountId = SafeConvert.ToInt(HiddenField_txtRefreshAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_txtCreateTime.Value);
            DateTime RefreshTime = SafeConvert.ToDateTime(HiddenField_txtRefreshTime.Value);


            XinYiOffice.Model.Permissions model = new XinYiOffice.Model.Permissions();
            model.Id = Id;
            model.Name = Name;
            model.Sign = Sign;
            model.PermissionCategoriesId = PermissionCategoriesId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Permissions bll = new XinYiOffice.BLL.Permissions();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
