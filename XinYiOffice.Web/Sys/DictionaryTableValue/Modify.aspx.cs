﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.DictionaryTableValue
{
    public partial class Modify : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }

        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.DictionaryTableValue bll = new XinYiOffice.BLL.DictionaryTableValue();
            XinYiOffice.Model.DictionaryTableValue model = bll.GetModel(Id);
            
            this.lblId.Text = model.Id.ToString();
            this.txtDictionaryTableId.Text = model.DictionaryTableId.ToString();
            this.txtDisplayName.Text = model.DisplayName;
            this.txtValue.Text = model.Value;
            this.txtSort.Text = model.Sort.ToString();


        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            int DictionaryTableId = int.Parse(this.txtDictionaryTableId.Text);
            string DisplayName = this.txtDisplayName.Text;
            string Value = this.txtValue.Text;
            int Sort = int.Parse(this.txtSort.Text);

            int RefreshAccountId = CurrentAccountId;
            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.DictionaryTableValue model = new XinYiOffice.Model.DictionaryTableValue();
            model.Id = Id;
            model.DictionaryTableId = DictionaryTableId;
            model.DisplayName = DisplayName;
            model.Value = Value;
            model.Sort = Sort;

            model.RefreshAccountId = RefreshAccountId;

            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;


            XinYiOffice.BLL.DictionaryTableValue bll = new XinYiOffice.BLL.DictionaryTableValue();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx?DictionaryTableId=" + DictionaryTableId);
        }


    }
}
