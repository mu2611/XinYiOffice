﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using System.Collections.Generic;

namespace XinYiOffice.Web.Sys.DictionaryTableValue
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        protected void InitData()
        {
            try
            {
                int iDictionaryTableId = SafeConvert.ToInt(xytools.url_get("DictionaryTableId"));
                int maxvalue = 0;

                hidDictionaryTableId.Value = iDictionaryTableId.ToString();
                Model.DictionaryTable dic = new BLL.DictionaryTable().GetModel(iDictionaryTableId);

                if (dic != null && !string.IsNullOrEmpty(dic.Name))
                {
                    txtDictionaryTableId.Text = dic.Name;
                }

                DataSet dicTableValueList=new BLL.DictionaryTableValue().GetList(string.Format("DictionaryTableId={0} and TenantId={1} ", iDictionaryTableId,CurrentTenantId));
                if (dicTableValueList!=null)
                {
                    dicTableValueList.Tables[0].DefaultView.Sort = "CreateTime desc";
                    
                    foreach (DataRow dr in dicTableValueList.Tables[0].Rows)
                    {
                        if (maxvalue < SafeConvert.ToInt(dr["Value"],0))
                        {
                            maxvalue = SafeConvert.ToInt(dr["Value"]);
                        }
                    }
                }

                txtValue.Text = SafeConvert.ToInt(maxvalue+1).ToString();
                
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                Response.Redirect("/Sys/DictionaryTable/List.aspx");
            }
            finally
            { 
            }

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx?DictionaryTableId=" + hidDictionaryTableId.Value);
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int DictionaryTableId = SafeConvert.ToInt(hidDictionaryTableId.Value, 0);
            string DisplayName = this.txtDisplayName.Text;
            string Value = this.txtValue.Text;
            int Sort = SafeConvert.ToInt(this.txtSort.Text);
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.DictionaryTableValue model = new XinYiOffice.Model.DictionaryTableValue();
            model.DictionaryTableId = DictionaryTableId;
            model.DisplayName = DisplayName;
            model.Value = Value;
            model.Sort = Sort;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.DictionaryTableValue bll = new XinYiOffice.BLL.DictionaryTableValue();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx?DictionaryTableId=" + DictionaryTableId);
        }
    }
}
