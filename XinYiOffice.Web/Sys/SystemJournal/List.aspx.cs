﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using System.Text;
using XinYiOffice.Common;
using System.Data;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Sys.SystemJournal
{
    public partial class List : BasicPage
    {
        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("ACCOUNT_MANAGE") && ValidatePermission("ACCOUNT_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }

            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                //switch (action)
                //{
                //    case "search":
                //        SearchApp1.SetSqlWhere();
                //        BindData();
                //        break;
                //}
            }
        }

        #region gridView
        public void BindData()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vSystemJournal ");
            strSql.AppendFormat("where  1=1 and TenantId={0} ", CurrentTenantId);
            //strSql.Append(SearchApp1.SqlWhere);
            strSql.Append(" order by Id desc ");

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            DataTable dt = pd.GetListPageBySql(strSql.ToString(), _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion

            repSystemJournal.DataSource = dt;
            repSystemJournal.DataBind();
        }
        #endregion
    }
}