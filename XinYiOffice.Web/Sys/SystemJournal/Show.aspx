﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Sys.SystemJournal.Show" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>日志详情</h4></div>
 
   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		日志类型
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		操作内容
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblOperationContent" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		当前用户id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCurrentAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		当前IP地址
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblIPAddress" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		系统信息
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSystemInfo" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
	
</table>
</div>
            </form>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cp2" runat="server">
</asp:Content>
