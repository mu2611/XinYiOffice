﻿<%@ Page Title="帐号 用于帐号登录" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Sys.Accounts.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
<script>
    $(function () { 
    
    $(".select_role").click(function () {

        var len = $("input[name='chk_list']:checked").length;
        var v = $("input[name='chk_list']:checked").attr("va");
        if (len != 1) { alert('请选择一个进行分配'); return; }


        $.layer({
            type: 2,
            title: ['分配角色', true],
            iframe: { src: '/Sys/Roles/SelectRoles.aspx?accountId='+v+"&sj="+Math.random() },
            area: ['650px', '320px'],
            offset: ['100px', ''],
            border: [5, 0.3, '#000', true]
        
        })

 

    });
});
</script>

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2">
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            
  <%if (ValidatePermission("ACCOUNT_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("ACCOUNT_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("ACCOUNT_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("ACCOUNT_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

              <%if (ValidatePermission("ACCOUNT_LIST_ALLOT"))
              { %>
            <a href="javascript:;" class="select_role">分配角色</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:searchapp ID="SearchApp1" runat="server" TableName="Accounts"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ClientTracking_table">

                <asp:Repeater ID="repAccounts" runat="server" >
                <HeaderTemplate>
                <tr>
                <th class="center"><input type="checkbox" id="cb_all"></th>
                <th>用户名</th>
                <th>状态</th>
                <th>昵称</th>
                <th>姓名</th>
                <th>头像</th>
                <th>帐号类型</th>
                <th>创建时间</th>
                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "AccountName")%></a></td>
                <td><%# GetSateName(DataBinder.Eval(Container.DataItem, "Sate").ToString())%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "NiceName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "FullName")%></td>
                <td>
                <img src="<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem,"HeadPortrait").ToString())%>" width="37" height="37"/>
                </td>

                <td><%#GetAccountType(DataBinder.Eval(Container.DataItem, "AccountType").ToString())%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime","{0:yyyy-MM-dd}")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>


</form>
<%=pagehtml%>
<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>

</asp:Content>