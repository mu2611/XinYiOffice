﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Basic;
using System.Web.UI.HtmlControls;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.MenuTree
{
    public partial class List : BasicPage
    { 
        XinYiOffice.BLL.MenuTree bll = new XinYiOffice.BLL.MenuTree();
        public int lay;
        public int indexNum;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MENUTREE_LIST"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            
        }

        protected void BindData()
        {
            GetMenuTree(0, ref tbody_cell);
        }

        #region 菜单
        public void GetMenuTree(int pid, ref System.Web.UI.HtmlControls.HtmlGenericControl hgc)
        {
            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0}", pid));

            int CurrentId = 0;
            int CurrentParentMenuTreeId = 0;
            string _format = string.Empty;

            foreach (DataRow _dr in dr)
            {
                lay++;//层级

                CurrentId = SafeConvert.ToInt(_dr["Id"], 1);
                CurrentParentMenuTreeId = SafeConvert.ToInt(_dr["MenuTreeId"], 0);

                string name = SafeConvert.ToString(_dr["Name"]);
                bool isClass = GetClassInExist(CurrentId);

                System.Web.UI.HtmlControls.HtmlTableRow htr = new System.Web.UI.HtmlControls.HtmlTableRow();

                System.Web.UI.HtmlControls.HtmlTableCell htc_1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_2 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_3 = new System.Web.UI.HtmlControls.HtmlTableCell();

                HtmlGenericControl span1 = new HtmlGenericControl("span");
                HtmlGenericControl span2 = new HtmlGenericControl("span");
                HtmlGenericControl span3 = new HtmlGenericControl("span");
                
                if (isClass)
                {

                    htc_1.Controls.Add(new LiteralControl(string.Format("<span class='folder'><a href=\"Modify.aspx?Id={1}\">{0}</a></span>", name, CurrentId)));
                }
                else
                {
                    htc_1.Controls.Add(new LiteralControl(string.Format("<span class='file'><a href=\"Modify.aspx?Id={1}\">{0}</a></span>", name, CurrentId)));
                }

                span2.InnerText = SafeConvert.ToString(_dr["IsShow"]);
                htc_2.Controls.Add(span2);

                span3.InnerText = SafeConvert.ToString(_dr["Sort"]);
                htc_3.Controls.Add(span3);

                //1个层级
                htr.Attributes.Add("data-tt-id", CurrentId.ToString());

                //有父级
                if (CurrentParentMenuTreeId != 0)
                {
                    htr.Attributes.Add("data-tt-parent-id", CurrentParentMenuTreeId.ToString());
                }

                htr.Controls.Add(htc_1);
                htr.Controls.Add(htc_2);
                htr.Controls.Add(htc_3);

                hgc.Controls.Add(htr);

                if (isClass)
                {
                    GetMenuTree(CurrentId, ref hgc);
                }

                lay--;//层级减1
                indexNum++;//序列加1
            }
        }

       
        /// <summary>
        /// 是否有子集
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        protected static bool GetClassInExist(int cid)
        {
            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0}",cid));

            if (dr != null && dr.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Add.aspx");
        }



    }
}
