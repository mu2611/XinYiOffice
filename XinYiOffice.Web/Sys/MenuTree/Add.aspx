﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.MenuTree.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>新建菜单</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		唯一签名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtGuid" runat="server" Width="200px"></asp:TextBox>(建议不做更改)
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		菜单名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		上级
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_MenuTreeId" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		链接地址
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtChainedAddress" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		是否显示
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsShow" runat="server" Width="200px">1</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSort" runat="server" Width="200px">1</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		菜单小图标
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIcon" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		菜单大图标：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIconMax" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		菜单说明
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDescription" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>

    </div>

     <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

    </form>
</asp:Content>
