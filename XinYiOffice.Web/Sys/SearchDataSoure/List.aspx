﻿<%@ Page Title="搜索项所使用的数据源" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Sys.SearchDataSoure.List" %>


<script language="javascript" src="../js/CheckBox.js" type="text/javascript"></script>

<form id="f2" name="f2" runat="server">
<!--Title -->

            <!--Title end -->

            <!--Add  -->

            <!--Add end -->

            <!--Search -->
            <table style="width: 100%;" cellpadding="2" cellspacing="1" class="border">
                <tr>
                    <td style="width: 80px" align="right" class="tdbg">
                         <b>关键字：</b>
                    </td>
                    <td class="tdbg">                       
                    <asp:TextBox ID="txtKeyword" runat="server"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnSearch" runat="server" Text="查询"  OnClick="btnSearch_Click" >
                    </asp:Button>                    
                        
                    </td>
                    <td class="tdbg">
                    </td>
                </tr>
            </table>
            <!--Search end-->
            <br />
            <asp:GridView ID="gridView" runat="server" AllowPaging="True" Width="100%" CellPadding="3"  OnPageIndexChanging ="gridView_PageIndexChanging"
                    BorderWidth="1px" DataKeyNames="Id" OnRowDataBound="gridView_RowDataBound"
                    AutoGenerateColumns="false" PageSize="10"  RowStyle-HorizontalAlign="Center" OnRowCreated="gridView_OnRowCreated">
                    <Columns>
                    <asp:TemplateField ControlStyle-Width="30" HeaderText="选择"    >
                                <ItemTemplate>
                                    <asp:CheckBox ID="DeleteThis" onclick="javascript:CCA(this);" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField> 
                            
		<asp:BoundField DataField="DataSoureType" HeaderText="数据源 1-数据库,2-文本文" SortExpression="DataSoureType" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="DataSoureConntion" HeaderText="数据源连接字符串 若为数据库则" SortExpression="DataSoureConntion" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="DataType" HeaderText="数据格式 字符串,xml,js" SortExpression="DataType" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="DisplayValue" HeaderText="代表显示的值 比如 查询出来的" SortExpression="DisplayValue" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="TrueValue" HeaderText="代表实际的值 而 Id 则为下" SortExpression="TrueValue" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="FormType" HeaderText="输入表单类型 1-文本,2-下" SortExpression="FormType" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="FormStyle" HeaderText="表单样式 作为style=""" SortExpression="FormStyle" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="FormClass" HeaderText="表单使用Class" SortExpression="FormClass" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="CreateAccountId" HeaderText="添加者id" SortExpression="CreateAccountId" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="RefreshAccountId" HeaderText="更新者id" SortExpression="RefreshAccountId" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="CreateTime" HeaderText="创建时间" SortExpression="CreateTime" ItemStyle-HorizontalAlign="Center"  /> 
		<asp:BoundField DataField="RefreshTime" HeaderText="更新时间 最近一次编辑的人员I" SortExpression="RefreshTime" ItemStyle-HorizontalAlign="Center"  /> 
                            
                            <asp:HyperLinkField HeaderText="详细" ControlStyle-Width="50" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="Show.aspx?id={0}"
                                Text="详细"  />
                            <asp:HyperLinkField HeaderText="编辑" ControlStyle-Width="50" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="Modify.aspx?id={0}"
                                Text="编辑"  />
                            <asp:TemplateField ControlStyle-Width="50" HeaderText="删除"   Visible="false"  >
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete"
                                         Text="删除"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                </asp:GridView>
               <table border="0" cellpadding="0" cellspacing="1" style="width: 100%;">
                <tr>
                    <td style="width: 1px;">                        
                    </td>
                    <td align="left">
                        <asp:Button ID="btnDelete" runat="server" Text="删除" OnClick="btnDelete_Click"/>                       
                    </td>
                </tr>
            </table>
</form>
