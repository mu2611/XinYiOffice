﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace XinYiOffice.Web.Sys.SearchDataSoure
{
    public partial class Show : Page
    {        
        		public string strid=""; 
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					strid = Request.Params["id"];
					int Id=(Convert.ToInt32(strid));
					ShowInfo(Id);
				}
			}
		}
		
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.SearchDataSoure bll=new XinYiOffice.BLL.SearchDataSoure();
		XinYiOffice.Model.SearchDataSoure model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.lblDataSoureType.Text=model.DataSoureType.ToString();
		this.lblDataSoureConntion.Text=model.DataSoureConntion;
		this.lblDataType.Text=model.DataType.ToString();
		this.lblDisplayValue.Text=model.DisplayValue;
		this.lblTrueValue.Text=model.TrueValue;
		this.lblFormType.Text=model.FormType.ToString();
		this.lblFormStyle.Text=model.FormStyle;
		this.lblFormClass.Text=model.FormClass;
		this.lblCreateAccountId.Text=model.CreateAccountId.ToString();
		this.lblRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.lblCreateTime.Text=model.CreateTime.ToString();
		this.lblRefreshTime.Text=model.RefreshTime.ToString();

	}


    }
}
