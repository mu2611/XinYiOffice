﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SearchDataSoure
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int DataSoureType = int.Parse(this.DropDownList_DataSoureType.SelectedValue);
            string DataSoureConntion = this.txtDataSoureConntion.Text;
            int DataType = int.Parse(this.DropDownList_DataType.SelectedValue);
            string DisplayValue = this.txtDisplayValue.Text;
            string TrueValue = this.txtTrueValue.Text;
            int FormType = int.Parse(this.DropDownList_FormType.SelectedValue);
            string FormStyle = this.txtFormStyle.Text;
            string FormClass = this.txtFormClass.Text;
            int CreateAccountId = CurrentAccountId;
             
            DateTime CreateTime = DateTime.Now;


            XinYiOffice.Model.SearchDataSoure model = new XinYiOffice.Model.SearchDataSoure();
            model.DataSoureType = DataSoureType;
            model.DataSoureConntion = DataSoureConntion;
            model.DataType = DataType;
            model.DisplayValue = DisplayValue;
            model.TrueValue = TrueValue;
            model.FormType = FormType;
            model.FormStyle = FormStyle;
            model.FormClass = FormClass;
            model.CreateAccountId = CreateAccountId;

            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.SearchDataSoure bll = new XinYiOffice.BLL.SearchDataSoure();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
