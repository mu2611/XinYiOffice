﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web
{
    public partial class ChangePassword :BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void alinkSave_Click(object sender, EventArgs e)
        {
            bool isNull = xytools.DataIsNoNull(TextBox_oldpas.Text, TextBox_newpassword, TextBox_newpassword2);
            if (isNull)
            {
                if (TextBox_oldpas.Text != CurrentAccount.AccountPassword)
                {
                    xytools.web_alert("旧密码输入不正确!");
                    return;
                }
                if (TextBox_newpassword.Text != TextBox_newpassword2.Text)
                {
                    xytools.web_alert("新密码确认输入不正确");
                    return;
                }

                try
                {
                    Model.Accounts acc = CurrentAccount;
                    acc.AccountPassword = TextBox_newpassword2.Text;
                    new BLL.Accounts().Update(acc);
                    xytools.web_alert("已更改!");
                }
                catch (Exception ex)
                {
                    EasyLog.WriteLog(ex);
                    xytools.web_alert("服务器忙,请稍候重试");

                }
                finally
                {
                }

            }
        }

    }
}