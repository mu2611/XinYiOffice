﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Header.aspx.cs" Inherits="XinYiOffice.Web.Header" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="/css/init.css" rel="stylesheet" type="text/css" />
<link href="/css/head.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="/js/init.js"></script>
<script type="text/javascript">
    $(function () {
        var topWindow = $(window.parent.document);
        $('.head_pos_f').click(function () {
            topWindow.find("iframe[name='leftlist']").attr('src', 'LeftMent.aspx');
            $('#top_nav li').removeClass('active');
            $(this).hide(); $('.head_pos').show();
            var aLi = topWindow.find('.show_nav li');
            var aIfream = topWindow.find('.show_iframe');
            var nav = topWindow.find('#min_title_list');
            var iframe_box = topWindow.find('#iframe_box');
            var have = false;
            aLi.each(function () {
                if ($(this).find('span').html() == '我的桌面') {
                    aLi.removeClass('act'); $(this).addClass('act');
                    var index = aLi.index($(this));
                    aIfream.hide().eq(index).show();
                    have = true;
                    return false;
                }
            })
            if (!have) {
                aLi.removeClass('act');
                nav.append('<li class="act"><span title="我的桌面">我的桌面</span><a href="javascript:;"></a></li>');
                aIfream.hide();
                iframe_box.append('<div class="show_iframe"><div class="mb"></div><iframe frameborder="0" src="/Desktop.aspx"></iframe></div>');
                var showBox = iframe_box.find('.show_iframe:visible')
                showBox.find('iframe').hide().load(function () {
                    showBox.find('.mb').hide();
                    $(this).show()
                })
            }
        })

        $("#search_button").click(function () {
            var iframe_box = topWindow.find('#iframe_box');
            iframe_box.append('<div class="show_iframe"><div class="mb"></div><iframe frameborder="0" src="/Search.aspx"></iframe></div>');
            //creatIframe('ss.aspx','搜索');
        });

        //根据guid判断出网盘链接
        $("#top_nav li a").each(function () {
            var t = $(this);
            var guid = t.attr("guid");
            var url = '';

            t.click(function () {
                var maxcon = topWindow.find("#maxcon");
                maxcon.html('');
                switch (guid) {
                    case "9891e81d-ff2e-4a6b-9c15-845046019052":
                        url = $(this).attr("href") + '?sj=' + Math.random();
                        maxcon.append('<div class="mb"></div><iframe id="ff" name="ff" frameborder="0" scrolling="no" src="' + url + '" style="position:relative; height:100%;" ></iframe>');
                        maxcon.find("#ff").load(function () {
                            topWindow.find("#left_list").hide();
                            topWindow.find("#right_show").hide();

                            var mainheight = $(this).contents().find("html").height();
                            if (mainheight <= 600) {
                                maxcon.find("#ff").height(600);
                            }
                            else {
                                maxcon.find("#ff").height(mainheight);
                            }

                            topWindow.find("#maxcon").find('.mb').hide();
                        });
                        break;

                    default:
                        topWindow.find("#left_list").show();
                        topWindow.find("#right_show").show();
                        break;
                } //switch
            });


        })
    })
</script>
</head>

<body>
<div id="head">
	<div id="head_top" class="clear">
    	<h1><a href="javascript:;">新亿Office</a></h1>
        <div id="OAUser_content">
        	<span id="portrait"><img src="<%=GetAccountsHeadPortrait()%>" title="<%=CurrentAccountNiceName%>" width="37" height="37"/></span>
            <span class="Welcome">您好</span>
            <span id="UserName"><a href="javascript:;"><%=CurrentAccountNiceName%></a></span>
            
            <span id="head_search_box" class="clear" style=" display:none;">
            	<input id="search_text" type="text" value="搜索联系人/内部信件/邮件" onclick="if(this.value='搜索联系人/内部信件/邮件'){this.value='';}" onblur="if(this.value==''){this.value='搜索联系人/内部信件/邮件';}" />
                <input id="search_button" type="button" value=" " />
            </span>

            <span id="login_Set"><a href="LeftMent.aspx" target="leftlist">设置</a>|<a href="/OutLogin.aspx" target="_top">注销</a></span>
        </div>
    </div>
    <div id="nav" class="clear">
    	<div class="head_pos cc_t">现在位置：我的桌面</div>
        <div class="head_pos_f cc_t" style="display:none"><a href="javascript:;"></a></div>
        <ul id="top_nav">
            
            <asp:Repeater ID="repMent" runat="server">
                <ItemTemplate>
                <li><a href="<%#DataBinder.Eval(Container.DataItem, "ChainedAddress")%>" target="leftlist" guid="<%#DataBinder.Eval(Container.DataItem, "Guid")%>"><%#DataBinder.Eval(Container.DataItem, "Name")%></a></li>
                </ItemTemplate>
            </asp:Repeater>


        </ul>
    </div>
</div>
</body>
</html>