﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.DesktopPlug
{
    public partial class _ClientTracking : BasicUserControl
    {
        public int ClientTrackingCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
           if(!IsPostBack)
           {
               InitData();
           }
        }

        protected void InitData()
        {
            try
            {
                StringBuilder strWhere = new StringBuilder();

                strWhere.AppendFormat(" Sate=1 and ReturnAccountId={0} and TenantId={1}", CurrentAccountId,CurrentTenantId);
                
                ClientTrackingCount = new BLL.ClientTracking().GetRecordCount(strWhere.ToString());

                repClientTracking.DataSource = ClientServer.GetClientTracking(strWhere.ToString(), 6);
                repClientTracking.DataBind();
            }
            catch
            { 
            }

        }
    }
}