﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;
using System.Data;

namespace XinYiOffice.Web.DesktopPlug
{
    public partial class _MyProject : BasicUserControl
    {
        public string MyProjectListCount = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        protected void InitData()
        {
            DataTable dt=ProjectInfoServer.GetMyProjectList(CurrentAccountId, 6, CurrentTenantId);
            MyProjectListCount = dt.Rows.Count.ToString();
            
            repProject.DataSource = dt;
            repProject.DataBind();
        }
    }
}