﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientInfo.Add" Title="增加页" EnableEventValidation="false" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>

	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

<script>
    $(function () {
        var DropDownList_Province = $("#<%=DropDownList_Province.ClientID%>");
        var DropDownList_City = $("#<%=DropDownList_City.ClientID%>");
        var HiddenField_City = $("#<%=HiddenField_City.ClientID%>");

        DropDownList_Province.click(function () {
            DropDownList_City.html("");
            $.getJSON("/Call/Ajax.aspx?action=getcity&pid=" + DropDownList_Province.val() + "&sj=" + Math.random(), function (json) {

                $.each(json.Data, function (i) {
                    DropDownList_City.append("<option value='" + json.Data[i].Id + "'>" + json.Data[i].Name + "</option>");
                });
            });

        });


        DropDownList_City.change(function () {
            HiddenField_City.val(DropDownList_City.val());
        });

        var autoSupClientInfoId = $("#<%=TextBox_SupClientInfoId.ClientID%>");
        var autoHiddenField_SupClientInfoId = $("#<%=hidSupClientInfoId.ClientID%>");
        var url_getclient = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random();
        //客户
        autoSupClientInfoId.autocomplete({
            source: url_getclient,
            select: function (event, ui) {
                //alert(ui.item.value);
                autoSupClientInfoId.val(ui.item.label);
                autoHiddenField_SupClientInfoId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoSupClientInfoId.val(ui.item.label);
                return false;
            }
        });

        var auto_MarketingPlanId = $("#<%=TextBox_MarketingPlanId.ClientID%>");
        var auto_hid_MarketingPlanId = $("#<%=hid_MarketingPlanId.ClientID%>");
        var url_MarketingPlanId = "/Call/Ajax.aspx?action=getmarketingplan&sj=" + Math.random();

        //获取营销计划
        auto_MarketingPlanId.autocomplete({
            source: url_MarketingPlanId,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_MarketingPlanId.val(ui.item.label);
                auto_hid_MarketingPlanId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_MarketingPlanId.val(ui.item.label);
                return false;
            }
        });


    })
</script>
<style>
form,table{ font-size:12px;}
</style>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
     	<div class="h_title"><h4>基本信息</h4></div>
        
        <table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="13%" align="right" class="table_left">所属人职员：</td>
	<td height="25" width="36%" align="left">
		<asp:TextBox id="txtByAccountId" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
        
	</td>
	<td width="25%" align="left" class="table_left">客户编号
	：</td>
	<td width="26%" align="left"><asp:TextBox ID="txtIntNumber" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		客户名称
	：</td>
	<td height="25" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px" MaxLength="20" ></asp:TextBox>
	</td>
	<td height="25" align="left" class="table_left">权限属性
	：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_Power" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		热度
	：</td>
	<td height="25" align="left">
		<asp:DropDownList ID="DropDownList_txtHeat" runat="server">
        </asp:DropDownList>
	</td>
	<td height="25" align="left" class="table_left">信用等级
	：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_txtQualityRating" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		类型
	：</td>
	<td height="25" align="left">
		<asp:DropDownList ID="DropDownList_txtClientType" runat="server">
        </asp:DropDownList>
	</td>
	<td height="25" align="left" class="table_left">价值评估
	：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_txtValueAssessment" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		所属行业
	：</td>
	<td height="25" align="left">
		<asp:DropDownList ID="DropDownList1_txtIndustry" runat="server">
        </asp:DropDownList>
	</td>
	<td height="25" align="left" class="table_left">关系等级
	：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_txtRelBetweenGrade" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		来源
	：</td>
	<td height="25" align="left">
		<asp:DropDownList ID="DropDownList_txtSource" runat="server">
        </asp:DropDownList>
	</td>
	<td height="25" align="left" class="table_left">客户级别
	：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_txtCustomerLevel" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">标签：</td>
	<td height="25" align="left">
		<asp:TextBox id="txtTag" runat="server" Width="200px"></asp:TextBox>
	</td>
	<td height="25" align="left" class="table_left">发展阶段：
        </td>
	<td height="25" align="left"><asp:DropDownList ID="DropDownList_txtSeedtime" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		开户行
	：</td>
	<td height="25" align="left">
		<asp:TextBox id="txtBankDeposit" runat="server" Width="200px"></asp:TextBox>
	</td>
	<td height="25" align="left" class="table_left">上级客户
	：</td>
	<td height="25" align="left"><asp:TextBox ID="TextBox_SupClientInfoId" runat="server" CssClass="auto_input">无</asp:TextBox>
&nbsp;
<asp:HiddenField ID="hidSupClientInfoId" runat="server" />
(没有可不写)</td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">
		纳税号
	：</td>
	<td height="25" align="left">
		<asp:TextBox id="txtTaxNo" runat="server" Width="200px"></asp:TextBox>
	</td>
	<td height="25" align="left" class="table_left">来源营销计划：</td>
	<td height="25" align="left">
    <asp:TextBox ID="TextBox_MarketingPlanId"  runat="server" CssClass="auto_input"></asp:TextBox>
      <asp:HiddenField ID="hid_MarketingPlanId" runat="server" /></td>
	</tr>
	<tr>
	<td height="25" width="13%" align="right" class="table_left">接受短信号码
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtMobilePhone" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	</table>
<div class="h_title"><h4>备注</h4></div>
            <table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 银行帐号
      ：</td>
    <td width="39%" height="25" align="left"><asp:TextBox id="txtBankAccount" runat="server" Width="200px"></asp:TextBox></td>
    <td width="22%" height="25" align="left" class="table_left">省份
      ：</td>
    <td width="26%" align="left"><asp:DropDownList ID="DropDownList_Province" runat="server"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 国家地区
      ：</td>
    <td height="25" align="left"><asp:DropDownList ID="DropDownListCountriesRegions" runat="server"> </asp:DropDownList></td>
    <td height="25" align="left" class="table_left">城市
      ：      </td>
    <td height="25" align="left"><asp:DropDownList ID="DropDownList_City" runat="server"> </asp:DropDownList>
      <asp:HiddenField ID="HiddenField_City" runat="server" /></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 电话
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox></td>
    <td height="25" align="left" class="table_left">区县
      ：      </td>
    <td height="25" align="left"><asp:TextBox id="txtCounty" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 传真
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtFax" runat="server" Width="200px"></asp:TextBox></td>
    <td height="25" align="left" class="table_left">地址
      ：      </td>
    <td height="25" align="left"><asp:TextBox id="txtAddress" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 邮件
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox></td>
    <td height="25" colspan="2" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 邮编
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtZipCode" runat="server" Width="200px"></asp:TextBox></td>
    <td height="25" colspan="2" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left">公司简介
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtCompanyProfile" 
            runat="server" Width="655px" Height="102px" TextMode="MultiLine"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left">备注
      ：</td>
    <td height="25" colspan="3" align="left">
      <asp:TextBox id="txtRemarks" runat="server" 
            Width="655px" TextMode="MultiLine" Height="102px"></asp:TextBox></td>
    </tr>
  
    <tr>
    <td height="25" width="13%" align="right" class="table_left"> 负责人
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtGeneralManager" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 企业法人
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtBusinessEntity" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 营业执照
      ：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox id="txtBusinessLicence" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  
</table>


<div class="h_title"><h4>联系人</h4></div>
<table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">

  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 联系人：</td>
    <td width="37%" height="25" align="left"><asp:TextBox ID="TextBox_ClientLiaisons_Name" runat="server"></asp:TextBox>
      &nbsp;&nbsp;</td>
    <td width="25%" height="25" align="left" class="table_left"> 性别:      </td>
    <td width="25%" align="left"><asp:DropDownList ID="ddl_ClientLiaisons_Sex" runat="server"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 手机号码:</td>
    <td height="25" align="left"><asp:TextBox ID="txt_ClientLiaisons_phone" runat="server"></asp:TextBox>
      &nbsp;&nbsp;&nbsp;</td>
    <td height="25" align="left" class="table_left">固定电话：      </td>
    <td height="25" align="left"><asp:TextBox ID="txt_OtherPhone" runat="server"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> QQ:</td>
    <td height="25" align="left"><asp:TextBox ID="txt_QQ" runat="server"></asp:TextBox></td>
    <td height="25" align="left" class="table_left">联系人分类：
      </td>
    <td height="25" align="left"><asp:DropDownList 
            ID="DropDownList_ClassiFication" runat="server"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td height="25" width="13%" align="right" class="table_left"> 电子邮件：</td>
    <td height="25" colspan="3" align="left"><asp:TextBox ID="txt_ClientLiaisons_Email" runat="server" Width="200"></asp:TextBox></td>
    </tr>

</table>

          
 </div>
 
       <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        </div>
    

    </form>
</asp:Content>




