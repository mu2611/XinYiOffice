﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using System.Data;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.ClientInfo
{
    public partial class AddClientTracking : BasicPage
    {
        public string clientid = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            clientid = xytools.url_get("clientid");

            if(!IsPostBack)
            {
                InitBind();
            }

        }

        protected void InitBind()
        {
            SetDropDownList_ClientInfo("Heat", ref DropDownList_Head);
            SetDropDownList("PresentStage", ref DropDownList_SalesOpportunitiesSate);
            SetDropDownList("Sate", ref DropDownList_OpportunitiesSate);
            
            SetDropDownList_ClientInfo("Heat", ref DropDownList_ClientHeat);

            TextBox_PlanTime.Text = DateTime.Now.AddDays(1).ToString();

            hidClientId.Value = clientid;
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.SalesOpportunitiesDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void SetDropDownList_ClientInfo(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {

                Model.ClientInfo cinfo = new Model.ClientInfo();
                cinfo = new BLL.ClientInfo().GetModel(SafeConvert.ToInt(hidClientId.Value));

                int SalesOpportunitiesId = 0;

                #region 创建销售机会
                Model.SalesOpportunities so = new Model.SalesOpportunities();
                so.CreateAccountId = CurrentAccountId;
                so.PersonalAccountId = CurrentAccountId;
                so.CreateTime = DateTime.Now;
                so.OccTime = so.CreateTime;
                so.CurrencyNotes = string.Empty;
                so.CustomerService = SafeConvert.ToInt(hidClientId.Value);
                so.EstAmount = SafeConvert.ToDecimal("0.00");
                so.MarketingPlanId = cinfo.MarketingPlanId;//营销计划
                so.NextStep = 0;
                so.Sate = 1;
                so.Source = cinfo.Source;
                //so.StageRemarks = string.Format("创建客户({0})时同步写入空备注", model.Name);
                so.StageRemarks = cinfo.Remarks;
                so.TenantId = CurrentTenantId;

                so.OppName = string.Format("JH-{0}({1}-{2})", cinfo.Name, DateTime.Now.Year, DateTime.Now.Month);
                SalesOpportunitiesId = new BLL.SalesOpportunities().Add(so);
                #endregion

                #region 创建客户跟踪
                Model.ClientTracking ct = new Model.ClientTracking();
                ct.ActualTime = DateTime.Now;
                ct.BackNotes = TextBox_BackNotes.Text;
                ct.ClientHeat = SafeConvert.ToInt(DropDownList_Head.SelectedValue);
                ct.CloseNotes = TextBox_CloseNotes.Text;
                ct.CreateAccountId = CurrentAccountId;
                ct.CreateTime = DateTime.Now;
                ct.IsFollowUp = SafeConvert.ToInt(this.RadioButtonList_IsFollowUp.SelectedValue);
                ct.PlanTime = SafeConvert.ToDateTime(TextBox_PlanTime.Text);
                ct.RefreshAccountId = 0;
                ct.RefreshTime = DateTime.Now;
                ct.ReturnAccountId = CurrentAccountId;

                ct.SalesOpportunitiesId = SalesOpportunitiesId;
                ct.SalesOpportunitiesSate = SafeConvert.ToInt(this.DropDownList_SalesOpportunitiesSate.SelectedValue);
                ct.Sate = SafeConvert.ToInt(this.DropDownList_OpportunitiesSate.SelectedValue);

                ct.TenantId = CurrentTenantId;
                new BLL.ClientTracking().Add(ct);
                #endregion
                xytools.web_alert("已成功添加！");

            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
                xytools.web_alert("服务器忙，请稍候再试s！");
            }
            finally
            { 
            }
        }
    }
}