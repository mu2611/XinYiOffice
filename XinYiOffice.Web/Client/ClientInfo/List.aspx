﻿<%@ Page Title="客户信息表" Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Client.ClientInfo.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2">
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <%if (ValidatePermission("CLIENTINFO_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("CLIENTINFO_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:searchapp ID="SearchApp1" runat="server" TableName="vClientInfo"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

                <asp:Repeater ID="repClientInfo" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>客户名称</th><th>客户级别</th><th>录入时间</th><th>当前热度</th><th>类型</th><th>来源</th><th>信用等级</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#Eval("Name").ToString().Length > 30 ? Eval("Name").ToString().Substring(0, 30) + "..." : Eval("Name")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CustomerLevelName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime","{0:yyyy-MM-dd}")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "HeatName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ClientTypeName")%></td>

                <td><%#DataBinder.Eval(Container.DataItem, "SourceName")%></td>
                
                <td><%#DataBinder.Eval(Container.DataItem, "QualityRatingName")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>
<%=pagehtml%>
</form>
<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>


