﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.ClientLiaisons
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            txtPersonNumber.Text=string.Empty;
            DropDownList_Province.DataSource=AppDataCacheServer.Province;
            DropDownList_Province.DataTextField = "Name";
            DropDownList_Province.DataValueField = "Id";
            DropDownList_Province.DataBind();
            DropDownList_Province.Items.FindByText("北京").Selected=true;

            DropDownList_County.DataSource = AppDataCacheServer.GetCityByPid(SafeConvert.ToInt(DropDownList_Province.SelectedValue));
            DropDownList_County.DataTextField = "Name";
            DropDownList_County.DataValueField = "Id";
            DropDownList_County.DataBind();
            HiddenField_county.Value = DropDownList_County.SelectedValue;


            txtPersonNumber.Text = "CLL"+DateTime.Now.ToString("yyyyMMddmmss");
            SetDropDownList("Power", ref DropDownList_Power);
            SetDropDownList("Source", ref DropDownList_Source);
            SetDropDownList("IfContact", ref DropDownList_IfContact);
            SetDropDownList("ClassiFication", ref DropDownList_ClassiFication);
            SetDropDownList("PrimaryContact", ref DropDownList_PrimaryContact);
            SetDropDownList("Sex", ref DropDownList_Sex);
            
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientLiaisonsDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int ClientInfoId = SafeConvert.ToInt(HiddenField_ClientInfoId.Value);
            string PersonNumber = this.txtPersonNumber.Text;
            string FullName = this.txtFullName.Text;
            int Power = SafeConvert.ToInt(DropDownList_Power.SelectedValue);
            string Phone = this.txtPhone.Text;
            string OtherPhone = this.txtOtherPhone.Text;
            string Fax = this.txtFax.Text;
            string Email = this.txtEmail.Text;
            string QQ = this.txtQQ.Text;
            string Birthday = this.txtBirthday.Text;
            int ByAccountId = SafeConvert.ToInt(HiddenField_accountId.Value);
            string Department = this.txtDepartment.Text;
            string FUNCTION = this.txtFUNCTION.Text;
            string DirectSuperior = this.txtDirectSuperior.Text;
            int Source = SafeConvert.ToInt(DropDownList_Source.SelectedValue);
            int IfContact = SafeConvert.ToInt(DropDownList_IfContact.SelectedValue);
            int ClassiFication = SafeConvert.ToInt(DropDownList_ClassiFication.SelectedValue);
            string Business = this.txtBusiness.Text;
            int PrimaryContact = SafeConvert.ToInt(DropDownList_PrimaryContact.SelectedValue);
            string ShortPhone = this.txtShortPhone.Text;
            int Sex = SafeConvert.ToInt(DropDownList_Sex.SelectedValue);
            string CountriesRegions = this.txtCountriesRegions.Text;
            string Zip = this.txtZip.Text;
            string Address = this.txtAddress.Text;
            int Province = SafeConvert.ToInt(DropDownList_Province.SelectedValue);
            int County = SafeConvert.ToInt(DropDownList_County.SelectedValue);
            string Remarks = this.txtRemarks.Text;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ClientLiaisons model = new XinYiOffice.Model.ClientLiaisons();
            model.ClientInfoId = ClientInfoId;
            model.PersonNumber = PersonNumber;
            model.FullName = FullName;
            model.Power = Power;
            model.Phone = Phone;
            model.OtherPhone = OtherPhone;
            model.Fax = Fax;
            model.Email = Email;
            model.QQ = QQ;
            model.Birthday = Birthday;

            if(ByAccountId==0)
            {
                ByAccountId = CurrentAccountId;
            }

            model.ByAccountId = ByAccountId;

            model.Department = Department;
            model.FunctionName = FUNCTION;
            model.DirectSuperior = SafeConvert.ToInt(DirectSuperior);
            model.Source = Source;
            model.IfContact = IfContact;
            model.ClassiFication = ClassiFication;
            model.Business = Business;
            model.PrimaryContact = PrimaryContact;
            model.ShortPhone = ShortPhone;
            model.Sex = Sex;
            model.CountriesRegions = CountriesRegions;
            model.Zip = Zip;
            model.Address = Address;
            model.Province = Province;
            model.County = County;
            model.Remarks = Remarks;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ClientLiaisons bll = new XinYiOffice.BLL.ClientLiaisons();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
