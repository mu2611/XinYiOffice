﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientTracking.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 <form id="f2" name="f2" runat="server">
  <div class="setup_box" style=" margin-bottom:0">
     	<div class="h_title"><h4>联系人修改</h4></div>
        
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		当前客户热度
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtClientHeat" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		销售机会Id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSalesOpportunitiesId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		销售机会阶段 状态
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSalesOpportunitiesSate" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		回访备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBackNotes" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		回访状态 未回访
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSate" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		是否下步跟进 1-是,0-否
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsFollowUp" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		不跟进原因 (关闭原因)
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCloseNotes" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		回访人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtReturnAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际回访时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtActualTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		计划回访时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtPlanTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	</table>
    </div>
    
    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>   
          
    </form>

</asp:Content>


