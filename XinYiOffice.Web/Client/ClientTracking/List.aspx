﻿<%@ Page Title="客户跟踪表 对某个销" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Client.ClientTracking.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2"  >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            
<%if (ValidatePermission("CLIENTTRACKING_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("CLIENTTRACKING_LIST_ADD")&&false)
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("CLIENTTRACKING_LIST_EDIT")
                  &&false)
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("CLIENTTRACKING_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>
            
            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:searchapp ID="SearchApp1" runat="server" TableName="vClientTracking"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ClientTracking_table">
            	
                
                <asp:Repeater ID="repClientTracking" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>客户名称</th><th>当前热度</th><th>销售机会</th><th>机会阶段</th><th>计划回访时间</th><th>回访状态</th><th>是否跟进</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td ><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="/Client/ClientTracking/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "ClientInfoName")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Heat")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "OppName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "SalesOpportunitiesSateName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "PlanTime","{0:yyyy-MM-dd}")%></td>
                <td><a href="/Client/ClientTracking/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "SateName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "IsFollowUpName")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        
</div>


</form>
<%=pagehtml%>

<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>



