﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/BasicContent.Master" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Client.MarketingPlan.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" ID="he" runat="server">
<link href="/css/marketingplan.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" ID="cph" runat="server">
<form id="f2" name="f2" runat="server">

<div class="setup_box">
    	<div class="h_title"><h4>营销计划信息</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">计划所有人：</td><td><asp:Label id="lblSchemerAccountId" runat="server"></asp:Label> (<asp:Label id="lblId" runat="server"></asp:Label>)</td><td class="table_left nonestyle">
                执行人：</td><td><asp:Label id="lblExecuteAccountId" runat="server"></asp:Label></td></tr>
            <tr><td class="table_left">计划名称：</td><td><b><asp:Label id="lblProgramName" runat="server"></asp:Label></b></td><td class="table_left">状态：</td><td><asp:Label id="lblSate" runat="server"></asp:Label></td></tr>
            <tr><td class="table_left">类型：</td><td><asp:Label id="lblTYPE" runat="server"></asp:Label></td><td class="table_left nonestyle"></td><td></td></tr> 
            <tr><td class="table_left">开始日期：</td><td><asp:Label id="lblStartTime" runat="server"></asp:Label></td><td class="table_left">结束日期：</td><td><asp:Label id="lblEndTime" runat="server"></asp:Label></td></tr> 
            <tr><td class="table_left">创建时间：</td><td><asp:Label id="lblCreateTime" runat="server"></asp:Label>&nbsp;</td><td class="table_left">
                更新时间：</td><td><asp:Label id="lblRefreshTime" runat="server"></asp:Label>&nbsp;</td></tr> 
        </table>
        <div class="h_title"><h4>预期</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">预期收入：</td><td><asp:Label id="lblAnticipatedRevenue" runat="server"></asp:Label></td><td class="table_left">预算成本：</td><td><asp:Label id="lblBudgetCost" runat="server"></asp:Label></td></tr>
        </table>
        <div class="h_title"><h4>描述信息</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">描述：</td><td style=" width:auto"><asp:Label id="lblProgramDescription" runat="server"></asp:Label></td></tr>
        </table>
    </div>

    </form>
</asp:Content>





