﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.MarketingPlan
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MARKETINGPLAN_LIST_EDIT"))
            {
                NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.MarketingPlan bll = new XinYiOffice.BLL.MarketingPlan();
            //XinYiOffice.Model.MarketingPlan model = bll.GetModel(Id);

            SetDropDownList("Sate", ref DropDownList_Sate);
            SetDropDownList("Type", ref DropDownList_TYPE);

            DataSet ds = ClientServer.GetMarketingPlan(string.Format("Id={0}", Id),CurrentTenantId);
            DataRowCollection drc = ds.Tables[0].Rows;

            this.lblId.Text =  SafeConvert.ToString(drc[0]["Id"]);
            HiddenField_id.Value = SafeConvert.ToString(drc[0]["Id"]);

            txtSchemerAccountId.Text = SafeConvert.ToString(drc[0]["SchemerName"]);
            txtExecuteAccountId.Text = SafeConvert.ToString(drc[0]["ExecuteName"]);
            HiddenField_SchemerAccountId.Value = SafeConvert.ToString(drc[0]["SchemerAccountId"]);
            HiddenField_ExecuteAccountId.Value = SafeConvert.ToString(drc[0]["ExecuteAccountId"]);

            txtProgramName.Text = SafeConvert.ToString(drc[0]["ProgramName"]);
            DropDownList_Sate.SelectedValue = SafeConvert.ToString(drc[0]["Sate"]);
            DropDownList_TYPE.SelectedValue = SafeConvert.ToString(drc[0]["TYPE"]);
            txtStartTime.Text = SafeConvert.ToString(drc[0]["StartTime"]);
            txtEndTime.Text = SafeConvert.ToString(drc[0]["EndTime"]);
            txtAnticipatedRevenue.Text = SafeConvert.ToString(drc[0]["AnticipatedRevenue"]);
            txtBudgetCost.Text = SafeConvert.ToString(drc[0]["BudgetCost"]);
            txtProgramDescription.Text = SafeConvert.ToString(drc[0]["ProgramDescription"]);


        }


        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.MarketingPlanDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }



        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(HiddenField_id.Value);
            string ProgramName = this.txtProgramName.Text;
            string ProgramDescription = this.txtProgramDescription.Text;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            DateTime StartTime = DateTime.Parse(this.txtStartTime.Text);
            DateTime EndTime = DateTime.Parse(this.txtEndTime.Text);
            int TYPE = int.Parse(DropDownList_TYPE.SelectedValue);
            decimal AnticipatedRevenue = decimal.Parse(this.txtAnticipatedRevenue.Text);
            decimal BudgetCost = decimal.Parse(this.txtBudgetCost.Text);
            int SchemerAccountId = int.Parse(this.HiddenField_SchemerAccountId.Value);
            int ExecuteAccountId = int.Parse(this.HiddenField_ExecuteAccountId.Value);

            int RefreshAccountId = CurrentAccountId;
            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.BLL.MarketingPlan bll = new XinYiOffice.BLL.MarketingPlan();

            XinYiOffice.Model.MarketingPlan model = bll.GetModel(Id);
          
            model.ProgramName = ProgramName;
            model.ProgramDescription = ProgramDescription;
            model.Sate = Sate;
            model.StartTime = StartTime;
            model.EndTime = EndTime;
            model.TYPE = TYPE;
            model.AnticipatedRevenue = AnticipatedRevenue;
            model.BudgetCost = BudgetCost;
            model.SchemerAccountId = SchemerAccountId;
            model.ExecuteAccountId = ExecuteAccountId;

            model.RefreshAccountId = RefreshAccountId;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
