﻿<%@ Page Title="销售机会"  MasterPageFile="~/BasicContent.Master"  Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Client.SalesOpportunities.List" %>

<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="marketing_box">
    	<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
                <div class="btn clear">
                
<%if (ValidatePermission("SALESOPPORTUNITIES_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("SALESOPPORTUNITIES_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("SALESOPPORTUNITIES_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("SALESOPPORTUNITIES_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>
               
               </div>
                <div class="del clear"></div>
            </div>
            <form id="f2" >
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:SearchApp ID="SearchApp1" runat="server" TableName="vSalesOpportunities"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>  
                <a href="javascript:;" class="cancel res_search">取消</a>
              
                </div>
        	</div>
            </form>
        </div>
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="MarketingPlan_table">
            	
                
                <asp:Repeater ID="repvSalesOpportunities" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>机会名称</th><th>状态</th><th>目前阶段</th><th>所属客户</th><th>所属人</th><th>营销计划</th><th>来源</th><th>发生时间</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "OppName")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "SateName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "PresentStageName")%></td>
                
                <td><%#DataBinder.Eval(Container.DataItem, "ClientName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "FullName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ProgramName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "SourceName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "OccTime","{0:yyyy-MM-dd}")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
    </div>
<%=pagehtml%>
    <script>
        $('#MarketingPlan_table td').hover(function () {
            $(this).parent().find('td').css('background', '#f0f8fc')
        }, function () {
            $(this).parent().find('td').css('background', 'none')
        });
</script>

</asp:Content>
