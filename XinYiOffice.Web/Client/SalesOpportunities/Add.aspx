﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Client.SalesOpportunities.Add"
    Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

<link href="/css/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script src="/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

<script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.8.17.custom.min.js"></script>
<script src="/js/jquery-ui-widget-combobox.js"></script>
<script type="text/javascript">
    $(function () {
        var autoCustomerService = $("#<%=txtCustomerService.ClientID%>");
        var autoHiddenField_CustomerService = $("#<%=HiddenField_CustomerService.ClientID%>");

        var autotxtPersonalAccountId = $("#<%=txtPersonalAccountId.ClientID%>");
        var autoHiddenField_PersonalAccountId = $("#<%=HiddenField_PersonalAccountId.ClientID%>");

        var autoselect_plan = $("#<%=txtMarketingPlanId.ClientID%>");
        var select_pan = $("#<%=hidtxtMarketingPlanId.ClientID%>");

        var datas1;
        //objects为json数据源对象
        var url = "/Call/Ajax.aspx?action=getMarketingPlan&sj=" + Math.random();
        var url_getclient = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random();
        var url_getofficeinfo = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();
        //营销计划
        autoselect_plan.autocomplete({
            source: url,
            select: function (event, ui) {
                //alert(ui.item.value);
                autoselect_plan.val(ui.item.label);
                select_pan.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoselect_plan.val(ui.item.label);
                return false;
            }
        });

        //客户
        autoCustomerService.autocomplete({
            source: url_getclient,
            select: function (event, ui) {
                //alert(ui.item.value);
                autoCustomerService.val(ui.item.label);
                autoHiddenField_CustomerService.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoCustomerService.val(ui.item.label);
                return false;
            }
        });

        //职员
        autotxtPersonalAccountId.autocomplete({
            source: url_getofficeinfo,
            select: function (event, ui) {
                //alert(ui.item.value);
                autotxtPersonalAccountId.val(ui.item.label);
                autoHiddenField_PersonalAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autotxtPersonalAccountId.val(ui.item.label);
                return false;
            }
        });


    });
</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">

<div class="setup_box" style=" margin-bottom:0">

<div class="h_title"><h4>添加销售机会</h4></div>
<table cellspacing="0" cellpadding="0" width="100%" border="0" class="btable">
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        机会名称 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtOppName" runat="server" width="200px"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        所属客户 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtCustomerService" runat="server" width="200px"></asp:textbox>
                        <asp:hiddenfield id="HiddenField_CustomerService" runat="server" />
                        (打.可列出全部,也可按客户名称智能提示)
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        所属人 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtPersonalAccountId" runat="server" width="200px"></asp:textbox>
                        <asp:hiddenfield id="HiddenField_PersonalAccountId" runat="server" />
                        (默认为当前登录用户)
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        来自营销计划 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtMarketingPlanId" runat="server" width="200px"></asp:textbox>
                        <asp:hiddenfield id="hidtxtMarketingPlanId" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        来源 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:dropdownlist id="DropDownList_Source" runat="server">
        </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        客户需求描述 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtRequirement" runat="server" width="520px" height="77px" textmode="MultiLine"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        预计金额 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtEstAmount" runat="server" width="200px">0.00</asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        外币备注 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtCurrencyNotes" runat="server" width="200px"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        目前阶段 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:dropdownlist id="DropDownList_PresentStage" runat="server">
        </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        下一步 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:dropdownlist id="DropDownList_NextStep" runat="server">
        </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        状态 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:dropdownlist id="DropDownList_Sate" runat="server">
        </asp:dropdownlist>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        阶段备注 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtStageRemarks" runat="server" height="82px" textmode="MultiLine"
                            width="521px"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        发生时间 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtOccTime" runat="server" width="102px"   class="Wdate"  onClick="WdatePicker()"></asp:textbox>
                    </td>
                </tr>
                <tr>
                    <td height="25" width="30%" align="right" class="table_left">
                        预计确定日期 ：
                    </td>
                    <td height="25" width="*" align="left">
                        <asp:textbox id="txtEstDate" runat="server" width="100px" class="Wdate"  onClick="WdatePicker()"></asp:textbox>
                    </td>
                </tr>
            </table>
            
            
               <div class="clear btn_box">
                       <asp:LinkButton ID="btnSave" runat="server" CssClass="save" 
                           onclick="btnSave_Click1">保存</asp:LinkButton>
                         <asp:LinkButton ID="LinkButton2" runat="server" CssClass="cancel">取消</asp:LinkButton>

                        
                
              </div>  


</div>
</form>

</asp:Content>


