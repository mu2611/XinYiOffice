﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.SalesOpportunities
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("SALESOPPORTUNITIES_LIST_ADD"))
            {
                NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            SetDropDownList("Source", ref DropDownList_Source);
            //SetDropDownList("Feasibility", ref DropDownList_Feasibility);
            SetDropDownList("PresentStage", ref DropDownList_PresentStage);
            SetDropDownList("PresentStage", ref DropDownList_NextStep);
            SetDropDownList("Sate", ref DropDownList_Sate);


            txtPersonalAccountId.Text = CurrentAccountTrueName;
            HiddenField_PersonalAccountId.Value = CurrentAccountId.ToString();

            txtOccTime.Text = DateTime.Now.ToShortDateString();
            txtEstDate.Text = DateTime.Now.AddDays(3).ToShortDateString();

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.SalesOpportunitiesDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void btnSave_Click1(object sender, EventArgs e)
        {
            string OppName = this.txtOppName.Text;
            int CustomerService = SafeConvert.ToInt(HiddenField_CustomerService.Value);
            int PersonalAccountId = SafeConvert.ToInt(HiddenField_PersonalAccountId.Value);
            int MarketingPlanId = SafeConvert.ToInt(hidtxtMarketingPlanId.Value);
            int Source = SafeConvert.ToInt(DropDownList_Source.SelectedValue);
            string Requirement = this.txtRequirement.Text;
            decimal EstAmount = decimal.Parse(this.txtEstAmount.Text);
            string CurrencyNotes = this.txtCurrencyNotes.Text;
            //int Feasibility =  SafeConvert.ToInt(DropDownList_Feasibility.SelectedValue);
            int Feasibility = 0;
            int PresentStage = SafeConvert.ToInt(DropDownList_PresentStage.SelectedValue);
            Feasibility = PresentStage;

            int NextStep = SafeConvert.ToInt(DropDownList_NextStep.SelectedValue);
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            string StageRemarks = this.txtStageRemarks.Text;
            DateTime OccTime = DateTime.Parse(this.txtOccTime.Text);
            DateTime EstDate = DateTime.Parse(this.txtEstDate.Text);
            int RefreshAccountId = CurrentAccountId;
            int CreateAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.SalesOpportunities model = new XinYiOffice.Model.SalesOpportunities();
            model.OppName = OppName;
            model.CustomerService = CustomerService;
            model.PersonalAccountId = PersonalAccountId;
            model.MarketingPlanId = MarketingPlanId;
            model.Source = Source;
            model.Requirement = Requirement;
            model.EstAmount = EstAmount;
            model.CurrencyNotes = CurrencyNotes;
            model.Feasibility = Feasibility;
            model.PresentStage = PresentStage;
            model.NextStep = NextStep;
            model.Sate = Sate;
            model.StageRemarks = StageRemarks;
            model.OccTime = OccTime;
            model.EstDate = EstDate;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.SalesOpportunities bll = new XinYiOffice.BLL.SalesOpportunities();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
