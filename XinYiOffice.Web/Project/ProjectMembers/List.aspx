﻿<%@ Page Title="项目组成员 成员与工" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Project.ProjectMembers.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<style>
.marketing_table table td
{
    padding-top:5px;
 }
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2">
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear"><a href="javascript:;" id="search_a"  class="sj" style=" display:none;">条件查询</a><a href="add.aspx" style=" display:none;">添加</a><a href="javascript:history.go(-1);" class="edit2" style=" display:;">返回</a><a href="javascript:;" class="tdel">删除</a></div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:searchapp ID="SearchApp1" runat="server" TableName="vProjectInfoList"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ProjectMembers_table">

                <asp:Repeater ID="repProjectMembers" runat="server" >
                <HeaderTemplate>
                <tr><th class="center"><input type="checkbox" id="cb_all"></th>
                <th>姓名</th>
                <th>项目角色</th>
                <th>创建时间</th>
                <th>创建人</th>
              
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>">
                 <img src="<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem,"HeadPortrait").ToString())%>" title="<%#DataBinder.Eval(Container.DataItem,"FullName")%>" width="37" height="37"/>
                <span><%#DataBinder.Eval(Container.DataItem, "FullName")%></span>
                
                </a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Name")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateFullName")%></td>
                
                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>


</form>

<script>
    $('#ProjectMembers_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>


