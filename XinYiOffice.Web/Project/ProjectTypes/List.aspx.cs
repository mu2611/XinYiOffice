﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Project.ProjectTypes
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.ProjectTypes bll = new XinYiOffice.BLL.ProjectTypes();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PROJECTTYPES_BASIC") && base.ValidatePermission("PROJECTTYPES_LIST") ))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                
                BindData();
            }
        }

        #region gridView          
        public void BindData()
        {
            StringBuilder sb = new StringBuilder(" 1=1 ");

            DataTable dt = ProjectInfoServer.GetProjectTypes(sb.ToString(),CurrentTenantId);

            repProjectTypes.DataSource = dt;
            repProjectTypes.DataBind();
        }

        #endregion

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

        }

    }
}
