﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Project.ProjectTypes.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server" enctype="multipart/form-data">
<div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改项目类型</h4></div>

   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		项目类型名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTypeName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		类型描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTypeDescribe" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		小图标：</td>
	<td height="25" width="*" align="left">
		<asp:FileUpload ID="FileUpload_TypeIco" runat="server" Width="260px" />
        <asp:Image ID="Image_TypeIco" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		大图标
	：</td>
	<td height="25" width="*" align="left">
		<asp:FileUpload ID="FileUpload_TypeIcoMax" runat="server" Width="260px" />
        <asp:Image ID="Image_TypeIcoMax" runat="server" />
        <asp:HiddenField ID="HiddenField_RefreshTime" runat="server" />
        <asp:HiddenField ID="HiddenField_CreateTime" runat="server" />
        <asp:HiddenField ID="HiddenField_RefreshAccountId" runat="server" />
        <asp:HiddenField ID="HiddenField_CreateAccountId" runat="server" />
	</td></tr>
</table>
</div>
<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>

    </form>
</asp:Content>

 
