﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Project.ProjectInfo.Add" Title="增加页" %>
<asp:Content ContentPlaceHolderID="head" runat="server">
  <style type="text/css">
        .style1
        {
            height: 25px;
        }
        .setup_box table .wbl_15{ width:15%;}
    </style>


<script src="/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

	<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>

	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

<script>
    $(function () {

        var autoPMAccountId = $("#<%=txtProjectManagerAccountId.ClientID%>");
        var auto_hid_pm_account = $("#<%=hidProjectManagerAccountId.ClientID%>");
        var url_getwork = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();
        var url_getclient = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random();
        var url_getsopp = "/Call/Ajax.aspx?action=getsopp&sj=" + Math.random();
        var url_getclientliaisons = "/Call/Ajax.aspx?action=getclientliaisons&sj=" + Math.random();

        var auto_OrganiserAccountId = $("#<%=TextBox_OrganiserAccountId.ClientID%>");
        var auto_HiddenField_OrganiserAccountId = $("#<%=HiddenField_OrganiserAccountId.ClientID%>");

        var auto_TextBox_ExecutorAccountId = $("#<%=TextBox_ExecutorAccountId.ClientID%>")
        var auto_HiddenField_ExecutorAccountId = $("#<%=HiddenField_ExecutorAccountId.ClientID%>");

        var auto_txtByClientInfoId = $("#<%=txtByClientInfoId.ClientID%>");
        var auto_hid_ByClientInfoId = $("#<%=hid_ByClientInfoId.ClientID%>");

        var auto_txtBySalesOpportunitiesId = $("#<%=txtBySalesOpportunitiesId.ClientID%>");
        var auto_hid_BySalesOpportunitiesId = $("#<%=hid_BySalesOpportunitiesId.ClientID%>");

        var auto_txtSurveyorAccountId = $("#<%=txtSurveyorAccountId.ClientID%>");
        var auto_hid_SurveyorAccountId = $("#<%=hid_SurveyorAccountId.ClientID%>");

        //客户
        auto_txtByClientInfoId.autocomplete({
            source: url_getclient,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_txtByClientInfoId.val(ui.item.label);
                auto_hid_ByClientInfoId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_txtByClientInfoId.val(ui.item.label);
                return false;
            }
        });

        //联系人
        auto_txtSurveyorAccountId.autocomplete({
            source: url_getclientliaisons,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_txtSurveyorAccountId.val(ui.item.label);
                auto_hid_SurveyorAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_txtSurveyorAccountId.val(ui.item.label);
                return false;
            }
        });


        //销售机会
        auto_txtBySalesOpportunitiesId.autocomplete({
            source: url_getsopp,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_txtBySalesOpportunitiesId.val(ui.item.label);
                auto_hid_BySalesOpportunitiesId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_txtBySalesOpportunitiesId.val(ui.item.label);
                return false;
            }
        });

        //人员
        autoPMAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                autoPMAccountId.val(ui.item.label);
                auto_hid_pm_account.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoPMAccountId.val(ui.item.label);
                return false;
            }
        });

        //发起者
        auto_OrganiserAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_OrganiserAccountId.val(ui.item.label);
                auto_HiddenField_OrganiserAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_OrganiserAccountId.val(ui.item.label);
                return false;
            }
        });

        //执行人
        auto_TextBox_ExecutorAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                auto_TextBox_ExecutorAccountId.val(ui.item.label);
                auto_HiddenField_ExecutorAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                auto_TextBox_ExecutorAccountId.val(ui.item.label);
                return false;
            }
        });


        var sel_year = $("#<%=ddl_Year.ClientID%>");
        var sel_month = $("#<%=ddl_month.ClientID%>");
        var sel_project = $("#<%=DropDownList_ParentProjectId.ClientID%>");
        var hidParentProjectId = $("#<%=hid_ParentProjectId.ClientID%>");

        //根据月份选择项目
        sel_month.change(function () {

            sel_project.html("");
            sel_project.append("<option value='0'>没有所属项目</option>");

            $.getJSON("/Call/Ajax.aspx?action=getproject&month=" + sel_month.val() + "&year=" + sel_year.val() + "&sj=" + Math.random(), function (json) {
                //alert(json);
                $.each(json, function (i) {
                    sel_project.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
                });
            });


        });


        //选择项目
        sel_project.change(function () {
            if (sel_project.val() == "0") {
                return;
            }
            hidParentProjectId.val(sel_project.val());
        });


        $(obj).bind("input.autocomplete", function () { $(this).trigger('keydown.autocomplete'); })

    });
</script>

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>添加项目</h4></div>
 
 <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">
		所属上级项目：</td>
	<td height="25" colspan="3" align="left">
	  <asp:DropDownList ID="ddl_Year" runat="server">
	    <asp:ListItem>2012</asp:ListItem>
	    <asp:ListItem>2011</asp:ListItem>
	    <asp:ListItem>2013</asp:ListItem>
	    <asp:ListItem>2014</asp:ListItem>
	    <asp:ListItem>2015</asp:ListItem>
	    <asp:ListItem>2016</asp:ListItem>
	    </asp:DropDownList>
	  <asp:DropDownList ID="ddl_month" runat="server">
	    <asp:ListItem>1</asp:ListItem>
	    <asp:ListItem>2</asp:ListItem>
	    <asp:ListItem>3</asp:ListItem>
	    <asp:ListItem>4</asp:ListItem>
	    <asp:ListItem>5</asp:ListItem>
	    <asp:ListItem>6</asp:ListItem>
	    <asp:ListItem>7</asp:ListItem>
	    <asp:ListItem>8</asp:ListItem>
	    <asp:ListItem>9</asp:ListItem>
	    <asp:ListItem>10</asp:ListItem>
	    <asp:ListItem>11</asp:ListItem>
	    <asp:ListItem>12</asp:ListItem>
	    </asp:DropDownList>
	  <asp:DropDownList ID="DropDownList_ParentProjectId" runat="server">
	    <asp:ListItem Value="0">没有所属项目</asp:ListItem>
	    </asp:DropDownList>
	  <asp:HiddenField ID="hid_ParentProjectId" runat="server" />
	  </td>
	</tr>
	<tr>
	  <td height="25" align="right" class="table_left wbl_15">项目类型：</td>
	  <td height="25" align="left"><asp:DropDownList ID="ddl_ProjectTypeId" runat="server"> </asp:DropDownList></td>
	  <td height="25" align="left" class="table_left wbl_15">项目属性：</td>
	  <td height="25" align="left"><asp:DropDownList ID="DropDownList_ProjectPropertie" runat="server"> </asp:DropDownList></td>
	  </tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">项目标题
	：</td>
	<td width="27%" height="25" align="left"><asp:TextBox id="txtTitle" runat="server" Width="200px"></asp:TextBox>
        (在列表中显示)</td>
	<td width="15%" height="25" align="left" class="table_left wbl_15">项目负责人
	：</td>
	<td width="29%" height="25" align="left">
        <asp:TextBox id="txtProjectManagerAccountId" runat="server" Width="200px" 
            MaxLength="20" CssClass="auto_input" ></asp:TextBox>
      <asp:HiddenField ID="hidProjectManagerAccountId" runat="server" />
(默认为当前登录用户)</td>
	</tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">项目名称
	：</td>
	<td height="25" align="left"><asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
        (一般跟标题一样)</td>
	<td height="25" align="left" class="table_left wbl_15">发起人：</td>
	<td height="25" align="left">
        <asp:TextBox ID="TextBox_OrganiserAccountId" runat="server"  Width="200px" CssClass="auto_input"></asp:TextBox>
        <asp:HiddenField ID="HiddenField_OrganiserAccountId" runat="server" />
        </td>
	</tr>
	<tr>
	  <td height="25" align="right" class="table_left wbl_15">状态
	：</td>
	  <td height="25" align="left"><asp:DropDownList ID="DropDownList_State" runat="server"> </asp:DropDownList></td>
	  <td height="25" align="left" class="table_left wbl_15">执行人：</td>
	  <td height="25" align="left">
          <asp:TextBox ID="TextBox_ExecutorAccountId" runat="server"  Width="200px" CssClass="auto_input"></asp:TextBox>
          <asp:HiddenField ID="HiddenField_ExecutorAccountId" runat="server" />
        </td>
	  </tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">工作简述
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtJobDescription" runat="server" Width="488px" Height="72px" 
            TextMode="MultiLine"></asp:TextBox></td></tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">URL：</td>
	<td height="25" colspan="3" align="left">
        <asp:TextBox ID="TextBox_URL" runat="server" Width="488px">http://</asp:TextBox>
        </td></tr>
	<tr>
	  <td height="25" width="29%" align="right" class="table_left wbl_15">所属客户
	    ：</td>
	  <td height="25" colspan="3" align="left"><asp:TextBox id="txtByClientInfoId" runat="server" Width="200px" CssClass="auto_input"></asp:TextBox>
	    <asp:HiddenField ID="hid_ByClientInfoId" runat="server" /></td></tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">验收人
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtSurveyorAccountId" runat="server" Width="200px" CssClass="auto_input"></asp:TextBox>
      <asp:HiddenField ID="hid_SurveyorAccountId" runat="server" /></td></tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15"><span class="style1">前置任务：</span></td>
	<td height="25" colspan="3" align="left"><span class="style1">
	  <asp:DropDownList ID="ddl_PredecessorTaskProjectId" runat="server"> </asp:DropDownList>
	    (说明要完成此项目的之前项目)</span></td></tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">来源销售机会
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtBySalesOpportunitiesId" runat="server" Width="200px" CssClass="auto_input"></asp:TextBox>
      <asp:HiddenField ID="hid_BySalesOpportunitiesId" runat="server" /></td></tr>
	<tr>
	<td height="25" width="29%" align="right" class="table_left wbl_15">备注
	：</td>
	<td height="25" colspan="3" align="left"><asp:TextBox id="txtRemarks" runat="server" Width="200px"></asp:TextBox></td></tr>
	</table>
    
     <div class="h_title"><h4>时间成本</h4></div>
     <table width="100%" border="0" cellPadding="0" cellSpacing="0">
  <tr>
    <td height="25" width="29%" align="right" class="table_left wbl_15"> 项目预计完成时间
      ：</td>
    <td height="25" align="left"><asp:TextBox ID="txtCompletionTime" runat="server" Width="100px"  
            class="Wdate"  onClick="WdatePicker()" ></asp:TextBox></td>
    <td height="25" align="left" class="table_left wbl_15"><span class="style1">项目实际完成时间
      ：</span></td>
    <td height="25" align="left"><span class="style1">
      <asp:TextBox ID="txtActualFinishTime" runat="server" Width="100px"  
            class="Wdate"  onClick="WdatePicker()" ></asp:TextBox>
    </span></td>
  </tr>
  <tr>
    <td height="25" width="29%" align="right" class="table_left wbl_15">项目预计开始时间
      ：</td>
    <td height="25" align="left"><asp:TextBox ID="txtEstTimeStart" runat="server" Width="100px"  
            class="Wdate"  onClick="WdatePicker()"></asp:TextBox></td>
    <td height="25" align="left" class="table_left wbl_15">项目实际开始时间
      ：</td>
    <td height="25" align="left"><asp:TextBox ID="txtActualStartTime" runat="server" Width="100px"  
            class="Wdate"  onClick="WdatePicker()"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" width="29%" align="right" class="table_left wbl_15">预计完成工作日
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtCompleteWorkDay" runat="server" 
            Width="50px">1</asp:TextBox>
      天</td>
    <td height="25" align="left" class="table_left wbl_15">实际完成工作日
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtActualWorkingDay" runat="server" 
            Width="50px">1</asp:TextBox>
      天</td>
  </tr>
</table>

<div class="h_title"><h4>费用成本</h4></div>
<table width="100%" border="0" cellPadding="0" cellSpacing="0">
  <tr>
    <td width="29%" align="right" class="table_left wbl_15">项目预计费用
      ：</td>
    <td align="left" class="style1"><asp:TextBox id="txtEstDisbursement" runat="server" Width="200px">0.00</asp:TextBox></td>
    <td align="left" class="table_left wbl_15">实际支付费用
      ：</td>
    <td align="left" class="style1"><asp:TextBox id="txtActualPayment" runat="server" Width="200px">0.00</asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" align="right" class="table_left wbl_15">预计支出
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtEstExpenses" runat="server" Width="200px">0.00</asp:TextBox></td>
    <td height="25" align="left" class="table_left wbl_15">实际支出
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtActualExpenditure" runat="server" Width="200px">0.00</asp:TextBox></td>
  </tr>
  <tr>
    <td height="25" align="right" class="table_left wbl_15">预计收入
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtAnticipatedRevenue" runat="server" Width="200px">0.00</asp:TextBox></td>
    <td height="25" align="left" class="table_left wbl_15">实际收入
      ：</td>
    <td height="25" align="left"><asp:TextBox id="txtIncome" runat="server" Width="200px">0.00</asp:TextBox></td>
  </tr>
</table>
     
   </div>

    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <%--<a href="javascript:history.go(-1);" class="cancel">取消</a>--%>
        
    </div>


    </form>
</asp:Content>

